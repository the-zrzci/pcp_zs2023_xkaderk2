import os


def write_sources(file, name: str, sources: list[str]):
    sources.sort()
    sources_str = "\n".join(map(lambda s: "    " + s.replace("\\", "/"), sources))
    file.write(f"set({name} \n{sources_str}\n)\n\n")


def main():
    sources = []
    qmls = []
    assets = []

    # find source files
    for root, _dirs, files in os.walk("src"):
        for file in files:
            path = os.path.join(root, file)
            match file.split(".")[-1]:
                case "h" | "cpp":
                    sources.append(path)
                case "qml" | "js":
                    qmls.append(path)
                case "qmldir":
                    assets.append(path)

    # find assets
    for root, _dirs, files in os.walk("assets"):
        for file in files:
            assets.append(os.path.join(root, file))

    with open("generated.cmake", "w", encoding="utf-8") as f:
        write_sources(f, "game_sources", sources)
        write_sources(f, "game_qmls", qmls)
        write_sources(f, "game_assets", assets)


if __name__ == "__main__":
    main()
