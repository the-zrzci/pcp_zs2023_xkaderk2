#version 440

layout(location = 0) in vec2 qt_TexCoord0;
layout(location = 1) in vec2 qt_MultiTexCoord0;

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform buf {
    mat4 qt_Matrix;
    float qt_Opacity;
    vec2 tilePos;
    vec2 mapSize;
    float amplitude;
    float scale;
    float time;
    float reflectionAlpha;
};

layout(binding = 1) uniform sampler2D source;
layout(binding = 2) uniform sampler2D reflectionSource;

void main() {
    vec4 color = texture(source, qt_TexCoord0);

    vec2 pos = (tilePos + qt_TexCoord0) / mapSize;
    vec4 reflection = texture(reflectionSource, pos);

    fragColor = (reflection * reflectionAlpha + color * (1-reflectionAlpha)) * qt_Opacity;
}
