#version 440

layout(location = 0) in vec2 qt_TexCoord0;
layout(location = 1) in vec2 qt_MultiTexCoord0;

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform buf {
    mat4 qt_Matrix;
    float qt_Opacity;
    vec2 center;
    float ratio;
    float exponent;
    float minAlpha;
    float radius;
    float resolution;
};

layout(binding = 1) uniform sampler2D source;


int dither[8][8] = {
    { 0, 32,  8, 40,  2, 34, 10, 42}, /* 8x8 Bayer ordered dithering */
    {48, 16, 56, 24, 50, 18, 58, 26}, /* pattern. Each input pixel */
    {12, 44,  4, 36, 14, 46,  6, 38}, /* is scaled to the 0..63 range */
    {60, 28, 52, 20, 62, 30, 54, 22}, /* before looking in this table */
    { 3, 35, 11, 43,  1, 33,  9, 41}, /* to determine the action. */
    {51, 19, 59, 27, 49, 17, 57, 25},
    {15, 47,  7, 39, 13, 45,  5, 37},
    {63, 31, 55, 23, 61, 29, 53, 21}
}; 


float find_closest(int x, int y, float c0) {
    float limit = 0.0;
    if(x < 8) {
        limit = (dither[x][y]+1)/64.0;
    }

    if(c0 < limit) return 0.0;
    return 1.0;
}


void main() {
    vec2 aspect = vec2(1.0, 1.0 / ratio);
    vec2 coords = qt_TexCoord0 * aspect;
    float d = distance(center * aspect, coords);
    d = clamp(pow(d,exponent)/radius, 0, 1);

    float alpha = d;

    // Deithering
    int x = int(coords.x * resolution) % 8;
    int y = int(coords.y * resolution) % 8;
    
    alpha *= find_closest(x, y, d);
    alpha = clamp(alpha + minAlpha, 0, 1);

    // Output
    vec4 c = texture(source, qt_TexCoord0);
    fragColor = c.rgba * alpha * qt_Opacity;
}
