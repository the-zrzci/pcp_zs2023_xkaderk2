#version 440

layout(location = 0) in vec2 qt_TexCoord0;
layout(location = 1) in vec2 qt_MultiTexCoord0;

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform buf {
    mat4 qt_Matrix;
    float qt_Opacity;
    float base;
    float factor;
    float topAO;
    float leftAO;
    float bottomAO;
    float rightAO;
};

layout(binding = 1) uniform sampler2D source;

void main() {
    vec4 c = texture(source, qt_TexCoord0);

    float fac = max(0, base 
        - max(0, topAO - qt_TexCoord0.y) * factor
        - max(0, leftAO - qt_TexCoord0.x) * factor
        + min(0, (1 - qt_TexCoord0.y) - bottomAO) * factor
        + min(0, (1 - qt_TexCoord0.x) - rightAO) * factor
    );

    fragColor = vec4(c.r * fac, c.g * fac, c.b * fac, c.a) * qt_Opacity;
}
