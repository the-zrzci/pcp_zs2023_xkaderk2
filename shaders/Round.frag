#version 440

layout(location = 0) in vec2 qt_TexCoord0;
layout(location = 1) in vec2 qt_MultiTexCoord0;

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform buf {
    mat4 qt_Matrix;
    float qt_Opacity;
    float radius;
};

layout(binding = 1) uniform sampler2D source;

const vec2 CENTER = vec2(0.5, 0.5);
const vec4 EMPTY = vec4(0, 0, 0, 0);

void main() {
    if (distance(qt_TexCoord0, CENTER) < radius) {
        fragColor = texture(source, qt_TexCoord0) * qt_Opacity;
    } else {
        fragColor = EMPTY;
    }
}
