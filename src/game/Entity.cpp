#include "Entity.h"

#include <QJsonArray>

#include "core/Game.h"
#include "core/Map.h"
#include "core/utils.h"

game::Entity::Entity(core::Game* game, EntityParams const& params, game::Entity::Type type)
  : m_game(game),
    m_name(params.name),
    m_nickname(params.nickname),
    m_spriteSheet(params.spriteSheet),
    m_hitbox(params.hitbox),
    m_type(type) {}

game::Entity::Entity(core::Game* game, EntityParams const& params)
  : game::Entity::Entity(game, params, game::Entity::Type::Object) {}

void game::Entity::scheduleDelete() {
    m_willDelete = true;
    getCurrentMap()->removeEntity(this);
}

QString game::Entity::getName() const {
    return m_name;
}

QString game::Entity::getNickname() const {
    return m_nickname;
}

void game::Entity::setNickname(QString nickname) {
    m_nickname = nickname;
}

game::Entity::Type game::Entity::getType() const {
    return m_type;
}

void game::Entity::setInteraction(game::Entity::Interaction interaction) {
    m_interaction = interaction;
    emit interactionChanged();
}

game::core::Map* game::Entity::getCurrentMap() const {
    return qobject_cast<game::core::Map*>(parent());
}

void game::Entity::setSpriteState(int state) {
    if (state < 0 || state >= m_spriteSheet.stateFrameCount.size()) {
        throw std::out_of_range("Invalid sprite state");
    }

    m_spriteState = state;
    progressSpriteFrame(0);  // Make frame inside the state's range

    emit spriteStateChanged();
}

int game::Entity::getSpriteState() const {
    return m_spriteState;
}

int game::Entity::getSpriteFrame() const {
    return static_cast<int>(m_spriteFrame);
}

int game::Entity::getSpriteStateFrameCount() const {
    return m_spriteSheet.stateFrameCount.at(m_spriteState);
}

float game::Entity::getSpriteStatePeriod() const {
    return m_spriteSheet.stateFrameFrequency.at(m_spriteState);
}

void game::Entity::setSpriteFrame(int frame) {
    if (m_spriteSheet.stateFrameCount.size() < 1) return;
    if (frame < 0 || frame >= getSpriteStateFrameCount()) {
        throw std::out_of_range("Invalid sprite frame");
    }

    m_spriteFrame = frame;
    emit spriteFrameChanged();
}

void game::Entity::progressSpriteFrame(double delta) {
    progressSpriteFrame(delta, true);
}
bool game::Entity::progressSpriteFrame(double delta, bool loop) {
    if (m_spriteSheet.stateFrameFrequency.size() < 1) return false;

    m_spriteFrame += delta * getSpriteStatePeriod();

    bool finished = false;
    int frameCount = getSpriteStateFrameCount();
    if (loop) {
        m_spriteFrame = fmod(frameCount + m_spriteFrame, frameCount);
    } else {
        if (m_spriteFrame <= 0) {
            m_spriteFrame = 0;
            finished = true;
        } else if (m_spriteFrame >= frameCount - 1) {
            m_spriteFrame = frameCount - 1;
            finished = true;
        }
    }

    emit spriteFrameChanged();
    return finished;
}

QPointF game::Entity::getLastPosition() {
    auto lastPosition = m_lastPosition;
    m_lastPosition = m_position;
    return lastPosition;
}

QPointF game::Entity::getPosition() const {
    return m_position;
}

void game::Entity::forcePosition(QPointF position) {
    m_lastPosition = {-1, -1};
    return setPosition(position);
}

void game::Entity::setPosition(QPointF position) {
    return setPosition(position, true);
}
void game::Entity::setPosition(QPointF position, bool log) {
    if (m_position == position) {
        return;
    }

    if (m_lastPosition.x() == -1) {
        m_lastPosition = position;
    } else if (log) {
        m_lastPosition = m_position;
    }

    m_position = position;

    emit moved(m_position);
}

float game::Entity::getShadowed() const {
    return m_shadowed;
}

void game::Entity::setShadowed(float shadowed) {
    m_shadowed = shadowed;
    emit shadowedChanged();
}

QSizeF game::Entity::getHitbox() const {
    return m_hitbox;
}

QRectF game::Entity::getBoundingBox() const {
    return QRectF(
      m_position.x() - m_hitbox.width() / 2,
      m_position.y() - m_hitbox.height() / 2,
      m_hitbox.width(),
      m_hitbox.height()
    );
}

bool game::Entity::isCollidable() const {
    return m_collidable;
}

void game::Entity::setCollidable(bool collidable) {
    m_collidable = collidable;

    emit isCollidableChanged();
}

void game::Entity::update(double delta) {
    progressSpriteFrame(delta);
}

void game::Entity::onCollision(const std::vector<Entity*>& entities) {}

void game::Entity::interact(Interaction interaction) {
    emit interacted(interaction);
    emit m_game->playerInteractedWith(this, interaction);
}

void game::Entity::saveState(QJsonObject& json) const {
    json["position"] = game::core::qPairToJson(m_position);
    json["spriteState"] = m_spriteState;
    json["spriteFrame"] = m_spriteFrame;
    json["shadowed"] = m_shadowed;
    if (!m_nickname.isEmpty()) json["nickname"] = m_nickname;

    auto* map = getCurrentMap();
    if (map != nullptr) {
        json["map"] = static_cast<int>(map->getId());
    }
}

void game::Entity::loadState(QJsonObject const& json) {
    m_position = game::core::jsonToPointF(json.value("position"));
    m_spriteState = json.value("spriteState").toInt();
    m_spriteFrame = json.value("spriteFrame").toDouble();
    if (json.contains("nickname")) m_nickname = json.value("nickname").toString();
    m_shadowed = json.value("shadowed").toBool();

    emit moved(m_position);
    emit spriteStateChanged();
    emit spriteFrameChanged();
    emit shadowedChanged();

    if (json.contains("map")) {
        data::MapId mapId = data::MapId(json.value("map").toInt());

        auto* map = getCurrentMap();
        if (map == nullptr || map->getId() != mapId) {
            m_game->setEntityMap(this, data::MapId(json.value("map").toInt()));
        }
    }
}

void game::Entity::onDelete() {
    if (m_willDelete) deleteLater();
}
