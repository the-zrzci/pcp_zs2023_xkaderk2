//
// Created by Matěj on 14.11.2023.
//
#include "Chest.h"

game::Chest::Chest(game::core::Game* game, game::EntityParams entityParams, QString name) : Entity(game, entityParams) {
    m_inventory = new game::Inventory(1, 1);
    m_name = name;
}

game::Chest::Chest(game::core::Game* game, game::EntityParams entityParams, game::Inventory* inventory, QString name)
  : Entity(game, entityParams) {
    m_inventory = inventory;
    m_name = name;
}

game::Chest::Chest(game::core::Game* game, game::EntityParams entityParams, QVector<game::items::Item*> items, QString name)
  : Entity(game, entityParams) {
    m_inventory = new game::Inventory(items);
    m_name = name;
}

game::Chest::Chest(
  game::core::Game* game,
  game::EntityParams entityParams,
  QVector<game::items::Item*> items,
  int capacity,
  int width
)
  : Entity(game, entityParams) {
    m_inventory = new game::Inventory(items, capacity, width);
}
game::Inventory* game::Chest::getInventory() {
    return m_inventory;
}
