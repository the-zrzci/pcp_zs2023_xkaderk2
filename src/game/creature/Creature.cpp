//
// Created by xkapp on 14.11.2023.
//

#include "Creature.h"

#include <iostream>

#include "data/entities.h"
#include "game/creature/effects/Effect.h"
#include "game/creature/effects/HealthRegeneration.h"
#include "game/creature/effects/autoEffects/AutoManaRegen.h"
#include "src/game/core/Game.h"

game::creature::Creature::Creature(
  game::core::Game *game,
  game::EntityParams entityParams,
  int maxHealth,
  game::Inventory *inv,
  Stats *stats,
  bool isFriendly,
  Energy *energy,
  int money
)
  : game::Entity(game, entityParams, game::Entity::Type::Creature) {
    m_maxHealth = maxHealth;
    m_health = m_maxHealth;
    m_inventory = inv;
    m_level = 1;
    m_expPoints = 0;
    m_stats = stats;
    m_abilities = {};
    m_isFriendly = isFriendly;
    m_isStunned = false;
    m_isCasting = false;
    m_isAlive = true;
    m_energy = energy;
    m_money = money;
    m_xpReward = 0;
    m_activeEffects = {};
    m_healthRegeneration = new effects::AutoHealthRegen(this);
    m_energyRegeneration = new effects::AutoManaRegen(this);
    m_expPoints = 0;
    m_weapon = nullptr;
    m_AIController = nullptr;
    m_isInAction = false;
    m_canBeAsleep = false;  // make function to change this
    m_castedAbilityIndex = -1;

    m_actionCheckTimer.start();

    // Set speed to reverse for waking up animation
    m_animationSpeed = -1;
    // Set waking up animation to start
    setSpriteFrame(getSpriteStateFrameCount() - 1);
}

game::creature::Creature::Orientation game::creature::Creature::getOrientation() const {
    return m_orientation;
}

int game::creature::Creature::getSpriteState() const {
    int state = game::Entity::getSpriteState();

    // Single-orientation animation
    if (state < 1) return state;
    // Multi-orientation animations
    return 1 + (state - 1) * 4 + m_orientation;
}

void game::creature::Creature::move(QPointF direction) {
    move(direction, false);
}
void game::creature::Creature::move(QPointF direction, bool sprint) {
    static const double movementSpeed = 1.4;  // tiles per second
    static const double sprintMultiplier = 1.5;  // %

    if (game::Entity::getSpriteState() != State::Walking) {
        // Prevent walking when not walking
        // return;
    }

    if (game::Entity::getSpriteState() == State::Dead) {
        return;
    }

    // Change animation orientation
    auto oldOrient = m_orientation;
    if (direction.x() < 0) {
        m_orientation = Orientation::Left;
    } else if (direction.x() > 0) {
        m_orientation = Orientation::Right;
    } else if (direction.y() < 0) {
        m_orientation = Orientation::Up;
    } else if (direction.y() > 0) {
        m_orientation = Orientation::Down;
    }

    if (m_orientation != oldOrient) {
        emit spriteStateChanged();
    }

    // Apply movement
    m_movement = direction * movementSpeed * (sprint ? sprintMultiplier : 1);
}

void game::creature::Creature::setActive(bool active) {
    m_isActive = active;
}

int game::creature::Creature::getHealth() const {
    return m_health;
}

int game::creature::Creature::getMaxHealth() const {
    return m_maxHealth;
}

game::Inventory *game::creature::Creature::getInventory() {
    return m_inventory;
}

QVector<game::creature::abilities::Ability *> game::creature::Creature::getAbilities() const {
    return m_abilities;
}

std::set<game::creature::effects::Effect *> game::creature::Creature::getActiveEffects() {
    return m_activeEffects;
}

game::creature::Energy game::creature::Creature::getEnergy() {
    return *m_energy;
}

game::creature::Stats *game::creature::Creature::getStats() {
    return m_stats;
}
game::creature::Stats game::creature::Creature::getFEStats() {
    return *m_stats;
}

QList<game::items::Armor *> game::creature::Creature::getFEquipment() const {
    return m_equipment.values();
}

QMap<game::items::ArmorType, game::items::Armor *> game::creature::Creature::getEquipment() const {
    return m_equipment;
}

game::items::Weapon *game::creature::Creature::getWeapon() const {
    return m_weapon;
}

bool game::creature::Creature::useAbility(int abilityIndex, QPointF targetArea) {
    if (abilityIndex < m_abilities.size()) {
        if (m_abilities.at(abilityIndex)->getRequiredLevel() > m_level) {
            return false;
        } else {
            return m_abilities.at(abilityIndex)->startCasting(this, targetArea, abilityIndex);
        }

    } else {
        return false;
    }
}

void game::creature::Creature::revive() {
    if (m_isAlive) return;

    m_isAlive = true;
    m_isActive = true;
    heal(m_maxHealth);
    setInteraction(Entity::NoInteraction);
}

void game::creature::Creature::heal(int health) {
    if ((m_health + health) > m_maxHealth) {
        m_health = m_maxHealth;
    } else {
        m_health += health;
    }
    emit healthChanged();
    if (health >= (m_maxHealth * 0.05)) {
        emit creatureHealed(health);
    } else {
        emit creatureSmallHealed(health);
    }
}

void game::creature::Creature::damage(items::Damage damage, Creature *damageDealer) {
    game::items::Damage sumArmor{0, 0, 0, 0};
    int finalDamage = 0;
    QMap<items::ArmorType, items::Armor *>::iterator iter = m_equipment.begin();

    while (iter != m_equipment.end()) {
        sumArmor.light += iter.value()->getProtection().light;
        sumArmor.magical += iter.value()->getProtection().magical;
        sumArmor.physical += iter.value()->getProtection().physical;
        sumArmor.shadow += iter.value()->getProtection().shadow;

        iter++;
    }
    // add temp armors (from effects)
    sumArmor.physical += m_stats->tempArmor;
    sumArmor.magical += m_stats->tempMagicArmor;
    sumArmor.light += m_stats->tempLightArmor;
    sumArmor.shadow += m_stats->tempShadowArmor;

    finalDamage += damage.physical - (sumArmor.physical / 3);

    finalDamage += damage.shadow - (sumArmor.shadow / 2);
    finalDamage += damage.magical - (sumArmor.magical / 2);
    finalDamage += damage.light - (sumArmor.light / 2.5);

    calculatedDamage(finalDamage, damageDealer);

    emit healthChanged();
}

void game::creature::Creature::calculatedDamage(int health, Creature *damageDealer) {
    if (m_isAlive) {
        m_health -= health;
        m_actionCheckTimer.restart();
        m_isInAction = true;
        // deactive auto effects

        m_healthRegeneration->changeStatus(false);
        m_energyRegeneration->changeStatus(false);
        emit creatureDamaged(health);

        if (m_health <= 0) {
            m_isAlive = false;
            setSpriteState(State::Dead);
            setInteraction(Entity::OpenInventory);
            emit creatureDied();
            emit m_game->creatureKilled(this, damageDealer);
            m_health = 0;
            // m_isActive = false;
            if (damageDealer != nullptr) damageDealer->gainExpPoints(m_xpReward);
        }
    }
}

bool game::creature::Creature::isFirendly() const {
    return m_isFriendly;
}

bool game::creature::Creature::isStunned() const {
    return m_isStunned;
}

void game::creature::Creature::stunStatusChange() {
    m_isStunned = !m_isStunned;
}

void game::creature::Creature::useItem(items::Item *item, bool inInventory) {
    if (item == nullptr) {
        return;
    }
    if (item->getItemType() == game::items::ItemType::Armor && inInventory) {
        // equip armor
        this->equipArmor(dynamic_cast<items::Armor *>(item));
    }
    if (item->getItemType() == game::items::ItemType::Armor && !inInventory) {
        // unequip armor
        this->unEquipArmor(dynamic_cast<items::Armor *>(item)->getArmorType());
    }
    if (item->getItemType() == game::items::ItemType::Weapon && inInventory) {
        // equip weapon
        this->equipWeapon(dynamic_cast<items::Weapon *>(item));
    }
    if (item->getItemType() == game::items::ItemType::Weapon && !inInventory) {
        // unequip weapon
        this->unEquipWeapon();
    }
    if (item->getItemType() == game::items::ItemType::Consumable && inInventory) {
        // consume potion
        dynamic_cast<items::Potion *>(item)->use(this);
    }
}

void game::creature::Creature::equipArmor(items::Armor *armor) {
    auto slotIt = m_equipment.find(armor->getArmorType());

    if (slotIt != m_equipment.end()) {
        // Slot was empty
        this->unEquipArmor(armor->getArmorType());
    }
    m_equipment.insert(armor->getArmorType(), armor);
    m_inventory->removeItem(armor);

    m_stats->intellect += armor->getStats().intellect;
    m_stats->stamina += armor->getStats().stamina;
    m_stats->strength += armor->getStats().strength;
    m_stats->agility += armor->getStats().agility;
    updateStats();
    emit statsChanged();
    emit equipmentChanged();
}

void game::creature::Creature::unEquipArmor(items::ArmorType slot) {
    auto slotIt = m_equipment.find(slot);

    m_stats->intellect -= slotIt.value()->getStats().intellect;
    m_stats->stamina -= slotIt.value()->getStats().stamina;
    m_stats->strength -= slotIt.value()->getStats().strength;
    m_stats->agility -= slotIt.value()->getStats().agility;
    updateStats();

    if (slotIt != m_equipment.end()) {
        m_inventory->addItem(slotIt.value());
        m_equipment.erase(slotIt);
    }

    emit statsChanged();
    emit equipmentChanged();
}

void game::creature::Creature::equipWeapon(items::Weapon *weapon) {
    if (m_weapon != nullptr) {
        this->unEquipWeapon();
    }
    m_weapon = weapon;
    m_inventory->removeItem(weapon);

    m_stats->intellect += weapon->getStats().intellect;
    m_stats->stamina += weapon->getStats().stamina;
    m_stats->strength += weapon->getStats().strength;
    m_stats->agility += weapon->getStats().agility;
    updateStats();
    emit statsChanged();
    emit weaponChanged();
}

void game::creature::Creature::unEquipWeapon() {
    if (m_weapon != nullptr) {
        m_stats->intellect -= m_weapon->getStats().intellect;
        m_stats->stamina -= m_weapon->getStats().stamina;
        m_stats->strength -= m_weapon->getStats().strength;
        m_stats->agility -= m_weapon->getStats().agility;
        updateStats();

        m_inventory->addItem(m_weapon);
        m_weapon = nullptr;
    }
    emit statsChanged();
    emit weaponChanged();
}

int game::creature::Creature::getMoney() {
    return m_money;
}

void game::creature::Creature::addEffect(effects::Effect *effect) {
    m_activeEffects.insert(effect);
    effect->activateEffect(this);

    connect(effect, &game::creature::effects::Effect::effectEnded, this, &Creature::deleteEffect);
}

void game::creature::Creature::setAbilities(QVector<game::creature::abilities::Ability *> abilities) {
    m_abilities = abilities;
    emit abilitiesChanged();
}

void game::creature::Creature::setExpPoints(int xpPoints) {
    m_expPoints = xpPoints;
    emit expPointsChanged();
}

void game::creature::Creature::setLevel(int level) {
    for (int i = m_level; i < level; i++) {
        levelUp();
        emit statsChanged();
    }
    // m_level = level;
    // emit levelChanged(level);
}

int game::creature::Creature::getLevel() const {
    return m_level;
}

int game::creature::Creature::getExpPoints() const {
    return m_expPoints;
}

void game::creature::Creature::setXpReward(int xpAmount) {
    m_xpReward = xpAmount;
}

bool game::creature::Creature::hasEnoughEnergy(game::creature::EnergyType type, float amount) const {
    if (type == game::creature::EnergyType::None) {
        return true;
    }

    if (m_energy->energyType == type) {
        if ((m_energy->energy - amount) < 0) {
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

void game::creature::Creature::update(double delta) {
    if (!m_isActive) return;

    if (m_isFriendly) {
        if (m_actionCheckTimer.hasExpired(InactivityInterval)) {
            m_isInAction = false;

            m_healthRegeneration->changeStatus(true);
            m_energyRegeneration->changeStatus(true);
            m_actionCheckTimer.restart();
        }
    } else {
        if (m_actionCheckTimer.hasExpired(NonPlayerInactivityInterval)) {
            m_isInAction = false;

            m_healthRegeneration->changeStatus(true);
            m_energyRegeneration->changeStatus(true);
            m_actionCheckTimer.restart();
        }
    }

    if (m_AIController != nullptr) {
        m_AIController->update(delta);
    }

    setPosition(getPosition() + m_movement * delta);

    // If in walking state
    if (game::Entity::getSpriteState() == State::Walking) {
        if (m_movement.isNull()) {
            // If stopped walking, reset the animation
            setSpriteFrame(0);
        } else {
            // Else progress the animation
            qreal distance = sqrt(pow(m_movement.x(), 2) + pow(m_movement.y(), 2));
            if (progressSpriteFrame(distance * delta, false)) {
                // Skip first frame when reseting animation
                setSpriteFrame(1);
            }
        }
    } else {
        // Progress animation
        if (progressSpriteFrame(delta * m_animationSpeed, false)) {
            // Animation ended

            if (!m_isAlive) {
                m_isActive = false;
                return;
            }

            // Set walking
            setSpriteState(State::Walking);

            // Reset speed
            m_animationSpeed = 1;
        }
    }

    // Reset movement
    m_movement = {0, 0};

    for (int i = 0; i < m_abilities.size(); ++i) {
        m_abilities.at(i)->updateTimers(delta);
    }

    for (auto effect : std::vector(m_activeEffects.begin(), m_activeEffects.end())) {
        effect->updateTimers(delta);
    }

    m_healthRegeneration->updateTimers(delta);
    m_energyRegeneration->updateTimers(delta);
}

void game::creature::Creature::saveState(QJsonObject &json) const {
    game::Entity::saveState(json);
    json["orientation"] = m_orientation;
    json["health"] = m_health;
    json["maxHealth"] = m_maxHealth;
    json["exp"] = m_expPoints;
    json["level"] = m_level;
    json["energy_maxEnergy"] = m_energy->maxEnergy;
    json["energy_energy"] = m_energy->energy;
    json["stats_stamina"] = m_stats->stamina;
    json["stats_strenght"] = m_stats->strength;
    json["stats_intelect"] = m_stats->intellect;
    json["stats_agility"] = m_stats->agility;
}

void game::creature::Creature::loadState(QJsonObject const &json) {
    game::Entity::loadState(json);
    m_orientation = Orientation(json.value("orientation").toInt());
    m_health = json.value("health").toInt();
    m_maxHealth = json.value("maxHealth").toInt();
    m_expPoints = json.value("exp").toInt();
    m_level = json.value("level").toInt();
    m_energy->maxEnergy = json.value("energy_maxEnergy").toInt();
    m_energy->energy = json.value("energy_energy").toInt();
    m_stats->strength = json.value("stats_strenght").toInt();
    m_stats->stamina = json.value("stats_stamina").toInt();
    m_stats->intellect = json.value("stats_intelect").toInt();
    m_stats->agility = json.value("stats_agility").toInt();

    emit statsChanged();
    emit healthChanged();
}

void game::creature::Creature::lowerEnergy(game::creature::EnergyType type, float amount) {
    if (m_energy->energyType == type) {
        if (hasEnoughEnergy(type, amount)) {
            m_energy->energy -= amount;
            emit energyChanged();
        } else {
        }
    }
}

void game::creature::Creature::gainEnergy(game::creature::EnergyType type, float amount) {
    if (m_energy->energyType == type) {
        if ((m_energy->energy + amount) > m_energy->maxEnergy) {
            m_energy->energy = m_energy->maxEnergy;
        } else {
            m_energy->energy += amount;
        }
        emit energyChanged();
    }
}

void game::creature::Creature::gainExpPoints(int xpPoints) {
    if (xpPoints + m_expPoints >= data::LEVELS_XP.at(m_level - 1)) {
        // 30/100, 350
        int xpPointsNeededForNextLevel = data::LEVELS_XP.at(m_level - 1) - m_expPoints;
        xpPoints -= xpPointsNeededForNextLevel;
        m_expPoints = 0;
        levelUp();
        emit expPointsChanged();
        if (xpPoints != 0) {
            gainExpPoints(xpPoints);
        }

    } else {
        m_expPoints += xpPoints;
        emit expPointsChanged();
    }

    while (m_expPoints >= data::LEVELS_XP.at(m_level - 1)) {
        m_expPoints = 0;
        m_level += 1;
        levelUp();
        emit statsChanged();
    }
}

void game::creature::Creature::modifyCreature(
  int health,
  QVector<game::creature::abilities::Ability *> abilities,
  game::items::Weapon *weapon,
  QMap<game::items::ArmorType, game::items::Armor *> equipment
) {
    m_maxHealth = health;
    heal(health);
    emit maxHealthChanged();
    m_abilities = abilities;
    m_weapon = weapon;
    m_equipment = equipment;
}

void game::creature::Creature::setHealth(int health) {
    if (health > m_maxHealth) {
        m_health = m_maxHealth;
    } else {
        m_health = health;
    }
    emit healthChanged();
}

void game::creature::Creature::setEnergy(int energy) {
    if (energy > m_energy->maxEnergy) {
        m_energy->energy = m_energy->maxEnergy;
    } else {
        m_energy->energy = energy;
    }
    emit energyChanged();
}

bool game::creature::Creature::getCastingSTatus() const {
    return m_isCasting;
}

void game::creature::Creature::setCastingStatus(bool isCasting) {
    m_isCasting = isCasting;
}

void game::creature::Creature::setAIController(game::creature::AIController *controller) {
    if (controller != nullptr) controller->setControlledCreature(this);
    m_AIController = controller;
}

game::creature::AIController *game::creature::Creature::getAIController() const {
    return m_AIController;
}

bool game::creature::Creature::isActive() const {
    return m_isActive;
}

game::core::Game *game::creature::Creature::getGame() const {
    return m_game;
}
bool game::creature::Creature::getAlive() {
    return m_isAlive;
}

void game::creature::Creature::resetCastedAbilityIndex() {
    m_castedAbilityIndex = -1;
    emit castingAbilityChanged();
}

void game::creature::Creature::deleteEffect(creature::effects::Effect *effect) {
    m_activeEffects.erase(effect);
}

int game::creature::Creature::xpNeededForNextLevel() {
    try {
        return data::LEVELS_XP.at(m_level - 1);
    } catch (std::out_of_range) {
        // :P
        return 99999999;
    }
}
