#include "Civilian.h"

#include "src/game/core/Game.h"

game::creature::Civilian::Civilian(game::core::Game* game, game::EntityParams entityParams, QList<data::QuestId> quests)
  : game::creature::Creature(game, entityParams, 0, new game::Inventory(0, 0), new Stats{}, true, new Energy{}, 0),
    m_quests(quests) {
    m_isAlive = false;  // prevent damage
    setSpriteFrame(0);  // stand up
    setInteraction(Entity::Dialogue);  // enable dialogue

    connect(
      m_game->getQuestManager(),
      &game::quests::QuestManager::activeQuestsChanged,
      this,
      &Civilian::availableQuestsChanged
    );
}

QList<game::quests::QuestParams> game::creature::Civilian::getAvailableQuests() const {
    QList<game::quests::QuestParams> list;

    auto questManager = m_game->getQuestManager();
    for (auto questId : m_quests) {
        if (questManager->canStartQuest(questId)) {
            list.append(questManager->getQuestParams(questId));
        }
    }

    return list;
}

QList<game::quests::QuestParams> game::creature::Civilian::getActiveQuests() const {
    QList<game::quests::QuestParams> list;

    auto questManager = m_game->getQuestManager();
    for (auto questId : m_quests) {
        if (questManager->isQuestActive(questId)) {
            list.append(questManager->getQuestParams(questId));
        }
    }

    return list;
}

void game::creature::Civilian::update(double delta) {
    // prevent thinking
}

void game::creature::Civilian::levelUp() {}
void game::creature::Civilian::updateStats() {}
