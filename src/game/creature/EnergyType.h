//
// Created by xkapp on 14.11.2023.
//

#ifndef REALMOFTEMPORALITY_ENERGYTYPE_H
#define REALMOFTEMPORALITY_ENERGYTYPE_H

namespace game::creature {
    enum class EnergyType {
        Energy,
        Mana,
        Rage,
        None
    };

}


#endif //REALMOFTEMPORALITY_ENERGYTYPE_H
