//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_CREATURE_H
#define REALMOFTEMPORALITY_CREATURE_H
#include <QElapsedTimer>
#include <QMap>
#include <QPoint>
#include <QVector>
#include <string>
#include <vector>

#include "../Inventory.h"
#include "../items/Armor.h"
#include "../items/Weapon.h"
#include "AIController.h"
#include "Energy.h"
#include "Stats.h"
#include "abilities/Ability.h"
#include "effects/Effect.h"
#include "game/creature/effects/autoEffects/AutoEnergyEffect.h"
#include "game/creature/effects/autoEffects/AutoHealthRegen.h"
#include "src/data/CreatureFactory.h"
#include "src/data/levels.h"
#include "src/game/Entity.h"
#include <set>

namespace game::creature {
    class Creature : public Entity {
        Q_OBJECT
        Q_PROPERTY(game::Inventory* inventory READ getInventory CONSTANT);
        Q_PROPERTY(int health READ getHealth NOTIFY healthChanged);
        Q_PROPERTY(int maxHealth READ getMaxHealth NOTIFY maxHealthChanged);
        Q_PROPERTY(int isFriendly READ isFirendly CONSTANT);
        Q_PROPERTY(game::creature::Energy energy READ getEnergy NOTIFY energyChanged);
        Q_PROPERTY(int level READ getLevel NOTIFY levelChanged);
        Q_PROPERTY(int expPoints READ getExpPoints NOTIFY expPointsChanged);
        Q_PROPERTY(QVector<game::creature::abilities::Ability*> abilities READ getAbilities NOTIFY abilitiesChanged);
        Q_PROPERTY(QList<items::Armor*> equipment READ getFEquipment NOTIFY equipmentChanged);
        Q_PROPERTY(bool isAlive READ getAlive NOTIFY healthChanged);
        Q_PROPERTY(int castedAbilityIndex MEMBER m_castedAbilityIndex NOTIFY castingAbilityChanged);
        Q_PROPERTY(int xpNeededForNextLevel READ xpNeededForNextLevel NOTIFY levelChanged);
        Q_PROPERTY(items::Weapon* weapon READ getWeapon NOTIFY weaponChanged);
        Q_PROPERTY(game::creature::Stats stats READ getFEStats NOTIFY statsChanged);

        QPointF m_movement;
        /// @brief Whether the creature should be updating
        bool m_isActive = true;

      public:
        enum State { Dead, Walking, Hitting, Casting, Slash };
        enum Orientation { Up, Left, Down, Right };

      protected:
        /// @brief what way is the creature looking
        Orientation m_orientation = Orientation::Down;
        int m_health;
        game::Inventory* m_inventory;
        int m_level;
        int m_expPoints;
        QVector<abilities::Ability*> m_abilities;
        std::set<effects::Effect*> m_activeEffects;
        effects::AutoHealthRegen* m_healthRegeneration;
        effects::AutoEnergyEffect* m_energyRegeneration;
        game::creature::Energy* m_energy;
        items::Weapon* m_weapon;
        QMap<items::ArmorType, items::Armor*> m_equipment;
        bool m_isFriendly;
        int m_money;
        bool m_isStunned;
        bool m_isCasting;
        int m_castedAbilityIndex;
        bool m_isAlive;
        int m_xpReward;
        Stats* m_stats;
        int m_maxHealth;
        bool m_canBeAsleep;
        bool m_isInAction;
        AIController* m_AIController;
        QElapsedTimer m_actionCheckTimer;

        static const qint64 InactivityInterval = 15 * 1000;  // temp 15000
        static const qint64 NonPlayerInactivityInterval = 25 * 1000;

        /// @brief speed of animation, ignored for walk animation
        float m_animationSpeed;

        int getSpriteState() const override;

      public:
        Creature(
          game::core::Game* game,
          game::EntityParams entityParams,
          int maxHealth,
          game::Inventory* inv,
          Stats* stats,
          bool isFriendly,
          Energy* energy,
          int money
        );

        void move(QPointF direction);
        void move(QPointF direction, bool sprint);
        void setActive(bool active);

        // geters
        Orientation getOrientation() const;
        int getHealth() const;
        game::Inventory* getInventory();
        QVector<game::creature::abilities::Ability*> getAbilities() const;
        std::set<effects::Effect*> getActiveEffects();
        Energy getEnergy();
        Stats* getStats();
        Stats getFEStats();
        int getMaxHealth() const;
        bool getCastingSTatus() const;
        // Equipment for front end visualization in list (not in map)
        QList<items::Armor*> getFEquipment() const;
        QMap<items::ArmorType, items::Armor*> getEquipment() const;
        Q_INVOKABLE items::Weapon* getWeapon() const;
        int getMoney();
        int getLevel() const;
        int getExpPoints() const;
        bool isFirendly() const;
        bool isStunned() const;
        bool hasEnoughEnergy(EnergyType type, float amount) const;
        bool isActive() const;
        game::core::Game* getGame() const;
        int xpNeededForNextLevel();

        // seters
        void setAbilities(QVector<game::creature::abilities::Ability*> abilities);
        void setExpPoints(int xpPoints);
        void setLevel(int level);
        void modifyCreature(
          int health,
          QVector<game::creature::abilities::Ability*> abilities,
          game::items::Weapon* weapon,
          QMap<game::items::ArmorType, game::items::Armor*> equipment
        );
        void setXpReward(int xpAmount);
        void setHealth(int health);
        void setEnergy(int energy);
        void setCastingStatus(bool isCasting);
        void setAIController(AIController* controller);
        AIController* getAIController() const;

        // actions
        Q_INVOKABLE void useItem(items::Item *item, bool inInventory);
        Q_INVOKABLE bool useAbility(int abilityIndex, QPointF targetArea);  // set to bool later
        void revive();
        void heal(int health);
        void damage(items::Damage damage, Creature* damageDealer);
        void calculatedDamage(int health, Creature* damageDealer);
        void equipWeapon(items::Weapon* weapon);
        void unEquipWeapon();
        void equipArmor(items::Armor* armor);
        void unEquipArmor(items::ArmorType slot);
        void stunStatusChange();
        void addEffect(effects::Effect* effect);
        void lowerEnergy(EnergyType type, float amount);
        void gainEnergy(EnergyType type, float amount);
        void gainExpPoints(int xpPoints);
        bool getAlive();
        virtual void levelUp() = 0;
        virtual void updateStats() = 0;
        void resetCastedAbilityIndex();
        void deleteEffect(creature::effects::Effect* effect);

        virtual void update(double delta) override;
        virtual void saveState(QJsonObject& json) const override;
        virtual void loadState(QJsonObject const& json) override;

        // freens
        friend bool game::creature::abilities::Ability::startCasting(
          game::creature::Creature* caster,
          QPointF targetPosition,
          int abilityIndex
        );
        friend void game::creature::abilities::Ability::changeCatserSpriteState(std::string state);

      signals:
        void healthChanged();
        void maxHealthChanged();
        void energyChanged();
        void levelChanged(int level);
        void expPointsChanged();
        void abilitiesChanged();
        void equipmentChanged();
        void weaponChanged();
        void creatureDied();
        void creatureSmallHealed(int vlaue);
        void creatureHealed(int value);
        void creatureDamaged(int vlaue);
        void creatureDamagedBy(int damage, Creature* damageDealer, Creature* damageDealtTo);
        void castingAbilityChanged();
        void statsChanged();
    };

}  // namespace game::creature
#endif  // REALMOFTEMPORALITY_CREATURE_H
