//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_ENERGY_H
#define REALMOFTEMPORALITY_ENERGY_H
#include <string>
#include "EnergyType.h"

namespace game::creature {
    struct Energy {
        Q_GADGET

        Q_PROPERTY(int energy MEMBER energy);
        Q_PROPERTY(game::creature::EnergyType energyType MEMBER energyType);
        Q_PROPERTY(int maxEnergy MEMBER maxEnergy);
        Q_PROPERTY(std::string energyName MEMBER energyName);
        
      public:
        int energy;
        game::creature::EnergyType energyType;
        int maxEnergy;
        std::string energyName;
    };
}

#endif //REALMOFTEMPORALITY_ENERGY_H
