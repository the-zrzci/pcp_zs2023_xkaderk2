//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_STATS_H
#define REALMOFTEMPORALITY_STATS_H
#include <QObject>

namespace game::creature {
    struct Stats {
        Q_GADGET

        Q_PROPERTY(int strength MEMBER strength CONSTANT);
        Q_PROPERTY(int stamina MEMBER stamina CONSTANT);
        Q_PROPERTY(int intellect MEMBER intellect CONSTANT);
        Q_PROPERTY(int agility MEMBER agility CONSTANT);
        Q_PROPERTY(float movementSpeed MEMBER movementSpeed CONSTANT);
        Q_PROPERTY(int tempArmor MEMBER tempArmor CONSTANT);
        Q_PROPERTY(int tempMagicArmor MEMBER tempMagicArmor CONSTANT);
        Q_PROPERTY(int tempLightArmor MEMBER tempLightArmor CONSTANT);
        Q_PROPERTY(int tempShadowArmor MEMBER tempShadowArmor CONSTANT);

        
    public:
        int strength;
        int stamina;
        int intellect;
        int agility;
        float movementSpeed;
        int tempArmor;
        int tempMagicArmor;
        int tempLightArmor;
        int tempShadowArmor;
    };
}


#endif //REALMOFTEMPORALITY_STATS_H
