#ifndef REALMOFTEMPORALITY_CIVILIAN_H
#define REALMOFTEMPORALITY_CIVILIAN_H

#include <QList>
#include <QString>

#include "Creature.h"
#include "src/game/quests/Quest.h"

namespace data {
    enum class QuestId;
}

namespace game::creature {
    class Civilian : public Creature {
        Q_OBJECT;

        Q_PROPERTY(QList<game::quests::QuestParams> quests READ getAvailableQuests NOTIFY availableQuestsChanged);
        Q_PROPERTY(QList<game::quests::QuestParams> activeQuests READ getActiveQuests NOTIFY availableQuestsChanged);

        QList<data::QuestId> m_quests;

      public:
        Civilian(game::core::Game* game, game::EntityParams entityParams, QList<data::QuestId> quests);

        QList<game::quests::QuestParams> getAvailableQuests() const;
        QList<game::quests::QuestParams> getActiveQuests() const;

        void update(double delta) override;
        void levelUp() override;
        void updateStats() override;

      signals:
        void availableQuestsChanged();
    };
}  // namespace game::creature

#endif  // REALMOFTEMPORALITY_CIVILIAN_H
