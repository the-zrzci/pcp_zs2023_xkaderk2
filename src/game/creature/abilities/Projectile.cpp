//
// Created by xkapp on 19.12.2023.
//

#include "Projectile.h"

#include "src/game/core/Game.h"

game::creature::abilities::Projectile::Projectile(
  QPointF directionVector,
  qreal speed,
  game::core::Game* game,
  game::EntityParams entityParams,
  game::creature::abilities::Ability* sourceAbility
)
  : Entity(game, entityParams, game::Entity::Type::Sensor) {
    m_directionVector = directionVector;
    m_speed = speed;
    m_sourceAbility = sourceAbility;
    m_traveledDistance = 0;
}

void game::creature::abilities::Projectile::update(double delta) {
    // Apply movement
    setPosition(getPosition() + (m_directionVector * m_speed * delta));
    // Progress animation
    progressSpriteFrame(delta * m_speed);

    // Keep track of range
    m_traveledDistance += (delta * m_speed);

    if (m_traveledDistance >= m_sourceAbility->getRange()) {
        scheduleDelete();
    }
}

void game::creature::abilities::Projectile::onCollision(const std::vector<Entity*>& entities) {
    if (entities.size() == 0) {
        // We collided with tiles, die
        scheduleDelete();
    }

    // Ignore if we only collided with our caster
    else if (entities.size() > 1 || entities[0] != m_sourceAbility->getCaster()) {
        if (m_sourceAbility->getAoeRange() == 0) {
            // no aoe (arrow etc)
            std::vector<creature::Creature*> targets;
            for (auto entity : entities) {
                if (entity->getType() == game::Entity::Type::Creature) {
                    targets.push_back(qobject_cast<creature::Creature*>(entity));
                }
            }

            m_sourceAbility->setTargets(targets);

        } else {
            m_sourceAbility->setTargets(
              m_game->getCurrentMap()->findCreaturesInRange(this, m_sourceAbility->getAoeRange())
            );
        }

        m_sourceAbility->hit();
        scheduleDelete();
    }
}
