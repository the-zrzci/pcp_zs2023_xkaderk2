//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_ABILITY_H
#define REALMOFTEMPORALITY_ABILITY_H
#include <QObject>
#include <QPointF>
#include <string>
#include <vector>

#include "../EnergyType.h"
#include "../effects/Effect.h"
#include "data/levels.h"
#include "data/constants.h"

namespace game::creature {
    class Creature;
}

namespace game::core {
    class Game;
}

namespace game::creature::abilities {
    class Ability : public QObject {
        Q_OBJECT

        Q_PROPERTY(QString name READ getName CONSTANT);
        Q_PROPERTY(QString description READ getDescription CONSTANT);
        Q_PROPERTY(bool ranged MEMBER m_ranged CONSTANT);
        Q_PROPERTY(float range MEMBER m_range CONSTANT);
        Q_PROPERTY(float aoeRange MEMBER m_aoeRange CONSTANT);
        Q_PROPERTY(int requiredLevel MEMBER m_requiredLevel CONSTANT);
        Q_PROPERTY(float castingTime MEMBER m_castingTime CONSTANT);
        Q_PROPERTY(float castingTimer MEMBER m_castingTimer NOTIFY updatedCasting);
        Q_PROPERTY(float cooldown MEMBER m_cooldown CONSTANT);
        Q_PROPERTY(float cooldownTimer MEMBER m_cooldownTimer NOTIFY updatedCooldown);
        Q_PROPERTY(int energyCost MEMBER m_energyCost CONSTANT);
        Q_PROPERTY(QString energyType READ getEnergyTypeName CONSTANT);
        Q_PROPERTY(bool requiresWeapon MEMBER m_requiresWeapon CONSTANT);
        Q_PROPERTY(QString icon MEMBER m_pathToIcon CONSTANT);

      private:
        QString m_name;
        QString m_description;
        bool m_ranged;  //
        // int m_aoeTargetLimit; //
        int m_requiredLevel;
        float m_castingTime;
        float m_castingTimer;
        float m_cooldown;
        float m_cooldownTimer;
        bool m_isReady;
        bool m_isBeingCasted;
        int m_abilityBarIndex;

      protected:
        creature::Creature* m_caster;
        std::vector<creature::Creature*> m_targets;
        EnergyType m_requiredEnergyType;
        int m_energyCost;
        bool m_requiresWeapon;
        game::core::Game* m_game;
        QPointF m_targetPosition;
        float m_range;  //
        float m_aoeRange;  // if 0, ability does not have aoe
        QString m_pathToIcon;

      public:
        Ability(
          game::core::Game* game,
          QString name,
          QString description,
          int requiredLevel,
          EnergyType requiredEnergyType,
          int energyCost,
          float castingTime,
          float cooldown,
          bool ranged,
          bool requiresWeapon,
          float range,
          float aoeRange
        );

        // virtual bool useAbility(creature::Creature* caster, std::vector<creature::Creature*> targets);
        QString getName();
        QString getDescription();
        int getRequiredLevel();
        EnergyType getEnergyType();
        QString getEnergyTypeName();
        int getEnergyCost();
        void updateTimers(double delta);
        Q_INVOKABLE void updateTargetPosition(QPointF targetPosition);
        Creature* getCaster();
        float getRange();
        float getAoeRange();
        void setTargets(std::vector<creature::Creature*> targets);
        int getResultingEnergyCost();
        void changeCatserSpriteState(std::string state);

        virtual bool startCasting(creature::Creature* caster, QPointF targetPosition, int abilityIndex);
        virtual void cast() = 0;
        virtual void hit() = 0;

      signals:
        void startedCasting();
        void finishedCasting();
        void interuptedCasting();
        void updatedCasting();
        void updatedCooldown();
    };
}  // namespace game::creature::abilities

#endif  // REALMOFTEMPORALITY_ABILITY_H
