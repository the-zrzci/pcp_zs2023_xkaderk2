//
// Created by xkapp on 28.01.2024.
//

#include "Fireball.h"
#include "game/creature/Creature.h"

game::creature::abilities::Fireball::Fireball(game::core::Game *game) :
        Ability(game,"Fireball","Shoots a masive fireball that burns the enemy",2,EnergyType::Mana,19,1.5,6,true,true,5.5,1.8) {
    m_pathToIcon = data::ASSET_PREFIX + "spells/fireball.jpg";
}

void game::creature::abilities::Fireball::cast() {
    QPointF direction = m_targetPosition - m_caster->getPosition();
    direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));

    auto projectile = new Projectile(
            direction,
            8,
            m_game,
            {
                    "Fireball",
                    {0.35, 0.35},
                    {
                            data::ASSET_PREFIX + "vfx/fireball.png",
                            .frameSize = {512, 512},
                            .hitboxOffset = {232, 232},
                            .stateFrameCount = {6},
                            .stateFrameFrequency = {2},
                    },
            },
            this
    );

    projectile->setPosition(m_caster->getPosition());
    m_game->getCurrentMap()->addEntity(projectile);

    m_caster->lowerEnergy(m_requiredEnergyType, m_caster->getEnergy().maxEnergy * (m_energyCost-data::BASIC_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
}

void game::creature::abilities::Fireball::hit() {
    auto staff = m_caster->getWeapon();

    int magicDmg = ((m_caster->getStats()->intellect * 2) + staff->getStats().intellect*1.4) + staff->getDamages().magical;
    for (auto target: m_targets) {
        target->damage(items::Damage{0, magicDmg, 0, 0},m_caster);
        //add flameEffect
    }

}