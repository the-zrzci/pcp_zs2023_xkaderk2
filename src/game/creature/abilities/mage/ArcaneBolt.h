//
// Created by xkapp on 20.11.2023.
//

#ifndef REALMOFTEMPORALITY_ARCANEBOLT_H
#define REALMOFTEMPORALITY_ARCANEBOLT_H
#include "../../Creature.h"
#include "../Projectile.h"
#include "src/data/constants.h"
#include "src/game/core/Game.h"

namespace game::creature::abilities {
    class ArcaneBolt : public Ability {

    public:
        ArcaneBolt(game::core::Game* game);

        void cast() override;
        void hit() override;
    };

}



#endif  // REALMOFTEMPORALITY_ARCANEBOLT_H
