//
// Created by xkapp on 28.01.2024.
//
#include "ArcaneBolt.h"
#include "game/creature/Creature.h"


game::creature::abilities::ArcaneBolt::ArcaneBolt(game::core::Game *game) :
        Ability(game, "Arcane Bolt","Weak manifestation of Arcane magic",0,EnergyType::Mana,10,0,4,true,true,5.5,0){
    m_pathToIcon = data::ASSET_PREFIX + "spells/arcane_bolt.jpg";
}

void game::creature::abilities::ArcaneBolt::cast() {
    QPointF direction = m_targetPosition - m_caster->getPosition();
    direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));

    auto projectile = new Projectile(
            direction,
            8,
            m_game,
            {
                    "Arcane Bolt",
                    {0.2, 0.2},
                    {
                            data::ASSET_PREFIX + "vfx/arcaneBolt.png",
                            .frameSize = {512, 512},
                            .hitboxOffset = {232, 232},
                            .stateFrameCount = {6},
                            .stateFrameFrequency = {2},
                    },
            },
            this
    );

    projectile->setPosition(m_caster->getPosition());
    m_game->getCurrentMap()->addEntity(projectile);

    m_caster->lowerEnergy(m_requiredEnergyType, m_caster->getEnergy().maxEnergy * (m_energyCost-data::BASIC_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
    changeCatserSpriteState("Hitting");
}

void game::creature::abilities::ArcaneBolt::hit() {
    auto staff = m_caster->getWeapon();

    if (staff == nullptr) {
        for (auto target: m_targets) {
            target->damage(items::Damage{0, static_cast<int>(m_caster->getStats()->intellect*1.3), 0, 0},m_caster);
        }

    } else {
        int magicDmg = ((m_caster->getStats()->intellect * 1.4) + staff->getStats().intellect) + staff->getDamages().magical;
        for (auto target: m_targets) {
            target->damage(items::Damage{0, magicDmg, 0, 0},m_caster);
        }
    }
}