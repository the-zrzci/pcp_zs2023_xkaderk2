//
// Created by xkapp on 20.11.2023.
//

#ifndef REALMOFTEMPORALITY_PROJECTILE_H
#define REALMOFTEMPORALITY_PROJECTILE_H
#include "../../Entity.h"
#include "Ability.h"

namespace game::creature::abilities {

    class Projectile : public Entity {
        Q_OBJECT;

        Q_PROPERTY(QPointF direction MEMBER m_directionVector CONSTANT);

        QPointF m_directionVector;
        qreal m_speed;  // speed (traveled in one second)
        qreal m_traveledDistance;
        // texture
        // particles
        game::creature::abilities::Ability* m_sourceAbility;

      public:
        Projectile(
          QPointF directionVector,
          qreal speed,
          game::core::Game* game,
          game::EntityParams entityParams,
          game::creature::abilities::Ability* sourceAbility
        );

        void update(double delta) override;
        void onCollision(const std::vector<Entity*>& entities) override;
    };

}  // namespace game::creature::abilities

#endif  // REALMOFTEMPORALITY_PROJECTILE_H
