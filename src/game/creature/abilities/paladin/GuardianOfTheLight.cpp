//
// Created by xkapp on 28.01.2024.
//
#include "GuardianOfTheLight.h"
#include "game/creature/Creature.h"


game::creature::abilities::GuardianOfTheLight::GuardianOfTheLight(
        game::core::Game *game) : Ability(game,"Guardian of The Light",
                                          "Gives you temporal boost to armor and health regenration",4,
                                          EnergyType::Mana,35,0,24,false,
                                          false,0,0){}


void game::creature::abilities::GuardianOfTheLight::cast() {
    changeCatserSpriteState("Casting");
    m_caster->lowerEnergy(EnergyType::Mana, m_caster->getEnergy().maxEnergy * (m_energyCost-data::ULTIMATE_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
    hit();

}

void game::creature::abilities::GuardianOfTheLight::hit() {
    //add efects to the caster
}