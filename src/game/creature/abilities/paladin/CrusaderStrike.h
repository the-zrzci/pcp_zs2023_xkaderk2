//
// Created by xkapp on 18.11.2023.
//

#ifndef REALMOFTEMPORALITY_CRUSADERSTRIKE_H
#define REALMOFTEMPORALITY_CRUSADERSTRIKE_H
#include "../Ability.h"

namespace game::creature::abilities {
    
    class CrusaderStrike : public Ability {
        
      public:
        CrusaderStrike(game::core::Game* game);
        void hit() override;
        void cast() override;
    };
}
#endif  // REALMOFTEMPORALITY_CRUSADERSTRIKE_H
