//
// Created by xkapp on 27.01.2024.
//

#ifndef REALMOFTEMPORALITY_HOLYLIGHT_H
#define REALMOFTEMPORALITY_HOLYLIGHT_H

#include "../Ability.h"

namespace game::creature::abilities {

    class HolyLight : public Ability {

    public:
        HolyLight(game::core::Game* game);
        void hit() override;
        void cast() override;
    };
}
#endif //REALMOFTEMPORALITY_HOLYLIGHT_H
