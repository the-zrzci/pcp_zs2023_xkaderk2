//
// Created by xkapp on 28.01.2024.
//

#ifndef REALMOFTEMPORALITY_GUARDIANOFTHELIGHT_H
#define REALMOFTEMPORALITY_GUARDIANOFTHELIGHT_H
#include "../Ability.h"


namespace game::creature::abilities {

    class GuardianOfTheLight : public Ability {

    public:
        GuardianOfTheLight(game::core::Game* game);
        void hit() override;
        void cast() override;
    };
}

#endif //REALMOFTEMPORALITY_GUARDIANOFTHELIGHT_H
