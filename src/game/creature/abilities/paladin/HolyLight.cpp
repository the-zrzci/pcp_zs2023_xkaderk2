//
// Created by xkapp on 27.01.2024.
//

#include "HolyLight.h"
#include "../../../items/Weapon.h"
#include "../../Creature.h"
#include "src/game/core/Game.h"
#include "data/entities.h"

game::creature::abilities::HolyLight::HolyLight(game::core::Game *game) : Ability(game,"Holy Light","Heal yourselft with the holy light itself",2,
                                                                                  creature::EnergyType::Mana,
                                                                                  25,1.4,10,false,false,0,0){
    m_pathToIcon = data::ASSET_PREFIX + "spells/holy_light.jpg";

}

void game::creature::abilities::HolyLight::cast() {
    m_caster->lowerEnergy(creature::EnergyType::Mana,m_caster->getEnergy().maxEnergy * (m_energyCost-data::ADAVANCED_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
    hit();
}

void game::creature::abilities::HolyLight::hit() {

    m_caster->heal(m_caster->getMaxHealth() * 0.28);

}