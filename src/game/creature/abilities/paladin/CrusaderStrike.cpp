//
// Created by xkapp on 18.11.2023.
//
#include "CrusaderStrike.h"
#include "../../../items/Weapon.h"
#include "../../Creature.h"
#include "src/game/core/Game.h"
#include "data/entities.h"

game::creature::abilities::CrusaderStrike::CrusaderStrike(game::core::Game* game)
    : Ability(game,"Crusader Strike", "Strike the enemy with holy light", 0, EnergyType::Mana, 15, 0, 8,
              false,true,1,0) {
    m_pathToIcon = data::ASSET_PREFIX + "spells/crusader_strike.jpg";
}

void game::creature::abilities::CrusaderStrike::cast() {
    m_targets = m_game->getCurrentMap()->findCreaturesInRange(m_caster, 1.5, 90);
    m_caster->lowerEnergy(m_requiredEnergyType, m_caster->getEnergy().maxEnergy * (m_energyCost-data::BASIC_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
    changeCatserSpriteState("Hitting");
    hit();
}

void game::creature::abilities::CrusaderStrike::hit() {
    auto weapon = m_caster->getWeapon();


        items::Damage damage = items::Damage{weapon->getDamages().physical,weapon->getDamages().light,
            (int)(m_caster->getStats()->intellect / 0.7),(int)(m_caster->getStats()->strength / 0.2)};

        for (int i = 0; i < m_targets.size(); ++i) {
            m_targets.at(i)->damage(damage,m_caster);
        }

}


