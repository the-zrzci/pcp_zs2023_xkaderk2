//
// Created by xkapp on 14.11.2023.
//

#include "Ability.h"

#include <iostream>

#include "../Creature.h"

game::creature::abilities::Ability::Ability(
  game::core::Game *game,
  QString name,
  QString description,
  int requiredLevel,
  EnergyType requiredEnergyType,
  int energyCost,
  float castingTime,
  float cooldown,
  bool ranged,
  bool requiresWeapon,
  float range,
  float aoeRange
) {
    m_game = game;
    m_name = name;
    m_description = description;
    m_requiredLevel = requiredLevel;
    m_requiredEnergyType = requiredEnergyType;
    m_energyCost = energyCost;
    m_castingTime = castingTime;
    m_cooldown = cooldown;
    m_castingTimer = 0;
    m_cooldownTimer = 0;
    m_ranged = ranged;
    m_range = range;
    m_aoeRange = aoeRange;
    m_requiresWeapon = requiresWeapon;
    m_isReady = true;
    m_targetPosition = {0, 0};
    m_abilityBarIndex = -1;
    m_pathToIcon = "";
}

QString game::creature::abilities::Ability::getName() {
    return m_name;
}

QString game::creature::abilities::Ability::getDescription() {
    return m_description;
}

int game::creature::abilities::Ability::getRequiredLevel() {
    return m_requiredLevel;
}

int game::creature::abilities::Ability::getEnergyCost() {
    return m_energyCost;
}

game::creature::EnergyType game::creature::abilities::Ability::getEnergyType() {
    return m_requiredEnergyType;
}

QString game::creature::abilities::Ability::getEnergyTypeName() {
    switch (m_requiredEnergyType) {
        case game::creature::EnergyType::Energy:
            return "Energy";
        case game::creature::EnergyType::Mana:
            return "Mana";
        case game::creature::EnergyType::Rage:
            return "Rage";
    }
    return "None";
}

// bool game::creature::abilities::Ability::useAbility(creature::Creature *caster, std::vector<creature::Creature *>) {}

void game::creature::abilities::Ability::updateTimers(double delta) {
    if (m_cooldownTimer > 0) {
        m_cooldownTimer -= delta;
        emit updatedCooldown();

        if (m_cooldownTimer <= 0) {
            m_isReady = true;
            emit updatedCooldown();
            m_cooldownTimer = 0;
        }
    }

    if (m_castingTimer > 0 and m_isBeingCasted and m_caster->getCastingSTatus()) {
        m_castingTimer -= delta;

        if (m_castingTimer <= 0) {
            m_isBeingCasted = false;
            m_castingTimer = 0;
            emit finishedCasting();
            m_caster->setCastingStatus(false);
            m_caster->resetCastedAbilityIndex();
            cast();  // vystrel
            if (m_cooldown != 0) {
                m_cooldownTimer = m_cooldown;
                emit updatedCooldown();
                m_isReady = false;
            }
        }
    } else {  // testing
        emit interuptedCasting();
        m_isBeingCasted = false;
        m_castingTimer = 0;
    }
    emit updatedCasting();
}

bool game::creature::abilities::Ability::startCasting(
  game::creature::Creature *caster,
  QPointF targetPosition,
  int abilityIndex
) {
    m_abilityBarIndex = abilityIndex;
    if (m_requiresWeapon and caster->getWeapon() == nullptr) {  // check wheter the ability requires weapon and if the
        // player has weapon
        return false;

    } else {
        if (m_isReady and !m_isBeingCasted) {  // if there is no cooldown currently, and the ability is not being casted
            m_targetPosition = targetPosition;

            if (caster->hasEnoughEnergy(m_requiredEnergyType, m_energyCost)) {
                if (m_castingTime != 0) {  // checking whether the ability requires casting process
                    m_castingTimer = m_castingTime;  // seting up casting procces
                    m_isBeingCasted = true;
                    m_caster = caster;
                    // after casting process finishes, the cast()is called (in update timers func)
                    m_caster->setSpriteState(game::creature::Creature::State::Casting);  // change animation to cast
                    emit startedCasting();
                    emit updatedCasting();
                    m_caster->setCastingStatus(true);
                    m_caster->m_castedAbilityIndex = m_abilityBarIndex;
                    emit m_caster->castingAbilityChanged();
                    return true;
                } else {  // if ability has no casting time then the ability is immidiately cast

                    m_caster = caster;
                    cast();
                    if (m_cooldown != 0) {
                        m_cooldownTimer = m_cooldown;
                        emit updatedCooldown();
                        m_isReady = false;
                    }

                    return true;
                }
            }
        }
    }
    // if the ability has running cooldown, it cannot be called, so it returns false
    return false;
}



void game::creature::abilities::Ability::updateTargetPosition(QPointF targetPosition) {
    m_targetPosition = targetPosition;
}

game::creature::Creature *game::creature::abilities::Ability::getCaster() {
    return m_caster;
}

float game::creature::abilities::Ability::getRange() {
    return m_range;
}

float game::creature::abilities::Ability::getAoeRange() {
    return m_aoeRange;
}

void game::creature::abilities::Ability::setTargets(std::vector<creature::Creature *> targets) {
    m_targets = targets;
}

int game::creature::abilities::Ability::getResultingEnergyCost() {
    return m_caster->getEnergy().maxEnergy *
           (m_energyCost - data::BASIC_SPELL_COST_MODIFIER.at(m_caster->getLevel() - 1)) / 100;
}

void game::creature::abilities::Ability::changeCatserSpriteState(std::string state) {
    //{ Dead, Walking, Hitting, Casting, Slash };
    if (state == "Casting") {
        m_caster->setSpriteState(game::creature::Creature::State::Casting);
    } else if (state == "Hitting") {
        m_caster->setSpriteState(game::creature::Creature::State::Hitting);
    } else if (state == "Slash") {
        m_caster->setSpriteState(game::creature::Creature::State::Slash);
    }
}
