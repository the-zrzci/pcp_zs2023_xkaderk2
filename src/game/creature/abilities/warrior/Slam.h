//
// Created by xkapp on 18.11.2023.
//

#ifndef REALMOFTEMPORALITY_SLAM_H
#define REALMOFTEMPORALITY_SLAM_H
#include "../Ability.h"

namespace game::creature::abilities {
    
    class Slam : public Ability {
      
      public:
        Slam(game::core::Game* game);
        //bool useAbility(creature::Creature *caster, std::vector<creature::Creature *> targets) override;
        void cast() override;
        void hit() override;

    };
    
}

#endif  // REALMOFTEMPORALITY_SLAM_H
