//
// Created by xkapp on 18.11.2023.
//

#include "Slam.h"
#include "../../Creature.h"
#include "game/core/Game.h"

game::creature::abilities::Slam::Slam(game::core::Game* game)
    : Ability(game,"Slam", "Slam enemies with your mighty force. "
                           "This damages all enemies in immidiate proximity.",
                           0, EnergyType::None, 0, 0, 9, false, true, 1.2, 1.2){
    m_pathToIcon = data::ASSET_PREFIX + "spells/slam.jpg";
      };

void game::creature::abilities::Slam::hit() {
    auto weapon = m_caster->getWeapon();

    int physicalDmg = (m_caster->getStats()->strength / 0.6) +
                      (m_caster->getStats()->agility / 0.2) + weapon->getDamages().physical;

    for (auto target : m_targets) {
        target->damage(items::Damage{physicalDmg, 0, 0, 0},nullptr);
    }

}

void game::creature::abilities::Slam::cast() {
    m_caster->gainEnergy(creature::EnergyType::Rage,20);
    m_targets = m_game->getCurrentMap()->findCreaturesInRange(m_caster,m_aoeRange);
    changeCatserSpriteState("Casting");
    hit();
}
