//
// Created by xkapp on 15.11.2023.
//

#include "MagicCast.h"

#include "../../Creature.h"
#include "../Projectile.h"
#include "src/data/constants.h"
#include "src/game/core/Game.h"

game::creature::abilities::MagicCast::MagicCast(game::core::Game* game)
  : Ability(game, "Magic cast", "Shoots a firework (perhaps)", 0, EnergyType::Mana, 7, 0.5, 2, true, false, 4.5, 0) {
    m_pathToIcon = data::ASSET_PREFIX + "spells/magic_cast.jpg";
}

void game::creature::abilities::MagicCast::cast() {
    QPointF direction = m_targetPosition - m_caster->getPosition();
    direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));

    auto projectile = new Projectile(
      direction,
      8,
      m_game,
      {
        "Magic Bolt",
        {0.2, 0.2},
        {
          data::ASSET_PREFIX + "vfx/magicBolt.png",
          .frameSize = {512, 512},
          .hitboxOffset = {232, 232},
          .stateFrameCount = {6},
          .stateFrameFrequency = {2},
        },
      },
      this
    );

    projectile->setPosition(m_caster->getPosition());  // at to nehitne týpka co to castí
    m_game->getCurrentMap()->addEntity(projectile);

    m_caster->lowerEnergy(m_requiredEnergyType, m_caster->getEnergy().maxEnergy * (m_energyCost-data::BASIC_SPELL_COST_MODIFIER.at(m_caster->getLevel()-1)) / 100);
}

void game::creature::abilities::MagicCast::hit() {
    auto staff = m_caster->getWeapon();

    if (staff == nullptr) {
        for (auto target: m_targets) {
            target->damage(items::Damage{0, m_caster->getStats()->intellect, 0, 0},m_caster);
        }

    } else {
        int magicDmg = (m_caster->getStats()->intellect * 0.8) + staff->getDamages().magical;
        for (auto target: m_targets) {
            target->damage(items::Damage{0, magicDmg, 0, 0},m_caster);
        }
    }
}

// bool game::creature::abilities::MagicCast::useAbility(
//     creature::Creature *caster, std::vector<creature::Creature *> targets
// ) {
//     auto staff = caster->getWeapon();

//     if (staff == nullptr) {
//         targets.at(1)->damage(new items::Damage{0, caster->getStats()->intellect, 0, 0});
//         return true;
//     } else {
//         int magicDmg = (caster->getStats()->intellect / 0.5) + staff->getDamages().magical;
//         targets.at(1)->damage(new items::Damage{0, magicDmg, 0, 0});
//         return true;
//     }
// }
