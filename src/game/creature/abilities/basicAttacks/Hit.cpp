//
// Created by xkapp on 14.11.2023.
//

#include "Hit.h"

#include "../../../items/Weapon.h"
#include "../../Creature.h"
#include "src/game/core/Game.h"

game::creature::abilities::Hit::Hit(game::core::Game* game)
  : Ability(game, "Hit", "Bonk the enemy very gently", 0, EnergyType::None, 0, 0, 1, false, false, 0.5, 0.5) {
    m_pathToIcon = data::ASSET_PREFIX + "spells/hit_alt.jpg";
}

void game::creature::abilities::Hit::hit() {
    auto weapon = m_caster->getWeapon();
    if (weapon == nullptr) {
        // mlácení pěstí

        int hitDamage;
        float strength = static_cast<float>(m_caster->getStats()->strength);
        strength = ((strength / 100) * 70);
        hitDamage = static_cast<int>(strength);

        for (int i = 0; i < m_targets.size(); ++i) {
            m_targets.at(i)->damage(items::Damage{hitDamage, 0, 0, 0},m_caster);
        }

    } else {
        // mlácení zbraní
        float strength = static_cast<float>(m_caster->getStats()->strength);

        strength = strength;
        int damageBonus = static_cast<int>(strength);

        auto damage = weapon->getDamages();
        damage.physical += damageBonus;

        for (int i = 0; i < m_targets.size(); ++i) {
            m_targets.at(i)->damage(damage,m_caster);
        }
    }
}

void game::creature::abilities::Hit::cast() {
    m_targets = m_game->getCurrentMap()->findCreaturesInRange(m_caster, 1.5, 90);
    changeCatserSpriteState("Hitting");
    hit();

}
