//
// Created by xkapp on 15.11.2023.
//

#ifndef REALMOFTEMPORALITY_MAGICCAST_H
#define REALMOFTEMPORALITY_MAGICCAST_H
#include "../Ability.h"

namespace game::creature::abilities {
    class MagicCast : public Ability {

      public:
        MagicCast(game::core::Game* game);

        void cast() override;
        void hit() override;
    };

}  // namespace game

#endif  // REALMOFTEMPORALITY_MAGICCAST_H
