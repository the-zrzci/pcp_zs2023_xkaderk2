//
// Created by xkapp on 14.11.2023.
//

#ifndef REALMOFTEMPORALITY_HIT_H
#define REALMOFTEMPORALITY_HIT_H
#include "../Ability.h"

namespace game::creature::abilities {
    class Hit : public Ability {
      public:
        Hit(game::core::Game* game);

        void cast() override;
        void hit() override;
    };
}


#endif  // REALMOFTEMPORALITY_HIT_H
