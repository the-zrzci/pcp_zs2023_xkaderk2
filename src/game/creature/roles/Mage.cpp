//
// Created by xkapp on 19.11.2023.
//

#include "Mage.h"

#include "src/game/creature/abilities/basicAttacks/Hit.h"
#include "src/game/creature/abilities/basicAttacks/MagicCast.h"
#include "src/game/creature/abilities/mage/ArcaneBolt.h"
#include "src/game/creature/abilities/mage/Fireball.h"

game::creature::Mage::Mage(
  game::core::Game* game,
  game::EntityParams entityParams,
  Energy* energy,
  Inventory* inv,
  bool isFirenldy,
  int money
)
  : Creature(
      game,
      entityParams,
      data::BASE_MAX_HEALTH.at("Mage"),
      inv,
      new Stats{3, 4, 10, 2, 1},
      isFirenldy,
      energy,
      money
    ) {
    if (isFirenldy) {
        m_abilities.push_back(new game::creature::abilities::Hit(game));
        m_abilities.push_back(new game::creature::abilities::MagicCast(game));
        m_abilities.push_back(new game::creature::abilities::ArcaneBolt(game));
        m_abilities.push_back(new game::creature::abilities::Fireball(game));
    }
    updateStats();
    heal(m_maxHealth);
    gainEnergy(game::creature::EnergyType::Mana, m_energy->maxEnergy);
}

void game::creature::Mage::levelUp() {
    m_stats->strength += data::MAGE_LVL_UP.strength;
    m_stats->stamina += data::MAGE_LVL_UP.stamina;
    m_stats->intellect += data::MAGE_LVL_UP.intellect;
    m_stats->agility += data::MAGE_LVL_UP.agility;

    updateStats();

    heal(m_maxHealth);
    gainEnergy(game::creature::EnergyType::Mana, m_energy->maxEnergy);
    m_level += 1;
    emit levelChanged(m_level);
}

void game::creature::Mage::updateStats() {
    int maxHealthBonus = m_stats->stamina * 5;
    emit healthChanged();
    int maxEnergyBonus = m_stats->intellect * 10;
    emit energyChanged();

    m_maxHealth = data::BASE_MAX_HEALTH.at("Mage") + maxHealthBonus;
    m_energy->maxEnergy = data::BASE_MAX_ENERGY.at("Mage") + maxEnergyBonus;
}
