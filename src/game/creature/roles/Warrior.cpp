//
// Created by xkapp on 19.11.2023.
//
#include "Warrior.h"

#include "src/data/levels.h"
#include "src/game/creature/Stats.h"
#include "src/game/creature/abilities/basicAttacks/Hit.h"
#include "src/game/creature/abilities/warrior/Slam.h"

game::creature::Warrior::Warrior(
  game::core::Game* game,
  game::EntityParams entityParams,
  Energy* energy,
  Inventory* inv,
  bool isFirenldy,
  int money
)
  : Creature(
      game,
      entityParams,
      data::BASE_MAX_HEALTH.at("Warrior"),
      inv,
      new Stats{10, 5, 2, 3, 1},
      isFirenldy,
      energy,
      money
    ) {
    if (isFirenldy) {
        m_abilities.push_back(new game::creature::abilities::Hit(game));
        m_abilities.push_back(new game::creature::abilities::Slam(game));
    }
    updateStats();
    heal(m_maxHealth);
}

void game::creature::Warrior::levelUp() {
    m_stats->strength += data::WARRIOR_LVL_UP.strength;
    m_stats->stamina += data::WARRIOR_LVL_UP.stamina;
    m_stats->intellect += data::WARRIOR_LVL_UP.intellect;
    m_stats->agility += data::WARRIOR_LVL_UP.agility;

    updateStats();
    heal(m_maxHealth);
    m_level += 1;
    emit levelChanged(m_level);
}

void game::creature::Warrior::updateStats() {
    int maxHealthBonus = m_stats->stamina * 5;
    emit healthChanged();

    m_maxHealth = data::BASE_MAX_HEALTH.at("Warrior") + maxHealthBonus;
}
