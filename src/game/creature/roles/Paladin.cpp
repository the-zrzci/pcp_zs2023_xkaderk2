//
// Created by xkapp on 19.11.2023.
//
#include "Paladin.h"

#include "src/game/creature/abilities/basicAttacks/Hit.h"
#include "src/game/creature/abilities/paladin/CrusaderStrike.h"
#include "src/game/creature/abilities/paladin/HolyLight.h"

game::creature::Paladin::Paladin(
  game::core::Game* game,
  game::EntityParams entityParams,
  Energy* energy,
  Inventory* inv,
  bool isFirenldy,
  int money
)
  : Creature(
      game,
      entityParams,
      data::BASE_MAX_HEALTH.at("Paladin"),
      inv,
      new Stats{6, 6, 4, 4, 1},
      isFirenldy,
      energy,
      money
    ) {
    if (isFirenldy) {
        m_abilities.push_back(new abilities::Hit(game));
        m_abilities.push_back(new abilities::CrusaderStrike(game));
        m_abilities.push_back(new abilities::HolyLight(game));
    }
    updateStats();
    heal(m_maxHealth);
    gainEnergy(game::creature::EnergyType::Mana, m_energy->maxEnergy);
}

void game::creature::Paladin::levelUp() {
    m_stats->strength += data::PALADIN_LVL_UP.strength;
    m_stats->stamina += data::PALADIN_LVL_UP.stamina;
    m_stats->intellect += data::PALADIN_LVL_UP.intellect;
    m_stats->agility += data::PALADIN_LVL_UP.agility;

    updateStats();

    heal(m_maxHealth);
    gainEnergy(game::creature::EnergyType::Mana, m_energy->maxEnergy);
    m_level += 1;
    emit levelChanged(m_level);
}

void game::creature::Paladin::updateStats() {
    int maxHealthBonus = m_stats->stamina * 5;
    emit healthChanged();
    int maxEnergyBonus = m_stats->intellect * 10;
    emit energyChanged();

    m_maxHealth = data::BASE_MAX_HEALTH.at("Mage") + maxHealthBonus;
    m_energy->maxEnergy = data::BASE_MAX_ENERGY.at("Mage") + maxEnergyBonus;
}
