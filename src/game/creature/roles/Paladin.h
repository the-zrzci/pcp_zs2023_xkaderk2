//
// Created by xkapp on 19.11.2023.
//

#ifndef REALMOFTEMPORALITY_PALADIN_H
#define REALMOFTEMPORALITY_PALADIN_H
#include "../Creature.h"

namespace game::creature {

    class Paladin : public Creature {
      public:
        Paladin(
          game::core::Game* game,
          game::EntityParams entityParams,
          Energy* energy,
          Inventory* inv,
          bool isFriendly,
          int money
        );
        void levelUp() override;
        void updateStats() override;
    };
}  // namespace game::creature

#endif  // REALMOFTEMPORALITY_PALADIN_H
