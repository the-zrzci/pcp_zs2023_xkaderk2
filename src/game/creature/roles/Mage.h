//
// Created by xkapp on 19.11.2023.
//

#ifndef REALMOFTEMPORALITY_MAGE_H
#define REALMOFTEMPORALITY_MAGE_H
#include "../Creature.h"

namespace game::creature {
    class Mage : public Creature {
      public:
        Mage(
          game::core::Game* game,
          game::EntityParams entityParams,
          Energy* energy,
          Inventory* inv,
          bool isFriendly,
          int money
        );
        void levelUp() override;
        void updateStats() override;
    };
}  // namespace game::creature

#endif  // REALMOFTEMPORALITY_MAGE_H
