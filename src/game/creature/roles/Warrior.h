//
// Created by xkapp on 19.11.2023.
//

#ifndef REALMOFTEMPORALITY_WARRIOR_H
#define REALMOFTEMPORALITY_WARRIOR_H
#include "../Creature.h"

namespace game::creature {

    class Warrior : public Creature {
      public:
        Warrior(
          game::core::Game* game,
          game::EntityParams entityParams,
          Energy* energy,
          Inventory* inv,
          bool isFriendly,
          int money
        );
        void levelUp() override;
        void updateStats() override;
    };
}  // namespace game::creature

#endif  // REALMOFTEMPORALITY_WARRIOR_H
