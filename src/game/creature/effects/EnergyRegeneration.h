//
// Created by xkapp on 22.11.2023.
//

#ifndef REALMOFTEMPORALITY_ENERGYREGENERATION_H
#define REALMOFTEMPORALITY_ENERGYREGENERATION_H
#include "Effect.h"
#include "../EnergyType.h"
#include "../Creature.h"

namespace game::creature::effects {
    class EnergyRegeneration : protected Effect {
      private:
        game::creature::EnergyType m_energyType;
        int m_energyAmount;

      public:
        EnergyRegeneration(std::string name, std::string description, float timeDuration, float periodDuration, Creature* target, EnergyType energyType, int energyAmount);
        void effectAction() override;

    };
}


#endif  // REALMOFTEMPORALITY_ENERGYREGENERATION_H
