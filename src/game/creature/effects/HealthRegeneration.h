//
// Created by xkapp on 05.12.2023.
//

#ifndef REALMOFTEMPORALITY_HEALTHREGENERATION_H
#define REALMOFTEMPORALITY_HEALTHREGENERATION_H
#include "Effect.h"


namespace game::creature::effects {

    class HealthRegeneration : public Effect {
    private:

        int m_healAmount;

    public:
        HealthRegeneration(std::string name, std::string description, float timeDuration, float periodDuration, Creature* target, int healAmount);
        void effectAction() override;
    };

}

#endif //REALMOFTEMPORALITY_HEALTHREGENERATION_H
