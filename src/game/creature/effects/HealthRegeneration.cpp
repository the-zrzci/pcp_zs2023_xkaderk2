//
// Created by xkapp on 05.12.2023.
//
#include "HealthRegeneration.h"
#include "../Creature.h"

game::creature::effects::HealthRegeneration::HealthRegeneration(std::string name, std::string description, float timeDuration,
                                                                float periodDuration, game::creature::Creature *target,
                                                                int healAmount) : Effect(name,description,timeDuration,periodDuration,target) {
    m_healAmount = healAmount;

}

void game::creature::effects::HealthRegeneration::effectAction() {
    m_target->heal(m_healAmount);

}
