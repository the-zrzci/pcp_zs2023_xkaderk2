//
// Created by xkapp on 23.01.2024.
//

#ifndef REALMOFTEMPORALITY_AUTOMANAREGEN_H
#define REALMOFTEMPORALITY_AUTOMANAREGEN_H

#include "AutoEnergyEffect.h"
namespace game::creature {
    class Creature;
}

namespace game::creature::effects {
    class AutoManaRegen : public AutoEnergyEffect {
    public:
        AutoManaRegen(Creature* target);
        void effectAction() override;

    };
}


#endif //REALMOFTEMPORALITY_AUTOMANAREGEN_H
