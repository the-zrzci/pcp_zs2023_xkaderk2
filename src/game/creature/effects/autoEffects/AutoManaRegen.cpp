//
// Created by xkapp on 24.01.2024.
//

#include "AutoManaRegen.h"
#include "game/creature/Creature.h"

game::creature::effects::AutoManaRegen::AutoManaRegen(game::creature::Creature *target) : AutoEnergyEffect(target,
                                                                                                             "Natural Mana Regeneration",
                                                                                                             "it gib mana"){

}


void game::creature::effects::AutoManaRegen::effectAction() {
    m_target->gainEnergy(EnergyType::Mana, m_target->getEnergy().maxEnergy * 0.05);


}