//
// Created by xkapp on 24.01.2024.
//

#ifndef REALMOFTEMPORALITY_AUTOENERGYEFFECT_H
#define REALMOFTEMPORALITY_AUTOENERGYEFFECT_H

#include "game/creature/effects/Effect.h"
#include <string>
namespace game::creature {
    class Creature;
}
namespace game::creature::effects {
    class AutoEnergyEffect : public Effect {
    protected:
        bool m_active;

    public:
        AutoEnergyEffect(Creature* target, std::string name, std::string description);
        void changeStatus(bool status);
        bool updateTimers(double delta) override;
    };

}

#endif //REALMOFTEMPORALITY_AUTOENERGYEFFECT_H
