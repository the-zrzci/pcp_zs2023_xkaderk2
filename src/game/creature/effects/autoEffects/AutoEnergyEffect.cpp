//
// Created by xkapp on 24.01.2024.
//

#include "AutoEnergyEffect.h"
#include "game/creature/Creature.h"


game::creature::effects::AutoEnergyEffect::AutoEnergyEffect(game::creature::Creature *target, std::string name, std::string description)
:Effect(name,description,0,1.5,target){
    m_active = true;
}

void game::creature::effects::AutoEnergyEffect::changeStatus(bool status) {
    m_active = status;
}

bool game::creature::effects::AutoEnergyEffect::updateTimers(double delta) {
        if (m_active and m_target->getEnergy().energy < m_target->getEnergy().maxEnergy) {

            m_periodTimer += delta;

            if (m_periodTimer >= m_periodDuration) {
                effectAction();
                m_periodTimer = 0;
            }

        } else {
            m_periodTimer = 0;
        }
        return true;

}
