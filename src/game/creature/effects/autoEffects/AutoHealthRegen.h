//
// Created by xkapp on 23.01.2024.
//

#ifndef REALMOFTEMPORALITY_AUTOHEALTHREGEN_H
#define REALMOFTEMPORALITY_AUTOHEALTHREGEN_H

#include "game/creature/effects/Effect.h"

namespace game::creature {
    class Creature;
}

namespace game::creature::effects {
    class AutoHealthRegen : public Effect {
        bool m_active;

    public:
        AutoHealthRegen(Creature* target);
        void changeStatus(bool status);
        bool updateTimers(double delta) override;
        void effectAction() override;

    };
}

#endif //REALMOFTEMPORALITY_AUTOHEALTHREGEN_H
