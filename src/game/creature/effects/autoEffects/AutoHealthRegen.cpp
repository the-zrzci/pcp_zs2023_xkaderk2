//
// Created by xkapp on 23.01.2024.
//

#include "AutoHealthRegen.h"
#include "game/creature/Creature.h"



game::creature::effects::AutoHealthRegen::AutoHealthRegen(Creature* target) : Effect("Natural Health Regeneration","It heals you",0,1,target) {
    m_active = true;

}

bool game::creature::effects::AutoHealthRegen::updateTimers(double delta){
    if (m_active and m_target->getHealth() < m_target->getMaxHealth()) {

        m_periodTimer += delta;

        if (m_periodTimer >= m_periodDuration) {
            effectAction();
            m_periodTimer = 0;
        }

    } else {
        m_periodTimer = 0;
    }
    return true;

}

void game::creature::effects::AutoHealthRegen::changeStatus(bool status) {
    m_active = status;
}

void game::creature::effects::AutoHealthRegen::effectAction() {
    m_target->heal(m_target->getMaxHealth()*0.03);


}