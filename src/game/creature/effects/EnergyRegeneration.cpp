//
// Created by xkapp on 22.11.2023.
//
#include "EnergyRegeneration.h"

game::creature::effects::EnergyRegeneration::EnergyRegeneration(std::string name, std::string description, float timeDuration, float periodDuration, Creature* target,
                                                                EnergyType energyType, int energyAmount) : Effect(name, description, timeDuration, periodDuration, target) {
          m_energyType = energyType;
          m_energyAmount = energyAmount;
      }

void game::creature::effects::EnergyRegeneration::effectAction() {

    m_target->gainEnergy(m_energyType, m_energyAmount);

}
