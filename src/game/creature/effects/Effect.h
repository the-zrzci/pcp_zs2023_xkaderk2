//
// Created by xkapp on 14.11.2023.
//

#ifndef REALMOFTEMPORALITY_EFFECT_H
#define REALMOFTEMPORALITY_EFFECT_H
#include <string>
#include <QObject>

namespace game::creature {
    class Creature;
}

namespace game::creature::effects {
    class Effect : public QObject{
      Q_OBJECT

      private:
        std::string m_name;
        std::string m_description;

    protected:
        Creature* m_target;
        float m_timer;
        float m_timeDuration;
        float m_periodTimer;
        float m_periodDuration;


      public:
        Effect(std::string name, std::string description, float timeDuration, float periodDuration, Creature* target);
        void activateEffect(Creature* target);  // changed
        virtual bool updateTimers(double delta);
        void lowerTimer(float time);
        void lowerPeriodTimer(float time);
        virtual void effectAction() = 0;

      signals:
        void effectEnded(Effect* effect);

    };
}  // namespace game::creature::effects

#endif  // REALMOFTEMPORALITY_EFFECT_H
