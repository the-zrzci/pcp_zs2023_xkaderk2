//
// Created by xkapp on 14.11.2023.
//

#include "Effect.h"
#include "qdebug.h"
game::creature::effects::Effect::Effect(std::string name, std::string description, float timeDuration, float periodDuration,
                                        game::creature::Creature *target) {
    m_name = name;
    m_description = description;
    m_timeDuration = timeDuration;
    m_timer = 0;
    m_periodDuration = periodDuration;
    m_periodTimer = 0;
    m_target = target;
//changed
}


void game::creature::effects::Effect::activateEffect(game::creature::Creature* target) {
    //changed
    m_target = target;
    m_timer = m_timeDuration;
    m_periodTimer = m_periodDuration;

}


void game::creature::effects::Effect::lowerTimer(float time) {
    m_timer -= time;
}

void game::creature::effects::Effect::lowerPeriodTimer(float time) {
    if (m_periodTimer > 0) {
        m_periodTimer -= time;

        if (m_periodTimer <= 0) {
            effectAction();

            m_periodTimer = m_periodDuration;
        }
    }
}


bool game::creature::effects::Effect::updateTimers(double delta) {
    lowerPeriodTimer(delta);

    if (m_timer > 0) {
        lowerTimer(delta);
    }

    if (m_timer <= 0) {
        emit effectEnded(this);
        return false;
    }

    return true;
}
