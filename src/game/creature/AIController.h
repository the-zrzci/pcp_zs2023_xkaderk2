//
// Created by xkapp on 23.01.2024.
//

#ifndef REALMOFTEMPORALITY_AICONTROLLER_H
#define REALMOFTEMPORALITY_AICONTROLLER_H
#include <vector>
#include <map>
#include <string>
#include <QPointF>




namespace game::creature {
    class Creature;



    class AIController {
    public:
        enum Action {Move,UseAbility,Sleep,UseItem,RunAway};
        enum Condition {LowHealth,LowMana,EnemyLowHealth,EnemyLowMana};

        struct AIConfig {
            float detectionRadius;
            bool likesHome;
            float rangeFromOrigin;
            std::vector<int> actionSequence;
            std::map<int,std::pair<Action,std::string>> actions;
            //std::map<int,std::string> conditions
        };

    private:
        Creature* m_controlledCreature;
        bool m_ranged;
        Creature* m_target;
        AIConfig m_config;
        QPointF m_origin;
        bool m_active;
        float m_rangeToTarget;
        Creature* m_player;

        bool m_hasBeenActivated;

        int m_actionIndexer;

        float m_mainActionTimer;
        float m_secondaryActionTimer;
        float m_additionalTimer; //spare timer

        float m_mainActionTriggerTime;
        float m_secondaryActionTriggerTime;
        float m_additionalTriggerTime; //spare timer


    public:
        AIController(AIConfig config,float mainActionTime,float secondaryActionTime);
        void setControlledCreature(Creature* creature);
        void update(double delta);
        void checkForAction();
        void doAction(std::pair<Action,std::string> action);
        bool isPlayerInRange();

    };

}

#endif //REALMOFTEMPORALITY_AICONTROLLER_H
