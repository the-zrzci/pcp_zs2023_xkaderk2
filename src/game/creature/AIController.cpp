//
// Created by xkapp on 23.01.2024.
//

#include "AIController.h"
#include "Creature.h"
#include "game/core/Game.h"

game::creature::AIController::AIController(game::creature::AIController::AIConfig config, float mainActionTime,
                                           float secondaryActionTime) {
    m_active = false;
    m_config = config;
    m_mainActionTriggerTime = mainActionTime;
    m_secondaryActionTriggerTime = secondaryActionTime;
    m_actionIndexer = 0;
    m_hasBeenActivated = false;
    m_player = nullptr;

}

void game::creature::AIController::setControlledCreature(game::creature::Creature *creature) {
    m_controlledCreature = creature;
}

void game::creature::AIController::update(double delta) {
    if (!m_controlledCreature->getAlive()) {
        return;
        //if controlled creature is dead, skip updates
    }

    if (!m_hasBeenActivated) {
        m_hasBeenActivated = true;
        m_origin = m_controlledCreature->getPosition();
    }
    m_player = m_controlledCreature->getGame()->getPlayer();

    if (!m_active) {
        if (isPlayerInRange()) {
            m_active = true;
            m_target = m_controlledCreature->getGame()->getPlayer();

        }
    } else {
        if (!isPlayerInRange()) {
            m_active = false;
            m_mainActionTimer = 0;
            m_secondaryActionTimer = 0;
            m_target = nullptr;
            m_actionIndexer = 0;

        }
    }

    m_rangeToTarget = (m_player->getPosition()-m_controlledCreature->getPosition()).manhattanLength();

    if ((m_player->getPosition()-m_origin).manhattanLength()<=m_config.rangeFromOrigin) {

        if (m_active) { //maybe not needed
            if (m_rangeToTarget > 1) {
                auto direction = m_player->getPosition() - m_controlledCreature->getPosition();
                direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));
                m_controlledCreature->move(direction);

            } else {
                m_mainActionTimer += delta;
                m_secondaryActionTimer += delta;

                if (m_mainActionTimer >= m_mainActionTriggerTime) {
                    checkForAction();
                    m_mainActionTimer = 0;
                }
            }
        } else if ((m_origin - m_controlledCreature->getPosition()).manhattanLength() > 0.01){
            auto direction = m_origin - m_controlledCreature->getPosition();
            direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));
            m_controlledCreature->move(direction);
        }


    } else {
        if (m_origin != m_controlledCreature->getPosition()) {
            auto direction = m_origin - m_controlledCreature->getPosition();
            direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));
            m_controlledCreature->move(direction);
        }

    }


//    if (m_active) {
//        //qDebug() << "aktivni";
//        //qDebug() << (m_controlledCreature->getGame()->getPlayer()->getPosition()-m_controlledCreature->getPosition()).manhattanLength();
//
//
//        m_rangeToTarget = (m_player->getPosition()-m_controlledCreature->getPosition()).manhattanLength();
//
//
//        if () {
//            if (m_rangeToTarget > 1) {
//                auto direction = m_target->getPosition() - m_controlledCreature->getPosition();
//                direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));
//                m_controlledCreature->move(direction);
//
//            } else {
//                m_mainActionTimer += delta;
//                m_secondaryActionTimer += delta;
//
//                if (m_mainActionTimer >= m_mainActionTriggerTime) {
//                    checkForAction();
//                    m_mainActionTimer = 0;
//                }
//            }
//
//        } else {
//            auto direction = m_origin - m_controlledCreature->getPosition();
//            direction /= sqrt(pow(direction.x(), 2) + pow(direction.y(), 2));
//            m_controlledCreature->move(direction);
//
//        }
//
//
//    }

}

void game::creature::AIController::doAction(std::pair<Action,std::string> action) {
    if (action.first==UseAbility) {
        m_controlledCreature->useAbility(std::stoi(action.second)-1,{1,1});
    }

}

bool game::creature::AIController::isPlayerInRange() {
    if ((m_controlledCreature->getGame()->getPlayer()->getPosition()-m_controlledCreature->
            getPosition()).manhattanLength()<=m_config.detectionRadius) {
        return true;
    } else {
        return false;
    }
}

void game::creature::AIController::checkForAction() {
    //check for conditions, check if AI is close enough to target
    doAction(m_config.actions.at(m_config.actionSequence.at(m_actionIndexer)));
    if (m_actionIndexer+1 >= m_config.actionSequence.size()) {
        m_actionIndexer = 0;
    } else {
        m_actionIndexer += 1;
    }

}