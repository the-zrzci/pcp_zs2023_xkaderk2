#ifndef REALMOFTEMPORALITY_INVENTORY_H
#define REALMOFTEMPORALITY_INVENTORY_H

#include <QObject>
#include <vector>

#include "items/Item.h"

namespace game {
    class Inventory : public QObject {
        Q_OBJECT
        Q_PROPERTY(int capacity READ getCapacity NOTIFY capacityChanged);
        Q_PROPERTY(int width READ getWidth NOTIFY widthChanged);
        Q_PROPERTY(QVector<items::Item*> items READ getItems NOTIFY contentChanged);

      private:
        int m_capacity, m_width;
        QVector<items::Item*> m_items;

      public:
        Q_INVOKABLE void moveItem(int from, int to);

        Inventory(int capacity, int width);

        Inventory(QVector<items::Item*> items);

        Inventory(QVector<items::Item*> items, int capacity, int width);

        bool addItem(game::items::Item* item);

        void removeItem(int itemIndex);

        void removeItem(game::items::Item* item);

        int getSize();

        QVector<items::Item*> getItems() const;

        int getWidth() const;
        int getCapacity() const;

        void saveState(QJsonObject& json) const;
        void loadState(game::core::Game* game, QJsonObject const& json);

      signals:
        void widthChanged();
        void capacityChanged();
        void contentChanged();
    };
}  // namespace game

#endif  // REALMOFTEMPORALITY_INVENTORY_H
