#ifndef REALMOFTEMPORALITY_DECORATION_H
#define REALMOFTEMPORALITY_DECORATION_H

#include "Entity.h"

namespace game {
    /// @brief Object entity without collisions
    class Decoration : public game::Entity {
        Q_OBJECT;

      public:
        Decoration(core::Game* game, const EntityParams& params);
    };
}  // namespace game

#endif  // REALMOFTEMPORALITY_DECORATION_H
