#include "Fire.h"

#include "core/Game.h"
#include "creature/Creature.h"

game::Fire::Fire(core::Game* game, const EntityParams& params, int damage)
  : game::Entity(game, params),
    m_damage(damage) {
    setCollidable(false);
    setInteraction(game::Entity::Interaction::Remove);
}

void game::Fire::setShadowed(float shadowed) {
    // nope, fire doesn't get shadowed
}

void game::Fire::onCollision(const std::vector<Entity*>& entities) {
    if (m_lastCheck < 0.2) {
        return;
    }
    m_lastCheck = 0;

    for (auto entity : entities) {
        auto* creature = qobject_cast<game::creature::Creature*>(entity);
        if (creature == nullptr) continue;
        creature->damage({.magical = m_damage}, nullptr);
    }
}

void game::Fire::update(double delta) {
    m_lastCheck += delta;

    game::Entity::update(delta);
}

void game::Fire::interact(Interaction interaction) {
    // Check if part of quest
    if (!m_game->getQuestManager()->isQuestSubject(this)) return;

    scheduleDelete();
    game::Entity::interact(interaction);
}
