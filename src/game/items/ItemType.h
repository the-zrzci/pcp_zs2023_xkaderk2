//
// Created by matys on 13.11.2023.
//

#ifndef REALMOFTEMPORALITY_ITEMTYPE_H
#define REALMOFTEMPORALITY_ITEMTYPE_H
namespace game::items {
    enum class ItemType {
    Misc,
    Consumable,
    Armor,
    Weapon
    };
}
#endif //REALMOFTEMPORALITY_ITEMTYPE_H
