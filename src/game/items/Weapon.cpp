//
// Created by Matěj on 14.11.2023.
//
#include "Weapon.h"

game::items::Weapon::Weapon(
  game::core::Game* game,
  game::EntityParams entityParams,
  QString description,
  int requiredLevel,
  int price,
  game::creature::Stats bonusStats,
  game::items::ItemType itemType,
  game::items::Damage damages,
  bool isRanged,
  game::items::WeaponType weaponType
)
  : Item(game, entityParams, description, requiredLevel, price, itemType) {

    m_damage = damages;
    m_isRanged = isRanged;
    m_weaponType = weaponType;
    m_bonus = bonusStats;
}

game::items::Damage game::items::Weapon::getDamages() {
    return m_damage;
}
bool game::items::Weapon::isRanged() {
    return m_isRanged;
}
game::items::WeaponType game::items::Weapon::getWeaponType() {
    return m_weaponType;
}
game::creature::Stats game::items::Weapon::getStats() {
    return m_bonus;
}
