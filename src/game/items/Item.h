//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_ITEM_H
#define REALMOFTEMPORALITY_ITEM_H
#include <string>

#include "../creature/Stats.h"
#include "ItemType.h"
#include "src/game/Entity.h"

namespace game::items {
    class Item : public game::Entity {
        Q_OBJECT

        Q_PROPERTY(ItemType itemType MEMBER m_itemType CONSTANT);
        Q_PROPERTY(int price MEMBER m_price CONSTANT);
        Q_PROPERTY(QString description MEMBER m_description CONSTANT);
        Q_PROPERTY(int requiredLevel MEMBER m_requiredLevel CONSTANT);
      private:
        QString m_description;
        int m_requiredLevel;
        int m_price;

        game::items::ItemType m_itemType;

      public:
        // return true on item compilation if it is literally the same object
        //  bool operator==(const Item& other) const {
        //      return this == &other;
        //  }
        Item(
          game::core::Game* game,
          game::EntityParams entityParams,
          QString description,
          int requiredLevel,
          int price,
          game::items::ItemType itemType
        );
        game::items::ItemType getItemType() const;
        int getRequiredLevel() const;
        QString getDescription() const;
        void interact(Interaction interaction) override;
        void drop();
    };

}  // namespace game::items
#endif  // REALMOFTEMPORALITY_ITEM_H
