//
// Created by Matěj on 16.11.2023.
//

#ifndef REALMOFTEMPORALITY_POTION_H
#define REALMOFTEMPORALITY_POTION_H
#include "Item.h"
#include "src/game/creature/EnergyType.h"
#include "src/game/creature/Stats.h"
#include "src/game/creature/effects/Effect.h"

namespace game::items {
    class Potion : public game::items::Item {
      private:
        bool m_healing;
        int m_value;
        bool m_isOverTime;
        game::creature::EnergyType m_energyType;
        game::creature::Stats m_bonusStat;
        game::creature::effects::Effect* m_effect;

      public:
        Potion(
          game::core::Game* game,
          game::EntityParams entityParams,
          QString description,
          int requiredLevel,
          int price,
          game::items::ItemType itemType,
          bool healing,
          int value,
          game::creature::EnergyType m_energyType
        );
        Potion(
          game::core::Game* game,
          game::EntityParams entityParams,
          QString description,
          int requiredLevel,
          int price,
          game::items::ItemType itemType,
          game::creature::effects::Effect* effect
        );
        void use(game::creature::Creature* target);
    };
}  // namespace game::items
#endif  // REALMOFTEMPORALITY_POTION_H
