//
// Created by Matěj on 14.11.2023.
//
#include "Item.h"
#include "src/game/core/Map.h"
#include "src/game/core/Game.h"

game::items::Item::Item(
            game::core::Game* game,
    game::EntityParams entityParams,
    QString description,
    int requiredLevel,
    int price,
    game::items::ItemType itemType
)
    : Entity(game,entityParams, game::Entity::Sensor) {
    m_description = description;
    m_requiredLevel = requiredLevel;
    m_price = price;
    m_itemType = itemType;
    m_interaction = Interaction::Pickup;
}
game::items::ItemType game::items::Item::getItemType() const {
    return m_itemType;
}
int game::items::Item::getRequiredLevel() const {
    return m_requiredLevel;
}
QString game::items::Item::getDescription() const {
    return m_description;
}

void game::items::Item::interact(game::Entity::Interaction interaction) {
    //if inventory is full do nothing
    if(m_game->getPlayer()->getInventory()->addItem(this)){
    getCurrentMap()->removeEntity(this);
    game::Entity::interact(interaction);
    }
}

void game::items::Item::drop() {
    this->setPosition(m_game->getPlayer()->getPosition());
    m_game->getCurrentMap()->addEntity(this);
    
}
