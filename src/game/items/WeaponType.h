//
// Created by Matěj on 14.11.2023.
//

#ifndef REALMOFTEMPORALITY_WEAPONTYPE_H
#define REALMOFTEMPORALITY_WEAPONTYPE_H
namespace game::items {
    enum class WeaponType {
        Sword,
        Hammer,
        Staff,
        Bow,
        Axe
    };
}
#endif  // REALMOFTEMPORALITY_WEAPONTYPE_H
