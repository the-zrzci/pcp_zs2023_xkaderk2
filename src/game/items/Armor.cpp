//
// Created by Matěj on 14.11.2023.
//
#include "Armor.h"
game::items::Armor::Armor(
  game::core::Game* game,
  game::EntityParams entityParams,
  QString description,
  int requiredLevel,
  game::creature::Stats bonusStats,
  int price,
  game::items::ItemType itemType,
  game::items::Damage protection,
  game::items::ArmorType armorType
)
  : Item(game, entityParams, description, requiredLevel, price, itemType) {
    m_protection = protection;
    m_armorType = armorType;
    m_bonus = bonusStats;
}

game::items::ArmorType game::items::Armor::getArmorType() {
    return m_armorType;
}
game::items::Damage game::items::Armor::getProtection() {
    return m_protection;
}
game::creature::Stats game::items::Armor::getStats() {
    return m_bonus;
}
