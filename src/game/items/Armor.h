//
// Created by Matěj on 13.11.2023.
//

#ifndef REALMOFTEMPORALITY_ARMOR_H
#define REALMOFTEMPORALITY_ARMOR_H

#include "ArmorType.h"
#include "Damage.h"
#include "Item.h"

namespace game::items {
    class Armor : public Item {
        Q_OBJECT
        Q_PROPERTY(game::items::ArmorType armorType READ getArmorType CONSTANT)
        Q_PROPERTY(game::creature::Stats bonus MEMBER m_bonus CONSTANT)
        Q_PROPERTY(game::items::Damage armor MEMBER m_protection CONSTANT)
        
      private:
        game::items::Damage m_protection;
        game::creature::Stats m_bonus;
        // Attribute for armor equipment
        game::items::ArmorType m_armorType;

      public:
        Armor(
          game::core::Game* game,
          game::EntityParams entityParams,
          QString description,
          int requiredLevel,
          game::creature::Stats bonusStats,
          int price,
          game::items::ItemType itemType,
          game::items::Damage protection,
          game::items::ArmorType armorType
        );
        game::items::ArmorType getArmorType();
        game::items::Damage getProtection();
        game::creature::Stats getStats();
    };
}  // namespace game::items

#endif  // REALMOFTEMPORALITY_ARMOR_H
