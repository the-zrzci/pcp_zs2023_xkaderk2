//
// Created by matys on 13.11.2023.
//

#ifndef REALMOFTEMPORALITY_ARMORTYPE_H
#define REALMOFTEMPORALITY_ARMORTYPE_H

namespace game::items{
    enum class ArmorType{
        Head,
        Shoulders,
        Back,
        Chest,
        Ring,
        Legs,
        Feet,
        Necklace,
        Gloves
    };
}

#endif //REALMOFTEMPORALITY_ARMORTYPE_H
