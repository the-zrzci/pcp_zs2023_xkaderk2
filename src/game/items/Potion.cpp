//
// Created by Matěj on 16.11.2023.
//

#include "Potion.h"
#include "src/game/core/Game.h"

#include "src/game/creature/Creature.h"
game::items::Potion::Potion(
  game::core::Game* game,
  game::EntityParams entityParams,
  QString description,
  int requiredLevel,
  int price,
  game::items::ItemType itemType,
  bool healing,
  int value,
  game::creature::EnergyType energyType
)
  : Item(game, entityParams, description, requiredLevel, price, itemType) {
    m_healing = healing;
    m_value = value;
    m_energyType = energyType;
    m_isOverTime = false;
    m_effect = nullptr;
}

game::items::Potion::Potion(
  game::core::Game* game,
  game::EntityParams entityParams,
  QString description,
  int requiredLevel,
  int price,
  game::items::ItemType itemType,
  game::creature::effects::Effect* effect
)
  : Item(game, entityParams, description, requiredLevel, price, itemType) {
    m_effect = effect;
    m_isOverTime = true;
    m_value = 0;
    m_healing = false;
}

void game::items::Potion::use(game::creature::Creature* target) {
    if (m_isOverTime) {
        target->addEffect(m_effect);
    } else {
        if (m_healing) {
            target->heal(m_value);
        } else {
            target->gainEnergy(m_energyType, m_value);
        }
    }
    m_game->getPlayer()->getInventory()->removeItem(this);
}
