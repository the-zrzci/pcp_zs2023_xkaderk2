//
// Created by Matěj on 13.11.2023.
//

#ifndef REALMOFTEMPORALITY_WEAPON_H
#define REALMOFTEMPORALITY_WEAPON_H

#include "Damage.h"
#include "Item.h"
#include "WeaponType.h"

namespace game::items {
    class Weapon : public Item {
        Q_OBJECT
        Q_PROPERTY(game::items::WeaponType weaponType MEMBER m_weaponType CONSTANT)
        Q_PROPERTY(game::creature::Stats bonus MEMBER m_bonus CONSTANT)
        Q_PROPERTY(game::items::Damage damage MEMBER m_damage CONSTANT)
        
        
      private:

        bool m_isRanged;
        game::items::Damage m_damage;
        game::items::WeaponType m_weaponType;
        game::creature::Stats m_bonus;

      public:
        Weapon(
          game::core::Game* game,
          game::EntityParams entityParams,
          QString description,
          int requiredLevel,
          int price,
          game::creature::Stats bonusStats,
          game::items::ItemType itemType,
          game::items::Damage damages,
          bool isRanged,
          game::items::WeaponType weaponType
        );
        game::items::Damage getDamages();
        int getRange();
        float getAttackSpeed();
        bool isRanged();
        game::items::WeaponType getWeaponType();
        game::creature::Stats getStats();
    };
}  // namespace game::items

#endif  // REALMOFTEMPORALITY_WEAPON_H
