//
// Created by Matěj on 13.11.2023.
//

#ifndef REALMOFTEMPORALITY_DAMAGE_H
#define REALMOFTEMPORALITY_DAMAGE_H
#include <QObject>

namespace game::items {
    struct Damage {
    Q_GADGET

        Q_PROPERTY(int physical MEMBER physical CONSTANT);
        Q_PROPERTY(int magical MEMBER magical CONSTANT);
        Q_PROPERTY(int light MEMBER light CONSTANT);
        Q_PROPERTY(int shadow MEMBER shadow CONSTANT);


    public:
        int physical;
        int magical;
        int light;
        int shadow;
    };
}
#endif //REALMOFTEMPORALITY_DAMAGE_H
