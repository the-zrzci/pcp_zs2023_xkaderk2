//
// Created by Matěj on 14.11.2023.
//
#include "Inventory.h"

#include <QJsonArray>
#include <iostream>
#include "src/game/core/Game.h"
#include "src/data/entities.h"

game::Inventory::Inventory(int capacity, int width) {
    m_width = width;
    m_capacity = capacity;
    m_items.resize(capacity, nullptr);
}

game::Inventory::Inventory(QVector<items::Item*> chestItems) {
    m_items = chestItems;
    // tryin to make constructor to automaticly decide chest size
    if (m_items.size() > 4) {  //(probably rework this)
        m_width = m_items.size() / 2;
    } else {
        m_width = m_items.size();
    }
    m_capacity = m_items.size();
}

game::Inventory::Inventory(QVector<items::Item*> items, int capacity, int width) {
    m_width = width;
    m_capacity = capacity;
    m_items = items;
}

bool game::Inventory::addItem(game::items::Item* item) {
    for (int i = 0; i < m_capacity; ++i) {
        if (m_items.at(i) == nullptr) {
            m_items.replace(i, item);
            emit contentChanged();
            return true;
        }
    }
    // inventory is full
    return false;
}

void game::Inventory::removeItem(int index) {
    if (index < m_capacity && index >= 0) {
        m_items.replace(index, nullptr);
    }
    emit contentChanged();
}

void game::Inventory::removeItem(game::items::Item* item) {
    auto it = std::find(m_items.begin(), m_items.end(), item);

    if (it != m_items.end()) {
        int index = std::distance(m_items.begin(), it);
        m_items.replace(index, nullptr);
    }
    emit contentChanged();
}

QVector<game::items::Item*> game::Inventory::getItems() const {
    return m_items;
}

int game::Inventory::getWidth() const {
    return m_width;
}
int game::Inventory::getCapacity() const {
    return m_capacity;
}

void game::Inventory::saveState(QJsonObject& json) const {
    QJsonArray items;
    for (auto const& item : m_items) {
        if (item != nullptr) {
            items.append(item->getName());
        }
    }
    json["items"] = items;
}

void game::Inventory::loadState(game::core::Game* game, QJsonObject const& json) {
    QJsonArray items = json["items"].toArray();
    for (auto const& item : items) {
        game::items::Item* itemToAdd = data::getEntityAs<game::items::Item*>(game, item.toString());
        if (itemToAdd != nullptr) {
            addItem(itemToAdd);
        }
    }
}

void game::Inventory::moveItem(int from, int to) {
    if (to < 0 && m_items.at(from) != nullptr) {
        game::items::Item* item = m_items.at(from);
        this->removeItem(item);
        item->drop();
    }

    if (m_capacity > from && m_items.at(from) != nullptr) {
        // from is valid input
        game::items::Item* itemFrom = m_items.at(from);
        game::items::Item* itemTo = m_items.at(to);
        m_items.replace(to, itemFrom);
        m_items.replace(from, itemTo);

        emit contentChanged();
    }
}


int game::Inventory::getSize() {
    int size = 0;
    for (auto item : m_items) {
        if (item != nullptr) {
            size++;
        }
    }
    return size;
}
