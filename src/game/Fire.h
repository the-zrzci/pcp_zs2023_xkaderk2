#ifndef REALMOFTEMPORALITY_FIRE_H
#define REALMOFTEMPORALITY_FIRE_H

#include "Entity.h"

namespace game {
    class Fire : public game::Entity {
        Q_OBJECT;

        int m_damage;
        double m_lastCheck = 0;

      public:
        Fire(core::Game* game, const EntityParams& params, int damage);

        void setShadowed(float shadowed) override;
        void onCollision(const std::vector<Entity*>& entities) override;
        void update(double delta) override;
        void interact(Interaction interaction) override;
    };
}  // namespace game

#endif  // REALMOFTEMPORALITY_FIRE_H
