#ifndef REALMOFTEMPORALITY_ENTITY_H
#define REALMOFTEMPORALITY_ENTITY_H

#include <QJsonObject>
#include <QList>
#include <QObject>
#include <QPointF>
#include <QRectF>
#include <QSizeF>
#include <vector>

#include "core/Savable.h"

namespace game {

    namespace core {
        class Game;
        class Map;
    }  // namespace core

    struct SpriteSheet {
        Q_GADGET

        Q_PROPERTY(QString source MEMBER source CONSTANT);
        Q_PROPERTY(QSizeF frameSize MEMBER frameSize CONSTANT);
        Q_PROPERTY(QSizeF hitboxOffset MEMBER hitboxOffset CONSTANT);
        Q_PROPERTY(int innerHeight MEMBER innerHeight CONSTANT);

      public:
        QString source;
        QSizeF frameSize;
        QSizeF hitboxOffset;
        QList<int> stateFrameCount;
        QList<float> stateFrameFrequency;
        int innerHeight = 0;
    };

    struct EntityParams {
        Q_GADGET

        Q_PROPERTY(QString name MEMBER name CONSTANT);
        Q_PROPERTY(QString nickname MEMBER nickname CONSTANT);
        Q_PROPERTY(QSizeF hitbox MEMBER hitbox CONSTANT);
        Q_PROPERTY(SpriteSheet spriteSheet MEMBER spriteSheet CONSTANT);

      public:
        QString name;
        QSizeF hitbox;
        SpriteSheet spriteSheet;
        QString nickname = {};
    };

    class Entity : public QObject, public core::Savable {
        Q_OBJECT

        Q_PROPERTY(QString name READ getName CONSTANT);
        Q_PROPERTY(QString nickname READ getNickname CONSTANT);
        Q_PROPERTY(QPointF position MEMBER m_position NOTIFY moved);
        Q_PROPERTY(QSizeF hitbox READ getHitbox CONSTANT);
        Q_PROPERTY(SpriteSheet spriteSheet MEMBER m_spriteSheet CONSTANT);
        Q_PROPERTY(Type type READ getType CONSTANT);
        Q_PROPERTY(bool collidable READ isCollidable NOTIFY isCollidableChanged);
        Q_PROPERTY(QRectF boundingBox READ getBoundingBox NOTIFY moved);
        Q_PROPERTY(int spriteState READ getSpriteState NOTIFY spriteStateChanged);
        Q_PROPERTY(int spriteFrame READ getSpriteFrame NOTIFY spriteFrameChanged);
        Q_PROPERTY(float shadowed READ getShadowed NOTIFY shadowedChanged);
        Q_PROPERTY(Interaction interaction MEMBER m_interaction NOTIFY interactionChanged);

      public:
        enum Type { Creature, Object, Sensor };
        Q_ENUM(Type);

        enum Interaction { NoInteraction = 0, OpenInventory, Dialogue, Pickup, Remove };
        Q_ENUM(Interaction);

        QPointF m_position;
        QPointF m_lastPosition{-1, -1};
        int m_spriteState = 0;
        double m_spriteFrame = 0;
        float m_shadowed = 0;
        bool m_willDelete = false;

      protected:
        QString m_nickname;
        const QString m_name;
        const QSizeF m_hitbox;
        const SpriteSheet m_spriteSheet;
        bool m_collidable = true;
        Interaction m_interaction = Interaction::NoInteraction;

        const Type m_type;
        core::Game* m_game;

        void setSpriteState(int state);
        virtual int getSpriteState() const;

        int getSpriteStateFrameCount() const;
        float getSpriteStatePeriod() const;

        int getSpriteFrame() const;
        void setSpriteFrame(int frame);
        void progressSpriteFrame(double delta);
        bool progressSpriteFrame(double delta, bool loop);

        void setInteraction(Interaction interaction);
        Entity(core::Game* game, const EntityParams& params, Type type);

      public:
        Entity(core::Game* game, const EntityParams& params);

        void scheduleDelete();

        QString getName() const;
        QString getNickname() const;
        void setNickname(QString nickname);
        Type getType() const;

        game::core::Map* getCurrentMap() const;

        bool isCollidable() const;
        void setCollidable(bool collidable);

        QPointF getLastPosition();

        QPointF getPosition() const;
        void forcePosition(QPointF position);
        void setPosition(QPointF position);
        void setPosition(QPointF position, bool log);

        float getShadowed() const;
        virtual void setShadowed(float shadowed);

        QSizeF getHitbox() const;
        QRectF getBoundingBox() const;

        virtual void update(double delta);
        virtual void onDelete();
        virtual void onCollision(const std::vector<Entity*>& entities);

        Q_INVOKABLE virtual void interact(Interaction interaction);

        virtual void saveState(QJsonObject& json) const;
        virtual void loadState(QJsonObject const& json);

      signals:
        void moved(QPointF position);
        void isCollidableChanged();
        void spriteStateChanged();
        void spriteFrameChanged();
        void shadowedChanged();
        void interactionChanged();
        void interacted(Interaction interaction);
    };

    struct EntityTemplate : public EntityParams {
        std::function<Entity*(core::Game*, const EntityParams&)> create;
    };
}  // namespace game

#endif  // REALMOFTEMPORALITY_ENTITY_H
