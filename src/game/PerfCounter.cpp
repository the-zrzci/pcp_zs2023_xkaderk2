#include "PerfCounter.h"

#include <QDebug>

game::PerfCounter::PerfCounter(QObject* parent, QString name) : QObject(parent), m_name(name) {}

game::PerfCounter::~PerfCounter() {
    qDebug() << "**************************";
    qDebug() << "Performance results for " + m_name + ":";
    for (auto [name, time] : m_counters.asKeyValueRange()) {
        qDebug() << name + ":" << (time / 1000000.0) << "ms";
    }
}

void game::PerfCounter::start() {
    m_timer.start();
}

void game::PerfCounter::restart(QString name) {
    stop(name);
    start();
}

void game::PerfCounter::stop(QString name) {
    if (m_counters.contains(name)) {
        m_counters[name] = (m_counters[name] + m_timer.nsecsElapsed()) / 2;
    } else {
        m_counters[name] = m_timer.nsecsElapsed();
    }
}
