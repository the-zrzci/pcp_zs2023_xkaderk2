#ifndef REALMOFTEMPORALITY_PERF_COUNTER_H
#define REALMOFTEMPORALITY_PERF_COUNTER_H

#include <QElapsedTimer>
#include <QHash>
#include <QObject>

namespace game {
    class PerfCounter : public QObject {
        Q_OBJECT;

        QString m_name;
        QElapsedTimer m_timer;
        QHash<QString, qint64> m_counters;

      public:
        PerfCounter(QObject* parent, QString name);
        ~PerfCounter();

        void start();
        void restart(QString name);
        void stop(QString name);
    };
};  // namespace game

#endif  // REALMOFTEMPORALITY_PERF_COUNTER_H
