#ifndef REALMOFTEMPORALITY_QUEST_MANAGER_H
#define REALMOFTEMPORALITY_QUEST_MANAGER_H

#include <QJsonObject>
#include <QList>
#include <QObject>
#include <QSet>

#include "Quest.h"
#include "src/game/Entity.h"

namespace game {
    class Entity;
}
namespace game::core {
    class Game;
}

namespace game::quests {
    class QuestManager : public QObject {
        Q_OBJECT

        Q_PROPERTY(QSet<Quest*> active READ getActiveQuests NOTIFY activeQuestsChanged);

        game::core::Game* m_game;
        QSet<data::QuestId> m_doneQuests;
        QSet<Quest*> m_activeQuests;

        Quest* activateQuest(data::QuestId id);

        friend class game::core::Game;  // Allow starting quests directly

      public:
        QuestManager(game::core::Game* game);
        bool isQuestActive(data::QuestId id) const;
        bool canStartQuest(data::QuestId id) const;
        Q_INVOKABLE void startQuest(data::QuestId id);

        static QuestParams getQuestParams(data::QuestId id);

        QSet<Quest*> getActiveQuests() const;

        Q_INVOKABLE bool isQuestSubject(game::Entity const* entity) const;
        Q_INVOKABLE QList<game::EntityParams> getRewards(data::QuestId id) const;

        void saveState(QJsonObject& json) const;
        void loadState(const QJsonObject& json);

      private slots:
        void onQuestCompleted(game::quests::Quest* quest);

      signals:
        void questStarted(QuestParams quest);
        void questCompleted(QuestParams quest);
        void activeQuestsChanged();
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_QUEST_MANAGER_H
