#include "QuestManager.h"

#include <QJsonArray>

#include "../core/Game.h"
#include "questTypes/AssassinationQuest.h"
#include "questTypes/InteractionQuest.h"
#include "questTypes/ItemQuest.h"
#include "questTypes/LocationQuest.h"
#include "src/data/entities.h"
#include "src/data/levels.h"
#include "src/data/quests.h"
#include "src/game/items/Item.h"

game::quests::QuestManager::QuestManager(game::core::Game* game) : m_game(game) {}

game::quests::Quest* game::quests::QuestManager::activateQuest(data::QuestId id) {
    const QuestParams* questParams = data::getQuestParams(id);

    game::quests::Quest* quest;

    switch (questParams->type) {
        case QuestType::Location:
            quest = new LocationQuest(this, static_cast<const QuestParams*>(questParams), m_game);
            break;
        case QuestType::Interaction:
            quest = new InteractionQuest(this, static_cast<const InteractionQuestParams*>(questParams), m_game);
            break;
        case QuestType::Assassination:
            quest = new AssassinationQuest(this, static_cast<const AssassinationQuestParams*>(questParams), m_game);
            break;
        case QuestType::Item:
            quest = new ItemQuest(this, static_cast<const ItemQuestParams*>(questParams), m_game);
            break;
        default:
            throw std::runtime_error("Unknown quest type " + static_cast<int>(questParams->type));
    }

    connect(quest, &Quest::questCompleted, this, &QuestManager::onQuestCompleted);

    m_activeQuests.insert(quest);
    emit activeQuestsChanged();
    return quest;
}

bool game::quests::QuestManager::isQuestActive(data::QuestId id) const {
    for (auto* quest : m_activeQuests) {
        if (quest->getInfo().id == id) return true;
    }
    return false;
}

bool game::quests::QuestManager::canStartQuest(data::QuestId id) const {
    // If already started quest, return false
    if (isQuestActive(id)) return false;

    // If already done quest, return false
    if (m_doneQuests.contains(id)) return false;

    const QuestParams* questInfo = data::getQuestParams(id);

    // If missing prerequisites, return false
    for (auto prereq : questInfo->prerequisites) {
        if (!m_doneQuests.contains(prereq)) return false;
    }

    return true;
}

void game::quests::QuestManager::startQuest(data::QuestId id) {
    // Assert?
    if (!canStartQuest(id)) throw std::runtime_error("Tried starting quest without prerequisites done");

    auto* quest = activateQuest(id);
    emit questStarted(quest->getInfo());
}

game::quests::QuestParams game::quests::QuestManager::getQuestParams(data::QuestId id) {
    return *data::getQuestParams(id);
}

QSet<game::quests::Quest*> game::quests::QuestManager::getActiveQuests() const {
    return m_activeQuests;
}

bool game::quests::QuestManager::isQuestSubject(game::Entity const* entity) const {
    for (auto* quest : m_activeQuests) {
        if (entity->getName() == quest->getInfo().entity) return true;
    }

    return false;
}

void game::quests::QuestManager::saveState(QJsonObject& json) const {
    QJsonArray doneQuests;
    for (auto questId : m_doneQuests) {
        doneQuests.append(static_cast<int>(questId));
    }
    json["doneQuests"] = doneQuests;

    QJsonArray activeQuests;
    for (auto quest : m_activeQuests) {
        QJsonObject questState;
        quest->saveState(questState);
        activeQuests.append(QJsonObject({
          {"id", static_cast<int>(quest->getInfo().id)},
          {"state", questState},
        }));
    }
    json["activeQuests"] = activeQuests;
}

void game::quests::QuestManager::loadState(const QJsonObject& json) {
    m_doneQuests.clear();

    auto doneQuests = json.value("doneQuests").toArray();
    for (auto questId : doneQuests) {
        m_doneQuests.insert(data::QuestId(questId.toInt()));
    }

    for (auto quest : m_activeQuests) {
        quest->deleteLater();
    }
    m_activeQuests.clear();

    auto activeQuests = json.value("activeQuests").toArray();
    for (auto questRaw : activeQuests) {
        auto questJson = questRaw.toObject();

        auto* quest = activateQuest(data::QuestId(questJson.value("id").toInt()));
        quest->loadState(questJson.value("state").toObject());
    }
    emit activeQuestsChanged();
}

void game::quests::QuestManager::onQuestCompleted(game::quests::Quest* quest) {
    using game::quests::Quest;

    auto it = m_activeQuests.find(quest);
    if (it == m_activeQuests.end()) throw std::runtime_error("Inactive quest has completed?");

    m_activeQuests.erase(it);

    auto questParams = quest->getInfo();

    m_doneQuests.insert(questParams.id);
    emit activeQuestsChanged();
    emit questCompleted(questParams);

    auto* player = m_game->getPlayer();
    player->gainExpPoints(data::MAIN_QUEST_XP.at(player->getLevel() - 1));

    for (auto reward : questParams.rewards) {
        auto* item = data::getEntityAs<game::items::Item*>(m_game, reward);
        if (item == nullptr) {
            qWarning() << "Player should receive item" << reward << "but it doesn't exist.";
            continue;
        }
        if (player->getInventory()->addItem(item)) {
            item->game::Entity::interact(game::Entity::Pickup);
        } else {
            // Drop on floor
            item->setPosition(player->getPosition());
            m_game->getCurrentMap()->addEntity(item);
        }
    }

    if (static_cast<int>(questParams.followup)) {
        startQuest(questParams.followup);
    } else if (static_cast<int>(questParams.id) == static_cast<int>(data::QuestId::__MAX_QUEST_ID) - 1) {
        // final quest done
        emit m_game->playerWon();
    }

    quest->deleteLater();
}

QList<game::EntityParams> game::quests::QuestManager::getRewards(data::QuestId id) const {
    auto params = data::getQuestParams(id);
    QList<game::EntityParams> list;
    for (auto reward : params->rewards) {
        try {
            list.push_back(data::ENTITIES.at(reward));
        } catch (std::out_of_range) {
            qWarning() << "Cannot find reward item" << reward << "from quest" << static_cast<int>(id);
        }
    }
    return list;
}
