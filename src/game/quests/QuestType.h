#ifndef REALMOFTEMPORALITY_QUEST_TYPE_H
#define REALMOFTEMPORALITY_QUEST_TYPE_H

#include <QObject>

namespace game::quests {
    Q_NAMESPACE;

    enum class QuestType { Location, Interaction, Assassination, Item };
    Q_ENUM_NS(QuestType);
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_QUEST_TYPE_H
