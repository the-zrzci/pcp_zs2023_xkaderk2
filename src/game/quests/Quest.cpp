#include "Quest.h"

#include "../core/Game.h"

game::quests::Quest::Quest(QObject* parent, const QuestParams* params, game::core::Game* game)
  : QObject(parent),
    m_params(params),
    m_game(game) {
    connect(game, &game::core::Game::mapChanged, this, &Quest::mapChanged);
}

void game::quests::Quest::setProgress(int progress) {
    setProgress(progress, true);
}

void game::quests::Quest::setProgress(int progress, bool autoComplete) {
    m_progress = progress;
    emit progressChanged();
    if (autoComplete && m_progress >= m_params->count) {
        emit questCompleted(this);
    }
}

game::quests::QuestParams game::quests::Quest::getInfo() const {
    return *m_params;
}

bool game::quests::Quest::isOnThisMap() const {
    return m_game->getCurrentMap()->getId() == m_params->map;
}

game::core::Location game::quests::Quest::getTargetArea() const {
    return m_game->getCurrentMap()->getLocation(m_params->location);
}

void game::quests::Quest::saveState(QJsonObject& json) const {
    json["progress"] = m_progress;
}

void game::quests::Quest::loadState(const QJsonObject& json) {
    m_progress = json["progress"].toInt();
}
