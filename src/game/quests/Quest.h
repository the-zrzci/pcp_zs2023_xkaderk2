#ifndef REALMOFTEMPORALITY_QUEST_H
#define REALMOFTEMPORALITY_QUEST_H

#include <QObject>
#include <QString>
#include <vector>

#include "QuestType.h"
#include "src/game/core/Location.h"

namespace data {
    enum class QuestId;
    enum class MapId;
}  // namespace data

namespace game::core {
    class Game;
}

namespace game::quests {
    struct QuestParams {
        Q_GADGET;

        Q_PROPERTY(data::QuestId id MEMBER id CONSTANT);
        Q_PROPERTY(QString name MEMBER name CONSTANT);
        Q_PROPERTY(QString description MEMBER description CONSTANT);
        Q_PROPERTY(QuestType type MEMBER type CONSTANT);
        Q_PROPERTY(QString dialogue MEMBER dialogue CONSTANT);
        Q_PROPERTY(data::MapId map MEMBER map CONSTANT);
        Q_PROPERTY(QString location MEMBER location CONSTANT);
        Q_PROPERTY(QString entity MEMBER entity CONSTANT);
        Q_PROPERTY(int count MEMBER count CONSTANT);

      public:
        data::QuestId id;
        QString name;
        QString description;
        QuestType type;
        std::vector<data::QuestId> prerequisites = {};
        std::vector<QString> rewards = {};
        data::QuestId followup = data::QuestId(0);
        QString dialogue = {};
        data::MapId map;
        QString location = {};
        QString entity = {};
        int count = 1;

        bool isNull() const {
            return name.isNull();
        }
    };

    class Quest : public QObject {
        Q_OBJECT;

        Q_PROPERTY(QuestParams info READ getInfo CONSTANT);
        Q_PROPERTY(bool isOnThisMap READ isOnThisMap NOTIFY mapChanged);
        Q_PROPERTY(game::core::Location targetArea READ getTargetArea NOTIFY targetChanged);
        Q_PROPERTY(int maxProgress MEMBER m_maxProgress CONSTANT);
        Q_PROPERTY(int progress MEMBER m_progress NOTIFY progressChanged);

      protected:
        int m_maxProgress = 0;
        int m_progress = 0;
        game::core::Game* m_game;

        const QuestParams* m_params;

        void setProgress(int progress);
        void setProgress(int progress, bool autoComplete);

      public:
        Quest(QObject* parent, const QuestParams* params, game::core::Game* game);

        QuestParams getInfo() const;

        bool isOnThisMap() const;
        virtual game::core::Location getTargetArea() const;

        virtual void saveState(QJsonObject& json) const;
        virtual void loadState(const QJsonObject& json);

      signals:
        void questCompleted(Quest* quest);
        // Re-signal from game
        void mapChanged();
        void progressChanged();
        void targetChanged();
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_QUEST_H
