#ifndef REALMOFTEMPORALITY_INTERACTION_QUEST_H
#define REALMOFTEMPORALITY_INTERACTION_QUEST_H

#include "../Quest.h"
#include "src/game/Entity.h"

namespace game::quests {
    struct InteractionQuestParams : public QuestParams {
        game::Entity::Interaction interaction;
    };

    class InteractionQuest : public Quest {
        Q_OBJECT;

        const game::Entity::Interaction m_interaction;

      public:
        InteractionQuest(QObject* parent, const InteractionQuestParams* params, game::core::Game* game);

      private slots:
        void onInteracted(game::Entity* entity, game::Entity::Interaction interaction);
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_INTERACTION_QUEST_H
