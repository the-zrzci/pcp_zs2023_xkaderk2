#include "AssassinationQuest.h"

#include "src/game/core/Game.h"

game::quests::AssassinationQuest::AssassinationQuest(
  QObject* parent,
  const AssassinationQuestParams* params,
  game::core::Game* game
)
  : game::quests::Quest(parent, params, game),
    m_targetNickname(params->targetNickname),
    m_mapTied(params->mapTied) {
    connect(game, &game::core::Game::creatureKilled, this, &AssassinationQuest::onCreatureKilled);
    if (params->checkPrevious) {
        // Call recheck after the quest was created
        QMetaObject::invokeMethod(this, "recheck", Qt::QueuedConnection);
    }
}

bool game::quests::AssassinationQuest::isTarget(game::Entity* entity) const {
    return (m_params->entity.isNull() || m_params->entity == entity->getName()) &&
           (m_targetNickname.isNull() || m_targetNickname == entity->getNickname()) &&
           (!m_mapTied || m_params->map == entity->getCurrentMap()->getId());
}

void game::quests::AssassinationQuest::recheck() {
    // Check if it wasn't killed already
    std::vector<game::core::Map*> maps;

    if (m_mapTied) {
        maps.push_back(m_game->getMap(m_params->map));
    } else {
        for (auto pair : m_game->getMaps()) maps.push_back(pair.second);
    }

    for (auto* map : maps) {
        for (auto entity : map->getEntities()) {
            auto* creature = qobject_cast<game::creature::Creature*>(entity);
            if (creature == nullptr) continue;

            if (!creature->getAlive()) onCreatureKilled(creature, creature);
        }
    }
}

void game::quests::AssassinationQuest::onCreatureKilled(
  game::creature::Creature* victim,
  game::creature::Creature* killer
) {
    if (!isTarget(victim)) return;
    // TODO: check killer? but would have to be known from past too

    setProgress(m_progress + 1);
}
