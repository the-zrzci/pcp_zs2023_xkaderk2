#ifndef REALMOFTEMPORALITY_ITEM_QUEST_H
#define REALMOFTEMPORALITY_ITEM_QUEST_H

#include <QString>

#include "../Quest.h"
#include "src/game/Entity.h"
#include "src/game/items/Item.h"

namespace game::quests {
    struct ItemQuestParams : public QuestParams {
        QString dropOffLocation;
        QString item;
    };

    class ItemQuest : public Quest {
        Q_OBJECT;

        QString m_dropOffLocation;
        QString m_item;

        Q_INVOKABLE std::vector<game::items::Item*> findMatchingItemsInInventory();

      public:
        ItemQuest(QObject* parent, const ItemQuestParams* params, game::core::Game* game);

        game::core::Location getTargetArea() const override;

      private slots:
        void onInteracted(game::Entity* entity, game::Entity::Interaction interaction);
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_ITEM_QUEST_H
