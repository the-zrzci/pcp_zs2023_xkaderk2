#include "ItemQuest.h"

#include "src/game/core/Game.h"

game::quests::ItemQuest::ItemQuest(QObject* parent, const ItemQuestParams* params, game::core::Game* game)
  : game::quests::Quest(parent, params, game),
    m_item(params->item),
    m_dropOffLocation(params->dropOffLocation) {
    connect(game, &game::core::Game::playerInteractedWith, this, &ItemQuest::onInteracted);
    QMetaObject::invokeMethod(this, "findMatchingItemsInInventory", Qt::QueuedConnection);
}

std::vector<game::items::Item*> game::quests::ItemQuest::findMatchingItemsInInventory() {
    std::vector<game::items::Item*> found;
    for (auto item : m_game->getPlayer()->getInventory()->getItems()) {
        if (item != nullptr && item->getName() == m_item) found.push_back(item);
    }
    setProgress(found.size(), false);
    emit targetChanged();
    return found;
}

game::core::Location game::quests::ItemQuest::getTargetArea() const {
    return m_game->getCurrentMap()->getLocation(m_progress >= m_maxProgress ? m_dropOffLocation : m_params->location);
}

void game::quests::ItemQuest::onInteracted(game::Entity* entity, game::Entity::Interaction interaction) {
    // If it's item picked up
    if (interaction == game::Entity::Interaction::Pickup) {
        findMatchingItemsInInventory();
    }

    // Check if it's the right NPC
    if (entity->getName() == m_params->entity && interaction == game::Entity::Interaction::Dialogue && isOnThisMap()) {
        auto found = findMatchingItemsInInventory();

        if (m_progress >= m_params->count) {
            emit questCompleted(this);

            // Remove the items
            for (int i = 0; i < m_params->count; i++) {
                m_game->getPlayer()->getInventory()->removeItem(found[i]);
                found[i]->deleteLater();
            }
        }
    }
}
