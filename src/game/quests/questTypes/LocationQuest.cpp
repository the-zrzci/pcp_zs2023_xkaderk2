#include "LocationQuest.h"

#include "src/game/core/Game.h"

game::quests::LocationQuest::LocationQuest(QObject* parent, const QuestParams* params, game::core::Game* game)
  : game::quests::Quest(parent, params, game) {
    connect(game, &game::core::Game::playerEnteredLocation, this, &LocationQuest::onLocationChanged);
}

void game::quests::LocationQuest::onLocationChanged(QString location) {
    if (location == m_params->location && isOnThisMap()) emit questCompleted(this);
}
