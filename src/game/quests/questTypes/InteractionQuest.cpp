#include "InteractionQuest.h"

#include "src/game/core/Game.h"

game::quests::InteractionQuest::InteractionQuest(
  QObject* parent,
  const InteractionQuestParams* params,
  game::core::Game* game
)
  : game::quests::Quest(parent, params, game),
    m_interaction(params->interaction) {
    connect(game, &game::core::Game::playerInteractedWith, this, &InteractionQuest::onInteracted);
}

void game::quests::InteractionQuest::onInteracted(game::Entity* entity, game::Entity::Interaction interaction) {
    if (entity->getName() == m_params->entity && interaction == m_interaction && isOnThisMap()) {
        setProgress(m_progress + 1);
    }
}
