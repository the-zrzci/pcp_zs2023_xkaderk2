#ifndef REALMOFTEMPORALITY_LOCATION_QUEST_H
#define REALMOFTEMPORALITY_LOCATION_QUEST_H

#include "../Quest.h"

namespace game::quests {
    class LocationQuest : public Quest {
        Q_OBJECT;

      public:
        LocationQuest(QObject* parent, const QuestParams* params, game::core::Game* game);

      private slots:
        void onLocationChanged(QString location);
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_LOCATION_QUEST_H
