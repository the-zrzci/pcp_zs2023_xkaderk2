#ifndef REALMOFTEMPORALITY_ASSASSINATION_QUEST_H
#define REALMOFTEMPORALITY_ASSASSINATION_QUEST_H

#include <QString>

#include "../Quest.h"
#include "src/game/Entity.h"
#include "src/game/creature/Creature.h"

namespace game::quests {
    struct AssassinationQuestParams : public QuestParams {
        QString targetNickname = {};
        bool mapTied = false;
        bool checkPrevious = false;
    };

    class AssassinationQuest : public Quest {
        Q_OBJECT;

        const QString m_targetNickname;
        const bool m_mapTied;

        bool isTarget(game::Entity* entity) const;
        Q_INVOKABLE void recheck();

      public:
        AssassinationQuest(QObject* parent, const AssassinationQuestParams* params, game::core::Game* game);

      private slots:
        void onCreatureKilled(game::creature::Creature* victim, game::creature::Creature* killer);
    };
}  // namespace game::quests

#endif  // REALMOFTEMPORALITY_ASSASSINATION_QUEST_H
