#include "Multiplayer.h"

#include "Game.h"
#include "src/data/entities.h"
#include "src/game/creature/Creature.h"

game::core::Multiplayer::Multiplayer(Game* game) : QObject(game), m_game(game), m_workerThread(this) {
    m_workerThread.setObjectName("MultiplayerThread");
    m_worker = new MultiplayerWorker();
    m_worker->moveToThread(&m_workerThread);
    connect(&m_workerThread, &QThread::finished, m_worker, &QObject::deleteLater);
    connect(&m_workerThread, &QThread::started, m_worker, &MultiplayerWorker::initialize);
    m_workerThread.start();
}

game::core::Multiplayer::~Multiplayer() {
    m_workerThread.quit();
    m_workerThread.wait();
}

void game::core::Multiplayer::listen(int port) {
    m_worker->onReady([=]() { QMetaObject::invokeMethod(m_worker, "listen", Q_ARG(int, port)); });
}

void game::core::Multiplayer::connectToHost(QString hostname, int port) {
    m_worker->onReady([=]() {
        QMetaObject::invokeMethod(m_worker, "disconnectFromHost");

        QMetaObject::invokeMethod(m_worker, "connectToHost", Q_ARG(QString, hostname), Q_ARG(int, port));
    });
}

QJsonObject game::core::Multiplayer::createStateJson() const {
    QJsonObject json;

    // Save other players
    for (auto [playerId, player] : m_worker->m_players.asKeyValueRange()) {
        if (player.entity == nullptr) continue;

        QJsonObject playerStateJson;
        player.entity->saveState(playerStateJson);

        json[playerId] = QJsonObject{
          {"name", player.entity->getName()},
          {"state", playerStateJson},
        };
    }

    // Save our player
    QJsonObject playerStateJson;
    m_game->getPlayer()->saveState(playerStateJson);

    json[m_worker->getOwnAddress()] = QJsonObject{
      {"name", m_game->getPlayer()->getName()},
      {"state", playerStateJson},
    };

    return json;
}

QByteArray game::core::Multiplayer::encodeJson(QJsonObject const& json) {
    return QJsonDocument(json).toJson(QJsonDocument::JsonFormat::Compact);
}

void game::core::Multiplayer::update() {
    static std::vector<QString> removePlayers;

    if (!m_timer.hasExpired(MIN_DELAY) || !m_worker->m_mutex.tryLock(1)) return;
    m_timer.start();

    auto stateJson = createStateJson();
    stateJson["__isResponse"] = true;
    auto responseData = encodeJson(stateJson);
    stateJson["__isResponse"] = false;
    auto stateData = encodeJson(stateJson);

    for (auto playerId : m_worker->m_players.keys()) {
        auto player = &m_worker->m_players[playerId];
        // Process incoming data
        if (!player->lastData.isEmpty()) {
            // Create new player
            if (player->entity == nullptr) {
                QString name = player->lastData.value("name").toString();
                player->entity = data::getEntityAs<game::creature::Creature*>(m_game, name);

                // Fallback to Petr Pavel
                if (player->entity == nullptr) {
                    qWarning() << "Player has entity name" << name
                               << "but no such entity found. Using Petr Pavel instead.";
                    player->entity = data::getEntityAs<game::creature::Creature*>(m_game, "Mage");
                }
                player->entity->setActive(false);

                m_game->getCurrentMap()->addEntity(player->entity);
            }

            // Update the player
            player->ttl = MAX_TTL;
            player->entity->loadState(player->lastData.value("state").toObject());
            player->lastData = {};
        }
        // Otherwise count down ttl
        else if (--player->ttl <= 0) {
            removePlayers.push_back(playerId);
        }
    }

    // Send back responses
    for (auto datagram : m_worker->m_respondTo) {
        QMetaObject::invokeMethod(
          m_worker,
          "writeDatagram",
          Qt::QueuedConnection,
          Q_ARG(QNetworkDatagram, datagram.makeReply(responseData))
        );
    }
    m_worker->m_respondTo.clear();

    // Send state to host
    if (m_worker->isConnected()) {
        QMetaObject::invokeMethod(m_worker, "write", Qt::QueuedConnection, Q_ARG(QByteArray, stateData));
    }

    // Remove inactive players
    for (auto playerId : removePlayers) {
        auto* entity = m_worker->m_players[playerId].entity;

        if (entity != nullptr) entity->scheduleDelete();

        m_worker->m_players.remove(playerId);
    }
    removePlayers.clear();
    m_worker->m_mutex.unlock();
}

//

game::core::MultiplayerWorker::MultiplayerWorker() {}

void game::core::MultiplayerWorker::initialize() {
    m_socket = new QUdpSocket(this);

    connect(m_socket, &QUdpSocket::readyRead, this, &MultiplayerWorker::readPendingDatagrams);
    connect(m_socket, &QUdpSocket::stateChanged, this, &MultiplayerWorker::stateChanged);

    m_ready = true;
    emit ready();
}

void game::core::MultiplayerWorker::stateChanged() {
    switch (m_socket->state()) {
        case QAbstractSocket::BoundState:
            qDebug() << "Listening on" << getOwnAddress();
            break;
        case QAbstractSocket::ConnectedState:
            qDebug() << "Connected to" << getHostAddress();
            break;
    }
}

QString game::core::MultiplayerWorker::getSenderAddress(QNetworkDatagram datagram) {
    return datagram.senderAddress().toString() + ":" + QString::number(datagram.senderPort());
}

QString game::core::MultiplayerWorker::getOwnAddress() const {
    return m_socket->localAddress().toString() + ":" + QString::number(m_socket->localPort());
}

QString game::core::MultiplayerWorker::getHostAddress() const {
    return m_socket->peerAddress().toString() + ":" + QString::number(m_socket->peerPort());
}

bool game::core::MultiplayerWorker::isConnected() const {
    return !m_socket->peerAddress().isNull();
}

bool game::core::MultiplayerWorker::isReady() const {
    return m_ready;
}

void game::core::MultiplayerWorker::onReady(std::function<void()> const& callback) {
    if (m_ready) {
        callback();
    } else {
        connect(this, &MultiplayerWorker::ready, this, callback);
    }
}

void game::core::MultiplayerWorker::listen(int port) {
    m_socket->bind(QHostAddress::Any, port);
}

void game::core::MultiplayerWorker::connectToHost(QString hostname, int port) {
    disconnectFromHost();

    m_socket->connectToHost(hostname, port);
}

void game::core::MultiplayerWorker::disconnectFromHost() {
    m_socket->disconnectFromHost();
}

void game::core::MultiplayerWorker::writeDatagram(QNetworkDatagram const& datagram) {
    m_socket->writeDatagram(datagram);
}

void game::core::MultiplayerWorker::write(QByteArray const& data) {
    m_socket->write(data);
}

void game::core::MultiplayerWorker::readPendingDatagrams() {
    QString ownId = getOwnAddress();

    while (m_socket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = m_socket->receiveDatagram();
        if (!datagram.isValid()) continue;

        QString sender = getSenderAddress(datagram);

        QJsonDocument data = QJsonDocument::fromJson(datagram.data());
        if (data.isNull()) {
            qInfo() << "Failed to parse data from" << sender;
            continue;
        }

        bool isResponse = data.object().take("__isResponse").toBool(true);

        m_mutex.lock();
        QJsonObject players = data.object();
        for (auto playerId : players.keys()) {
            if (playerId == ownId) continue;

            // Create new record if needed
            if (!m_players.contains(playerId)) {
                m_players[playerId] = {};
            }

            // Update
            auto player = &m_players[playerId];
            player->lastData = players.value(playerId).toObject();
        }

        // Don't respond to responses
        if (!isResponse) m_respondTo[sender] = datagram;
        m_mutex.unlock();
    }
}
