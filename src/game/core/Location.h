#ifndef REALMOFTEMPORALITY_LOCATION_H
#define REALMOFTEMPORALITY_LOCATION_H

#include <QJsonObject>
#include <QObject>
#include <QPointF>
#include <QRectF>
#include <QSizeF>

namespace data {
    enum class MapId;
}

namespace game::core {
    struct Location : public QRectF {
        Q_GADGET

        Q_PROPERTY(QString name MEMBER name CONSTANT);
        Q_PROPERTY(Type type MEMBER type CONSTANT);
        Q_PROPERTY(qreal x READ x CONSTANT);
        Q_PROPERTY(qreal y READ y CONSTANT);
        Q_PROPERTY(qreal width READ width CONSTANT);
        Q_PROPERTY(qreal height READ height CONSTANT);
        Q_PROPERTY(QString target MEMBER target CONSTANT);

      public:
        enum Type { Point, Area };
        Q_ENUM(Type);

        QString name;
        Type type;
        QString target;

        // Assigned when loading
        data::MapId map;

        QPointF position() const;

        void saveParameters(QJsonObject& json) const;
        static Location load(data::MapId mapId, const QJsonObject& json);
    };
}  // namespace game::core

#endif  // REALMOFTEMPORALITY_LOCATION_H
