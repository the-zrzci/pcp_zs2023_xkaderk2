#include "Game.h"

#include <QFile>
#include <QJsonDocument>
#include <QQuickWindow>
#include <iostream>

#include "src/data/entities.h"
#include "src/data/tiles.h"

game::core::Game::Game() {
    m_questManager = new quests::QuestManager(this);
}

void game::core::Game::setMaps(std::initializer_list<Map*> maps) {
    for (auto map : maps) {
        map->setParent(this);

        for (auto location : map->getLocations()) {
            if (m_locations.contains(location.name)) {
                qWarning() << "Duplicate location with name" << location.name << "on map"
                           << static_cast<int>(location.map) << "and"
                           << static_cast<int>(m_locations[location.name].map);
            }
            m_locations[location.name] = location;
        }

        m_maps[map->getId()] = map;
    }
}

void game::core::Game::setPlayer(game::creature::Creature* player) {
    m_player = player;
    emit playerChanged();
}

game::core::Game::~Game() {
    for (auto map : m_maps) delete map.second;
}

void game::core::Game::setEntityMap(game::Entity* entity, data::MapId mapId) {
    auto* oldMap = entity->getCurrentMap();
    if (oldMap != nullptr && oldMap->getId() != mapId) {
        oldMap->removeEntity(entity);
    }
    m_maps[mapId]->addEntity(entity);
}

void game::core::Game::saveGame() const {
    saveStateToFile("./save_game.json");
}
void game::core::Game::loadGame() {
    loadStateFromFile("./save_game.json");
}

void game::core::Game::saveState(QJsonObject& json) const {
    QJsonObject quests;
    m_questManager->saveState(quests);
    json["quests"] = quests;

    QJsonObject playerStateJson;
    m_player->saveState(playerStateJson);

    QJsonObject playerJson;
    playerJson["name"] = m_player->getName();
    playerJson["state"] = playerStateJson;
    json["player"] = playerJson;

    // QJsonObject maps;
    // for (auto map : m_maps) {
    //     QJsonObject mapJson;
    //     map.second->saveState(mapJson);
    //     maps[QString::number(static_cast<int>(map.first))] = mapJson;
    // }
    // json["maps"] = maps;
}

void game::core::Game::saveStateToFile(QString url) const {
    QJsonObject json;
    saveState(json);

    QFile file(url);
    if (!file.open(QIODevice::WriteOnly)) {
        throw std::runtime_error("Failed to open file " + url.toStdString());
    }

    QJsonDocument document(json);
    file.write(document.toJson(QJsonDocument::Compact));
    file.close();
}

void game::core::Game::loadState(const QJsonObject& json) {
    m_questManager->loadState(json.value("quests").toObject());

    if (json.contains("player")) {
        if (m_player != nullptr) {
            m_player->scheduleDelete();
        }

        QJsonObject playerJson = json.value("player").toObject();
        auto* player = data::getEntityAs<game::creature::Creature*>(this, playerJson.value("name").toString());
        player->loadState(playerJson.value("state").toObject());
        setPlayer(player);
    }

    // Force reload frontend
    emit mapChanged();
    emit playerTeleported();

    // auto maps = json.value("maps").toObject();
    // for (auto mapKey : maps.keys()) {
    //     auto mapJson = maps.value(mapKey).toObject();
    //     auto mapId = data::MapId(mapKey.toInt());
    //     m_maps[mapId]->loadState(this, mapJson);
    // }
}

void game::core::Game::loadStateFromFile(QString path) {
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error("Failed to open file " + path.toStdString());
    }

    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    if (doc.isNull()) {
        throw std::runtime_error("Failed to parse file " + path.toStdString());
    }

    loadState(doc.object());
}

game::quests::QuestManager* game::core::Game::getQuestManager() const {
    return m_questManager;
}

bool game::core::Game::isPaused() const {
    return m_paused;
}

void game::core::Game::setPaused(bool paused) {
    m_paused = paused;
    emit pausedChanged(paused);
}

void game::core::Game::update() {
    static const double TO_SEC = 1 / 1000.0;
    static const double MIN_DELTA = 1 / 512.0 / TO_SEC;  // Max 512 FPS to prevent weird stuff

    if (!m_deltaTimer.hasExpired(MIN_DELTA)) return;  // Skip too short delta

    m_delta = m_deltaTimer.restart();
    if (m_fps < 0 || m_delta == 0) {
        // Skip first frame
        m_fps = 0;
        return;
    } else if (m_fps == 0) {
        // Don't average second frame
        m_fps = 1000 / m_delta;
    } else {
        // Calculate weighted average
        m_fps = m_fps * 0.9 + (1000 / m_delta) * 0.1;
    }
    emit fpsUpdated(m_fps);

    if (m_paused) return;  // Don't do anything when paused

    double delta = m_delta * TO_SEC;  // convert delta to seconds

    // Do movement
    QPointF direction;
    if (m_controls.up) direction.ry() -= 1;
    if (m_controls.down) direction.ry() += 1;
    if (m_controls.left) direction.rx() -= 1;
    if (m_controls.right) direction.rx() += 1;
    if (!direction.isNull()) {
        if (direction.x() != 0 && direction.y() != 0) {
            direction *= 0.75;  // Pythagorean Theorem: Engineer Edition
        }
        m_player->move(direction, m_controls.sprint);
    }

    // Update multiplayer
    if (m_multiplayer != nullptr) m_multiplayer->update();

    // Update map
    getCurrentMap()->update(delta);

    // Check locations
    QString currentLocation = getCurrentMap()->findPointLocation(m_player->getPosition());
    if (currentLocation != m_lastLocation) {
        if (currentLocation.isNull()) {
            emit playerLeftLocation(m_lastLocation);
        } else {
            emit playerEnteredLocation(currentLocation);

            Location location = m_locations[currentLocation];
            if (!location.target.isNull()) {
                if (!m_locations.contains(location.target)) {
                    qWarning() << "Player should be teleported to" << location.target
                               << "but that location was not found.";
                } else {
                    Location target = m_locations[location.target];
                    auto offset = m_player->getPosition() - location.position();
                    offset.rx() = (offset.x() / location.width()) * target.width();
                    offset.ry() = (offset.y() / location.height()) * target.height();

                    changeMap(target.map, target.position() + offset);
                    currentLocation = target.name;
                }
            }
        }
        m_lastLocation = currentLocation;
    }
}

void game::core::Game::timerEvent(QTimerEvent* event) {
    update();
}

void game::core::Game::initializeMultiplayer() {
    if (m_multiplayer != nullptr) return;

    m_multiplayer = new game::core::Multiplayer(this);
}

void game::core::Game::start() {
    m_timer = startTimer(16, Qt::TimerType::PreciseTimer);
}

void game::core::Game::stop() {
    killTimer(m_timer);
}

void game::core::Game::changeMap(data::MapId mapId, QString location) {
    setEntityMap(m_player, mapId);
    emit mapChanged();

    try {
        getCurrentMap()->teleportEntity(m_player, location);
    } catch (std::out_of_range) {
        qWarning() << "Location" << location << "not found.";
    }
    emit playerTeleported();  // even if not
}
void game::core::Game::changeMap(data::MapId mapId, QPointF location) {
    if (!m_maps.count(mapId)) throw std::out_of_range("No such map");
    setEntityMap(m_player, mapId);
    emit mapChanged();

    getCurrentMap()->teleportEntity(m_player, location);
    emit playerTeleported();  // even if not
}

game::core::Map* game::core::Game::getCurrentMap() const {
    return m_player->getCurrentMap();
}

game::core::Map* game::core::Game::getMap(data::MapId mapId) const {
    return m_maps.at(mapId);
}

std::unordered_map<data::MapId, game::core::Map*> const& game::core::Game::getMaps() const {
    return m_maps;
}

game::creature::Creature* game::core::Game::getPlayer() const {
    return m_player;
}

QString game::core::Game::executeCommand(QString command) {
    if (command.isEmpty()) return {};

    auto args = command.split(" ");
    if (args[0] == "noclip") {
        // Noclip command
        m_player->setCollidable(!m_player->isCollidable());

    } else if (args[0] == "givexp") {
        m_player->gainExpPoints(args[1].toInt());

    } else if (args[0] == "sethealth") {
        // sets players health
        if (args.size() < 2) {
            return "Command usage: `sethealth <hp>`";
        }
        m_player->setHealth(args[1].toInt());

    } else if (args[0] == "revive") {
        // Revive player
        m_player->revive();

    } else if (args[0] == "setenergy") {
        // sets players energy
        if (args.size() < 2) {
            return "Command usage: `setenergy <energy>`";
        }
        m_player->setEnergy(args[1].toInt());

    } else if (args[0] == "setlvl") {
        // Set level command
        if (args.size() < 2) {
            return "Command usage: `setlvl <lvl>`";
        }
        m_player->setLevel(args[1].toInt());

    } else if (args[0] == "penance") {
        // Hurt yourself
        if (args.size() < 2) {
            return "Command usage: `penance <damage>`";
        }
        m_player->damage({args[1].toInt()}, nullptr);

    } else if (args[0] == "win") {
        // Win the game
        emit playerWon();

    } else if (args[0] == "spawn") {
        // Spawn an entity
        auto entityName = args.sliced(1).join(" ");
        try {
            auto entityTemplate = data::ENTITIES.at(entityName);
            auto* entity = entityTemplate.create(this, entityTemplate);
            entity->setPosition(m_player->getPosition());
            getCurrentMap()->addEntity(entity);
        } catch (std::out_of_range) {
            return "Entity '" + entityName + "' not found.";
        }

    } else if (args[0] == "tp") {
        // Teleport player to location
        auto locationName = args.sliced(1).join(" ");
        try {
            getCurrentMap()->teleportEntity(m_player, locationName);
        } catch (std::out_of_range) {
            return "Location '" + locationName + "' not found.";
        }

    } else if (args[0] == "setmap") {
        // Change player's map
        data::MapId mapId = data::MapId(args.at(1).toInt());
        try {
            changeMap(mapId, m_player->getPosition());
        } catch (std::out_of_range) {
            return "Map " + args.at(1) + " not found.";
        }

    } else if (args[0] == "startquest") {
        // Force start quest with given id
        data::QuestId questId = data::QuestId(args.at(1).toInt());
        try {
            m_questManager->activateQuest(questId);
            emit m_questManager->activeQuestsChanged();
        } catch (std::out_of_range) {
            return "Quest " + args.at(1) + " not found.";
        }

    } else if (args[0] == "listen") {
        // Start server
        if (args.size() < 2) {
            return "Command usage: `listen <port>`";
        }

        if (m_multiplayer == nullptr) initializeMultiplayer();
        m_multiplayer->listen(args[1].toInt());
        return "Starting listener...";

    } else if (args[0] == "connect") {
        // Connect to server
        if (args.size() < 3) {
            return "Command usage: `connect <host> <port>`";
        }

        if (m_multiplayer == nullptr) initializeMultiplayer();
        m_multiplayer->connectToHost(args[1], args[2].toInt());
        return "Connecting...";

    } else if (args[0] == "soulswap") {
        // Swap souls with an NPC
        auto npcs = getCurrentMap()->findCreaturesInRange(m_player, 1);

        game::creature::Creature* npc = nullptr;
        qreal npcDistance = -1;
        // Find suitable host
        for (auto* item : npcs) {
            // Find closest npc
            QPointF delta = m_player->getPosition() - item->getPosition();
            qreal distance = pow(delta.x(), 2) + pow(delta.y(), 2);
            if (npcDistance != -1 && npcDistance < distance) continue;

            npc = static_cast<game::creature::Creature*>(item);
            npcDistance = distance;
        }
        if (npc != nullptr) {
            auto old_player = m_player;
            m_player = npc;

            auto ai = npc->getAIController();
            old_player->setAIController(ai);
            m_player->setAIController(nullptr);
            emit playerChanged();
        } else {
            return "No suitable host for soulswap found.";
        }

    } else {
        return "Command '" + args[0] + "' is not defined.";
    }
    return {};
}
