#include "Location.h"

#include "data/MapId.h"
#include "utils.h"

QPointF game::core::Location::position() const {
    return {x(), y()};
}

void game::core::Location::saveParameters(QJsonObject& json) const {
    json["name"] = name;
    json["type"] = type;
    json["x"] = x();
    json["y"] = y();
    if (type == game::core::Location::Area) {
        json["width"] = width();
        json["height"] = height();
        if (!target.isEmpty()) {
            json["target"] = target;
        }
    }
}

game::core::Location game::core::Location::load(data::MapId mapId, const QJsonObject& json) {
    Type type = Type(json.value("type").toInt());

    return {
      {
        json.value("x").toDouble(),
        json.value("y").toDouble(),
        type == Type::Area ? json.value("width").toDouble() : 0,
        type == Type::Area ? json.value("height").toDouble() : 0,
      },
      json.value("name").toString(),
      type,
      json.value("target").toString(),
      mapId,
    };
}
