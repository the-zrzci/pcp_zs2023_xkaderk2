#include "Controls.h"

bool game::core::Controls::operator==(const Controls& r) {
    return up == r.up && down == r.down && left == r.left && right == r.right && sprint == r.sprint;
}
bool game::core::Controls::operator!=(const Controls& r) {
    return !(*this == r);
}
