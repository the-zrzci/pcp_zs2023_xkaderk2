#ifndef REALMOFTEMPORALITY_CONTROLS_H
#define REALMOFTEMPORALITY_CONTROLS_H

#include <QObject>

namespace game::core {
    struct Controls {
        Q_GADGET

        Q_PROPERTY(bool up MEMBER up);
        Q_PROPERTY(bool down MEMBER down);
        Q_PROPERTY(bool left MEMBER left);
        Q_PROPERTY(bool right MEMBER right);
        Q_PROPERTY(bool sprint MEMBER sprint);

      public:
        bool up = false;
        bool down = false;
        bool left = false;
        bool right = false;
        bool sprint = false;

        bool operator==(const Controls& r);
        bool operator!=(const Controls& r);
    };
}  // namespace game::core

#endif  // REALMOFTEMPORALITY_CONTROLS_H
