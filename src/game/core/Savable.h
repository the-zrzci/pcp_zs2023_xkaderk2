#ifndef REALMOFTEMPORALITY_SAVABLE_H
#define REALMOFTEMPORALITY_SAVABLE_H

#include <QJsonObject>

namespace game::core {
    class Savable {
      public:
        virtual void saveState(QJsonObject& json) const;
        virtual void loadState(QJsonObject& json);
    };
}  // namespace game::core

#endif  // REALMOFTEMPORALITY_SAVABLE_H
