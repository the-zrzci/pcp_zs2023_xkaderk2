#ifndef REALMOFTEMPORALITY_TILE_H
#define REALMOFTEMPORALITY_TILE_H

#include <QJsonObject>
#include <QMap>
#include <QObject>
#include <QString>

#include "Savable.h"

namespace game::core {

    struct TileBase {
        Q_GADGET

        Q_PROPERTY(QString name MEMBER name CONSTANT);
        Q_PROPERTY(bool isPassable MEMBER isPassable CONSTANT);
        Q_PROPERTY(bool isWall MEMBER isWall CONSTANT);
        Q_PROPERTY(QString texture MEMBER texture CONSTANT);
        Q_PROPERTY(QString top MEMBER top CONSTANT);
        Q_PROPERTY(QString left MEMBER left CONSTANT);

      public:
        QString name;
        bool isPassable = false;
        bool isWall = false;

        QString texture;
        QString top;
        QString left;
    };

    struct Tile : public Savable {
        Q_GADGET

        Q_PROPERTY(QString baseName MEMBER baseName CONSTANT);
        Q_PROPERTY(int height MEMBER height CONSTANT);

        Q_PROPERTY(TileBase const* base MEMBER base CONSTANT);
        Q_PROPERTY(float rightShadowed MEMBER rightShadowed CONSTANT);
        Q_PROPERTY(float leftShadowed MEMBER leftShadowed CONSTANT);

      public:
        Tile();
        Tile(QString baseName);
        Tile(QString baseName, int height);

        QString baseName;
        int height = 0;

        //
        // ############
        // Automatically generated
        // ############

        TileBase const* base = nullptr;

        float rightShadowed = 0;
        float leftShadowed = 0;

        Q_INVOKABLE TileBase getBase() const;
        Q_INVOKABLE bool isEmpty() const;

        void saveParameters(QJsonObject& json) const;
        static Tile load(const QJsonObject& json);

        friend bool operator==(const Tile& lhs, const Tile& rhs);
    };

    bool operator==(const game::core::Tile& lhs, const game::core::Tile& rhs);

}  // namespace game::core

#endif  // REALMOFTEMPORALITY_TILE_H
