#include "utils.h"

QJsonArray game::core::qPairToJson(QPoint const& point) {
    return qPairToJson(point.toPointF());
}
QJsonArray game::core::qPairToJson(QPointF const& point) {
    return {point.x(), point.y()};
}
QJsonArray game::core::qPairToJson(QSize const& size) {
    return qPairToJson(size.toSizeF());
}
QJsonArray game::core::qPairToJson(QSizeF const& size) {
    return {size.width(), size.height()};
}

QJsonArray game::core::qListToJson(QList<int> const& list) {
    QJsonArray arr;
    for (int item : list) {
        arr.append(item);
    }
    return arr;
}

QList<int> game::core::jsonToQListInt(QJsonValue const& json) {
    QList<int> list;
    QJsonArray arr = json.toArray();
    for (QJsonValue item : arr) {
        list.push_back(item.toInteger());
    }
    return list;
}

QPoint game::core::jsonToPoint(QJsonValue const& json) {
    auto arr = json.toArray();
    return {arr.at(0).toInt(), arr.at(1).toInt()};
}

QPointF game::core::jsonToPointF(QJsonValue const& json) {
    auto arr = json.toArray();
    return {arr.at(0).toDouble(), arr.at(1).toDouble()};
}

QSize game::core::jsonToSize(QJsonValue const& json) {
    auto arr = json.toArray();
    return {arr.at(0).toInt(), arr.at(1).toInt()};
}

QSizeF game::core::jsonToSizeF(QJsonValue const& json) {
    auto arr = json.toArray();
    return {arr.at(0).toDouble(), arr.at(1).toDouble()};
}
