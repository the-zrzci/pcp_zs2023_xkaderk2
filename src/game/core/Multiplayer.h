#ifndef REALMOFTEMPORALITY_MULTIPLAYER_H
#define REALMOFTEMPORALITY_MULTIPLAYER_H

#include <QElapsedTimer>
#include <QHash>
#include <QJsonDocument>
#include <QMutex>
#include <QNetworkDatagram>
#include <QObject>
#include <QString>
#include <QThread>
#include <QUdpSocket>
#include <vector>

#include "src/game/creature/Creature.h"

namespace game::core {
    class Game;
    class MultiplayerWorker;

    class Multiplayer : public QObject {
        Q_OBJECT

        // Client will be removed after MAX_TTL ticks of inactivity
        static const unsigned short MAX_TTL = 16;
        // Minimal number of miliseconds between updates
        static constexpr double MIN_DELAY = 1000 / 14.0;  // 14 FPS

        QThread m_workerThread;
        MultiplayerWorker* m_worker;
        QElapsedTimer m_timer;

        Game* m_game;

        QJsonObject createStateJson() const;
        static QByteArray encodeJson(QJsonObject const& json);

      public:
        Multiplayer(Game* game);
        ~Multiplayer();

        void listen(int port);
        void connectToHost(QString hostname, int port);

        void update();
    };

    class MultiplayerWorker : public QObject {
        Q_OBJECT

        QUdpSocket* m_socket;
        bool m_ready = false;

        struct PlayerRecord {
            game::creature::Creature* entity = nullptr;
            unsigned short ttl;
            QJsonObject lastData;
        };

        QMutex m_mutex;
        QHash<QString, PlayerRecord> m_players;
        QHash<QString, QNetworkDatagram> m_respondTo;

        friend class Multiplayer;

      private slots:
        void readPendingDatagrams();
        void stateChanged();

      public:
        MultiplayerWorker();
        void initialize();

        static QString getSenderAddress(QNetworkDatagram datagram);
        QString getOwnAddress() const;
        QString getHostAddress() const;
        bool isConnected() const;
        bool isReady() const;

        void onReady(std::function<void()> const& callback);

        Q_INVOKABLE void listen(int port);
        Q_INVOKABLE void connectToHost(QString hostname, int port);
        Q_INVOKABLE void disconnectFromHost();

        Q_INVOKABLE void writeDatagram(QNetworkDatagram const& datagram);
        Q_INVOKABLE void write(QByteArray const& data);

      signals:
        void ready();
    };
};  // namespace game::core

#endif  // REALMOFTEMPORALITY_MULTIPLAYER_H
