#ifndef REALMOFTEMPORALITY_MAP_H
#define REALMOFTEMPORALITY_MAP_H

#include <QColor>
#include <QHash>
#include <QList>
#include <QObject>
#include <QPointF>
#include <QString>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "Location.h"
#include "Tile.h"
#include "src/game/Entity.h"
#include "src/game/creature/Creature.h"

namespace data {
    enum class MapId;
}

namespace game::core {
    // For debug purposes
    struct RangeCheckLog {
        Q_GADGET;

        Q_PROPERTY(QPointF center MEMBER center CONSTANT);
        Q_PROPERTY(qreal radius MEMBER radius CONSTANT);
        Q_PROPERTY(qreal minAngle MEMBER minAngle CONSTANT);
        Q_PROPERTY(qreal maxAngle MEMBER maxAngle CONSTANT);

      public:
        QPointF center;
        qreal radius;
        qreal minAngle;
        qreal maxAngle;
    };

    class Map : public QObject {
        Q_OBJECT

        Q_PROPERTY(int width READ getWidth CONSTANT);
        Q_PROPERTY(int height READ getHeight CONSTANT);
        Q_PROPERTY(QList<Entity*> entities READ getEntities CONSTANT);
        Q_PROPERTY(QList<RangeCheckLog> lastRangeChecks READ getRangeChecks NOTIFY rangeChecksChanged);

        data::MapId m_mapId;
        QString m_defaultTile;
        std::vector<std::vector<Tile>> m_tiles;
        std::unordered_map<Entity::Type, std::unordered_set<Entity*>> m_entities;
        std::vector<Entity*> m_removeEntities;  // Entities to be removed next update
        QHash<QString, Location> m_locations;
        QList<RangeCheckLog> m_lastRangeChecks;  // for debug

        bool m_isUpdating = false;

        QList<RangeCheckLog> getRangeChecks() const;

        void buildShadows();
        void updateShadowed(game::Entity* entity) const;
        void removeEntityNow(game::Entity* entity);

        static QPointF resolveCollision(QRectF dynamicBBox, QRectF staticBBox);
        static bool collidesWithCircle(QRectF bbox, QPointF position, qreal radius);
        QPointF collideWithTiles(QRectF entityBBox) const;
        QPointF collideWithEntities(QRectF entityBBox, bool onlyObjects, std::vector<game::Entity*>& collided) const;

      public:
        Map(
          data::MapId mapId,
          QString defaultTile,
          std::vector<std::vector<Tile>> tiles,
          std::vector<Entity*> entities,
          std::vector<Location> locations
        );

        data::MapId getId() const;

        int getWidth() const;
        int getHeight() const;

        Tile getTile(int x, int y) const;
        Q_INVOKABLE Tile getTileOrDefault(int x, int y) const;

        QList<Entity*> getEntities() const;
        void addEntity(Entity* entity);
        void removeEntity(Entity* entity);

        /// @returns entities in radius around entity
        std::vector<creature::Creature*> findCreaturesInRange(Entity* source, qreal radius);
        /// @param radius maximum distance from source
        /// @param angle "width" of source's vision
        /// @returns entities within creatures's vision
        std::vector<creature::Creature*> findCreaturesInRange(creature::Creature* source, qreal radius, qreal angle);
        /// @returns entities within given circle
        std::vector<creature::Creature*> findCreaturesInRange(QPointF center, qreal radius);
        /// @returns entities within given pizza slice
        std::vector<creature::Creature*> findCreaturesInRange(
          QPointF center,
          qreal radius,
          qreal minAngle,
          qreal maxAngle
        );

        void teleportEntity(Entity* entity, QString location);
        void teleportEntity(Entity* entity, QPointF location);
        Location getLocation(QString location) const;
        QList<Location> getLocations() const;
        /// @brief Returns name of the first location the given point is in
        QString findPointLocation(QPointF point) const;

        void update(double delta);

        void saveState(QJsonObject& json) const;
        void loadState(Game* game, QJsonObject const& json);

        static Map* loadFromFile(game::core::Game* game, data::MapId mapId, QString const& url);
        static Map* load(game::core::Game* game, data::MapId mapId, QJsonObject const& json);

      signals:
        void entityAdded(Entity* entity);
        void entityRemoved(Entity* entity);
        void rangeChecksChanged();
    };
}  // namespace game::core

#endif  // REALMOFTEMPORALITY_MAP_H
