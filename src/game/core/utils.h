#ifndef REALMOFTEMPORALITY_GAME_UTILS_H
#define REALMOFTEMPORALITY_GAME_UTILS_H

#include <qglobal.h>

#include <QJsonArray>
#include <QJsonValue>
#include <QPoint>
#include <QPointF>
#include <QSize>
#include <QSizeF>
#include <QString>

namespace game::core {
    constexpr int signum(qreal v) {
        return (v < 0) - (v > 0);
    }

    constexpr qreal rad2deg(qreal rad) {
        return rad * (180 / M_PI);
    }

    extern QJsonArray qPairToJson(QPoint const& point);
    extern QJsonArray qPairToJson(QPointF const& point);
    extern QJsonArray qPairToJson(QSize const& size);
    extern QJsonArray qPairToJson(QSizeF const& size);

    extern QPoint jsonToPoint(QJsonValue const& json);
    extern QPointF jsonToPointF(QJsonValue const& json);

    extern QSize jsonToSize(QJsonValue const& json);
    extern QSizeF jsonToSizeF(QJsonValue const& json);

    extern QJsonArray qListToJson(QList<int> const& list);
    extern QList<int> jsonToQListInt(QJsonValue const& json);

}  // namespace game::core

#endif  // REALMOFTEMPORALITY_GAME_UTILS_H
