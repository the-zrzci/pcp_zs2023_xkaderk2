#include "Tile.h"

game::core::TileBase game::core::Tile::getBase() const {
    if (base == nullptr) {
        return {};
    }
    return {
        base->name,
        base->isPassable,
        base->isWall,
        base->texture,
        base->top,
        base->left,
    };
}

bool game::core::Tile::isEmpty() const {
    return baseName.isEmpty();
}

void game::core::Tile::saveParameters(QJsonObject& json) const {
    if (!baseName.isEmpty()) {
        json["base"] = baseName;
    }
    if (height) {
        json["height"] = height;
    }
}

game::core::Tile game::core::Tile::load(const QJsonObject& json) {
    return {
        json.value("base").toString(),
        json.value("height").toInt(),
    };
}

bool game::core::operator==(const game::core::Tile& lhs, const game::core::Tile& rhs) {
    return lhs.base == rhs.base && lhs.height == rhs.height;
}

game::core::Tile::Tile() {}

game::core::Tile::Tile(QString p_baseName) : baseName(p_baseName) {}

game::core::Tile::Tile(QString p_baseName, int p_height) : Tile(p_baseName) {
    height = p_height;
}
