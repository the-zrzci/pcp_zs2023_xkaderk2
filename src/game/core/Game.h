//
// Created by xkapp on 12.11.2023.
//

#ifndef REALMOFTEMPORALITY_GAME_H
#define REALMOFTEMPORALITY_GAME_H

#include <QElapsedTimer>
#include <QHash>
#include <QObject>
#include <QPointF>
#include <QQuickWindow>
#include <string>
#include <unordered_map>

#include "Controls.h"
#include "Location.h"
#include "Map.h"
#include "Multiplayer.h"
#include "src/data/MapId.h"
#include "src/game/Entity.h"
#include "src/game/creature/Creature.h"
#include "src/game/quests/QuestManager.h"

namespace game::core {
    class Game : public QObject {
        Q_OBJECT
        Q_PROPERTY(game::quests::QuestManager* quests READ getQuestManager CONSTANT);
        Q_PROPERTY(Map* currentMap READ getCurrentMap NOTIFY mapChanged);
        Q_PROPERTY(game::creature::Creature* player READ getPlayer NOTIFY playerChanged);
        Q_PROPERTY(Controls controls MEMBER m_controls);
        Q_PROPERTY(bool paused READ isPaused WRITE setPaused NOTIFY pausedChanged);
        Q_PROPERTY(float fps MEMBER m_fps NOTIFY fpsUpdated);

        game::quests::QuestManager* m_questManager;
        Multiplayer* m_multiplayer = nullptr;

        QHash<QString, Location> m_locations;
        std::unordered_map<data::MapId, Map*> m_maps;
        game::creature::Creature* m_player = nullptr;

        Controls m_controls;
        QString m_lastLocation;
        bool m_paused = true;

        int m_timer;
        QElapsedTimer m_deltaTimer;
        qint64 m_delta;
        float m_fps = -1;

        void update();
        void timerEvent(QTimerEvent* event) override;

        void initializeMultiplayer();

      public:
        Game();
        void setMaps(std::initializer_list<Map*> maps);
        void setPlayer(game::creature::Creature* player);
        ~Game();

        game::quests::QuestManager* getQuestManager() const;

        bool isPaused() const;
        void setPaused(bool paused);

        void setEntityMap(game::Entity* entity, data::MapId mapId);

        Q_INVOKABLE void saveGame() const;
        Q_INVOKABLE void loadGame();

        void saveState(QJsonObject& json) const;
        void saveStateToFile(QString url) const;
        void loadState(const QJsonObject& json);
        void loadStateFromFile(QString url);

        void start();
        void stop();

        void changeMap(data::MapId mapId, QString location);
        void changeMap(data::MapId mapId, QPointF location);
        Map* getCurrentMap() const;
        Map* getMap(data::MapId mapId) const;
        std::unordered_map<data::MapId, Map*> const& getMaps() const;
        game::creature::Creature* getPlayer() const;

        Q_INVOKABLE QString executeCommand(QString command);

      signals:
        void pausedChanged(bool paused);
        void fpsUpdated(float fps);
        void mapChanged();
        void playerTeleported();
        void playerChanged();

        void playerEnteredLocation(QString name);
        void playerLeftLocation(QString name);
        void playerInteractedWith(Entity* entity, Entity::Interaction interaction);  // Emitted by the entity

        void playerWon();

        void creatureKilled(
          game::creature::Creature* victim,
          game::creature::Creature* killer
        );  // Emitted by the killed entity
    };
}  // namespace game::core

#endif  // REALMOFTEMPORALITY_GAME_H
