#include "Map.h"

#include <QFile>
#include <QJsonDocument>
#include <vector>

#include "src/data/entities.h"
#include "src/data/tiles.h"
#include "utils.h"

game::core::Map::Map(
  data::MapId mapId,
  QString defaultTile,
  std::vector<std::vector<Tile>> tiles,
  std::vector<Entity*> entities,
  std::vector<Location> locations
)
  : m_mapId(mapId),
    m_defaultTile(defaultTile),
    m_tiles(tiles) {
    buildShadows();

    for (auto& location : locations) {
        m_locations[location.name] = location;
    }

    // All types
    for (auto type : {Entity::Type::Creature, Entity::Type::Object, Entity::Type::Sensor}) {
        m_entities[type] = std::unordered_set<Entity*>();
    }

    for (auto entity : entities) addEntity(entity);
}

data::MapId game::core::Map::getId() const {
    return m_mapId;
}

QList<game::core::RangeCheckLog> game::core::Map::getRangeChecks() const {
    return m_lastRangeChecks;
}

void game::core::Map::buildShadows() {
    using game::core::Tile;

    for (int y = 0; y < m_tiles.size(); y++) {
        for (int x = 0; x < m_tiles[y].size(); x++) {
            Tile* tile = &m_tiles.at(y).at(x);

            for (float distance = 0.5; distance < getWidth(); distance += 0.5) {
                int distanceCeil = ceil(distance);
                int distanceFloor = floor(distance);

                if (x - distanceFloor < 0 || y - distanceFloor < 0) break;

                // Check right or directly diagonal tile
                Tile rightTile = getTileOrDefault(x - distanceFloor, y - distanceCeil);
                if (rightTile.height >= distance + tile->height) {
                    tile->rightShadowed = distance;

                    // When directly diagonal, left half is shadowed too
                    if (distanceCeil == distanceFloor) tile->leftShadowed = distance;
                }

                // Check left tile if in between tiles
                if (!tile->leftShadowed && distanceCeil != distanceFloor) {
                    Tile leftTile = getTileOrDefault(x - distanceCeil, y - distanceFloor);
                    if (leftTile.height >= distance + tile->height) tile->leftShadowed = distance;
                }

                if (tile->rightShadowed && tile->leftShadowed) break;
            }
        }
    }
}

void game::core::Map::updateShadowed(game::Entity* entity) const {
    QPointF position = entity->getPosition();
    int intX = static_cast<int>(position.x());
    int intY = static_cast<int>(position.y());
    try {
        Tile tile = getTile(intX, intY);

        qreal subtileX = position.x() - intX;
        qreal subtileY = position.y() - intY;

        if (subtileX > subtileY) {
            entity->setShadowed(tile.rightShadowed);
        } else {
            entity->setShadowed(tile.leftShadowed);
        }
    } catch (std::out_of_range) {
        // ignore if player stands in the void
    }
}

void game::core::Map::removeEntityNow(game::Entity* entity) {
    m_entities.at(entity->getType()).erase(entity);

    // Tell the entity it were removed
    entity->onDelete();
}

QPointF game::core::Map::resolveCollision(QRectF dynamicBBox, QRectF staticBBox) {
    QRectF intersection = staticBBox.intersected(dynamicBBox);

    if (intersection.isEmpty()) return {};

    qreal ratio = staticBBox.width() / staticBBox.height();
    QPointF deltaPosition = staticBBox.center() - intersection.center();

    if (std::abs(deltaPosition.x() / ratio) > std::abs(deltaPosition.y() * ratio)) {
        // is probably horizontal
        return {intersection.width() * game::core::signum(deltaPosition.x()), 0};
    }
    // is probably vertical
    return {0, intersection.height() * game::core::signum(deltaPosition.y())};
}

// Source: https://stackoverflow.com/a/402010
bool game::core::Map::collidesWithCircle(QRectF bbox, QPointF center, qreal radius) {
    QPointF position = bbox.center();

    QPointF distance{
      std::abs(center.x() - position.x()),
      std::abs(center.y() - position.y()),
    };

    // Too far away
    if (distance.x() > (bbox.width() / 2 + radius)) {
        return false;
    }
    if (distance.y() > (bbox.height() / 2 + radius)) {
        return false;
    }

    // Too close
    if (distance.x() <= (bbox.width() / 2)) {
        return true;
    }
    if (distance.y() <= (bbox.height() / 2)) {
        return true;
    }

    // Might collide?
    qreal cornerDistance_sq = pow(distance.x() - bbox.width() / 2, 2) + pow(distance.y() - bbox.height() / 2, 2);

    return (cornerDistance_sq <= pow(radius, 2));
}

QPointF game::core::Map::collideWithTiles(QRectF entityBBox) const {
    QPointF offset;

    for (qreal x = floor(entityBBox.left()); x < ceil(entityBBox.right()); x++) {
        for (qreal y = floor(entityBBox.top()); y < ceil(entityBBox.bottom()); y++) {
            int tileX = static_cast<int>(x);
            int tileY = static_cast<int>(y);

            Tile tile;
            try {
                tile = getTile(tileX, tileY);
            } catch (std::out_of_range) {
                continue;  // entity stands in the void
            }
            if (tile.base == nullptr || tile.base->isPassable) continue;

            QRectF tileBBox(tileX, tileY, 1, 1);

            offset += resolveCollision(entityBBox, tileBBox);
        }
    }

    return offset;
}

QPointF game::core::Map::collideWithEntities(QRectF bbox, bool onlyObjects, std::vector<game::Entity*>& collided)
  const {
    QPointF offset;

    auto process = [&](game::Entity::Type type) {
        for (auto* entity : m_entities.at(type)) {
            // Skip non-collidable entities
            if (!entity->isCollidable()) continue;

            QRectF staticBBox = entity->getBoundingBox();
            QPointF result = resolveCollision(bbox, staticBBox);

            if (result.isNull()) continue;

            collided.push_back(entity);

            offset += result;
        }
    };

    process(game::Entity::Object);
    if (!onlyObjects) process(game::Entity::Creature);

    return offset;
}

int game::core::Map::getWidth() const {
    return m_tiles.at(0).size();
}

int game::core::Map::getHeight() const {
    return m_tiles.size();
}

game::core::Tile game::core::Map::getTile(int x, int y) const {
    return m_tiles.at(y).at(x);
}

game::core::Tile game::core::Map::getTileOrDefault(int x, int y) const {
    try {
        game::core::Tile tile = getTile(x, y);
        if (!tile.baseName.isNull()) return tile;
    } catch (std::out_of_range) {
        // pass
    }
    return data::getTile(m_defaultTile);
}

QList<game::Entity*> game::core::Map::getEntities() const {
    QList<Entity*> list;

    for (auto pair : m_entities) {
        list.append(QList(pair.second.begin(), pair.second.end()));
    }

    return list;
}

void game::core::Map::addEntity(Entity* entity) {
    m_entities.at(entity->getType()).insert(entity);
    entity->setParent(this);

    // Update entity shadowed
    updateShadowed(entity);

    emit entityAdded(entity);
}

void game::core::Map::removeEntity(Entity* entity) {
    if (m_isUpdating) {
        // Schedule delete next update to not break anything
        m_removeEntities.push_back(entity);
    } else {
        // otherwise delete immediately
        removeEntityNow(entity);
    }
    emit entityRemoved(entity);
}

std::vector<game::creature::Creature*> game::core::Map::findCreaturesInRange(Entity* source, qreal radius) {
    return findCreaturesInRange(source->getPosition(), radius);
}

std::vector<game::creature::Creature*> game::core::Map::findCreaturesInRange(
  creature::Creature* source,
  qreal radius,
  qreal angle
) {
    using game::creature::Creature;

    qreal direction = 0;  // 0 = to the right

    switch (source->getOrientation()) {
        case Creature::Orientation::Up:
            direction = 180 + 90;
            break;
        case Creature::Orientation::Left:
            direction = 180;
            break;
        case Creature::Orientation::Down:
            direction = 90;
            break;
    }

    return findCreaturesInRange(  //
      source->getPosition(),
      radius,
      direction - angle * 0.5,
      direction + angle * 0.5
    );
}

std::vector<game::creature::Creature*> game::core::Map::findCreaturesInRange(QPointF center, qreal radius) {
    return findCreaturesInRange(center, radius, 0, 0);
}

std::vector<game::creature::Creature*> game::core::Map::findCreaturesInRange(
  QPointF center,
  qreal radius,
  qreal minAngle,
  qreal maxAngle
) {
    std::vector<game::creature::Creature*> found;

    const bool checkAngle = minAngle != maxAngle;
    const qreal direction = maxAngle - minAngle;

    for (auto* entity : m_entities.at(Entity::Type::Creature)) {
        QPointF position = entity->getPosition();
        if (center == position) continue;  // ignore source (could crash angle calculation)

        if (!collidesWithCircle(entity->getBoundingBox(), center, radius)) {
            continue;  // too far
        }

        if (checkAngle) {
            QPointF delta = position - center;
            delta /= sqrt(pow(delta.x(), 2) + pow(delta.y(), 2));
            qreal angle = game::core::rad2deg(atan2(delta.y(), delta.x()));
            // Source: https://stackoverflow.com/a/66834497
            if (fmod(360 + angle - minAngle, 360) > fmod(360 + maxAngle - minAngle, 360)) {
                continue;  // outside frustum
            }
        }

        game::creature::Creature* creature = qobject_cast<game::creature::Creature*>(entity);
        if (creature != nullptr) found.push_back(creature);
    }

#ifdef QT_DEBUG
    m_lastRangeChecks.push_back({center, radius, minAngle, maxAngle});
    while (m_lastRangeChecks.size() > 3) m_lastRangeChecks.pop_front();

    emit rangeChecksChanged();
#endif

    return found;
}

void game::core::Map::teleportEntity(Entity* entity, QString location) {
    if (!m_locations.contains(location)) throw std::out_of_range("Location not found");
    auto loc = m_locations[location];
    teleportEntity(entity, QPointF(loc.x() + loc.width() * 0.5, loc.y() + loc.height() * 0.5));
}

void game::core::Map::teleportEntity(Entity* entity, QPointF location) {
    entity->forcePosition(location);
    updateShadowed(entity);
}

game::core::Location game::core::Map::getLocation(QString location) const {
    return m_locations[location];
}

QList<game::core::Location> game::core::Map::getLocations() const {
    return QList(m_locations.begin(), m_locations.end());
}

QString game::core::Map::findPointLocation(QPointF point) const {
    for (auto location : m_locations) {
        if (location.contains(point)) return location.name;
    }
    return {};
}

void game::core::Map::update(double delta) {
    m_isUpdating = true;

    // Remove old entities
    if (m_removeEntities.size() > 0) {
        for (auto* entity : m_removeEntities) removeEntityNow(entity);

        // Reset the todo list
        m_removeEntities.clear();
    }

    // Update objects
    for (auto* entity : m_entities.at(Entity::Type::Object)) entity->update(delta);

    std::vector<game::Entity*> collisionSubjects;

    // Update creatures and sensors
    for (auto type : {Entity::Type::Creature, Entity::Type::Sensor}) {
        bool onlyObjects = type == game::Entity::Type::Creature;

        for (auto* entity : m_entities.at(type)) {
            entity->update(delta);

            // Don't check ghosts and objects for collisions
            if (!entity->isCollidable()) continue;

            QPointF lastPosition = entity->getLastPosition();
            if (lastPosition.x() < 0) continue;
            QPointF newPosition = entity->getPosition();
            QPointF movement = lastPosition - newPosition;

            QRectF bbox = entity->getBoundingBox();

            // Split the movement into steps to not miss any collision
            qreal maxMovement = (bbox.width() + bbox.height()) * 0.5;
            int steps = movement.isNull() ? 1 : ceil(movement.manhattanLength() / maxMovement);

            QRectF sweptBBox = bbox.translated(movement);  // bbox before this frame
            QPointF movementStep = movement / steps;
            QPointF response;

            for (int step = 0; step < steps; step++) {
                sweptBBox.translate(-movementStep);

                // Collide with tiles
                response += collideWithTiles(sweptBBox);

                // Collide with static entities
                response += collideWithEntities(sweptBBox, onlyObjects, collisionSubjects);

                // If we found the collision, stop
                if (!response.isNull()) {
                    // Move creature to the Time-of-Impact and resolve the collision
                    newPosition = sweptBBox.center() + response;

                    // Set new position
                    if (onlyObjects) entity->setPosition(newPosition, false);
                    entity->onCollision(collisionSubjects);

                    break;
                }
            }

            // Update shadows
            if (!movement.isNull()) updateShadowed(entity);

            collisionSubjects.clear();
        }
    }

    m_isUpdating = false;
}

void game::core::Map::saveState(QJsonObject& json) const {
    QJsonArray entities;

    for (auto* entity : getEntities()) {
        QJsonObject entityStateJson;

        entity->saveState(entityStateJson);
        QJsonObject entityJson{
          {"name", entity->getName()},
          {"state", entityStateJson},
        };
        entities.push_back(entityJson);
    }

    json["entities"] = entities;
}

void game::core::Map::loadState(Game* game, QJsonObject const& json) {
    QJsonArray entities = json["entities"].toArray();

    for (auto entityJsonRaw : entities) {
        auto entityJson = entityJsonRaw.toObject();
        auto entity = data::getEntity(game, entityJson.value("name").toString());
        entity->loadState(entityJson.value("state").toObject());
    }
}

game::core::Map* game::core::Map::loadFromFile(game::core::Game* game, data::MapId mapId, QString const& url) {
    QFile file(url);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(
          "Failed to open map file " + url.toStdString() + ": " + file.errorString().toStdString()
        );
    }

    auto json = QJsonDocument::fromJson(file.readAll());
    if (json.isNull()) {
        throw std::runtime_error("Failed to load map " + url.toStdString());
    }

    return load(game, mapId, json.object());
}

game::core::Map* game::core::Map::load(game::core::Game* game, data::MapId mapId, QJsonObject const& json) {
    QString defaultTile(json.value("defaultTile").toString());

    // Load tiles
    QJsonArray jsonTiles = json.value("tiles").toArray();
    std::vector<std::vector<game::core::Tile>> tiles;
    tiles.reserve(jsonTiles.size());

    for (int y = 0; y < jsonTiles.size(); y++) {
        auto jsonRow = jsonTiles[y].toArray();
        std::vector<game::core::Tile> row;
        row.reserve(jsonRow.size());

        for (int x = 0; x < jsonRow.size(); x++) {
            auto tile = game::core::Tile::load(jsonRow[x].toObject());
            row.push_back(data::getTile(tile));
        }

        tiles.push_back(row);
    }

    // Load entities
    QJsonArray jsonEntities = json.value("entities").toArray();
    std::vector<game::Entity*> entities;

    for (auto jsonEntityRaw : jsonEntities) {
        QJsonObject jsonEntity = jsonEntityRaw.toObject();

        game::Entity* entity = data::getEntity(game, jsonEntity.value("name").toString());
        entity->setPosition(game::core::jsonToPointF(jsonEntity.value("position")));
        if (jsonEntity.contains("nickname")) entity->setNickname(jsonEntity.value("nickname").toString());

        entities.push_back(entity);
    }

    // Load locations
    QJsonArray jsonLocations = json.value("locations").toArray();
    std::vector<game::core::Location> locations;

    for (auto jsonLocationRaw : jsonLocations) {
        locations.push_back(game::core::Location::load(mapId, jsonLocationRaw.toObject()));
    }

    return new game::core::Map(mapId, defaultTile, tiles, entities, locations);
}
