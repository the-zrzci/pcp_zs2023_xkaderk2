#include "Decoration.h"

game::Decoration::Decoration(core::Game* game, const EntityParams& params) : game::Entity(game, params) {
    setCollidable(false);
}
