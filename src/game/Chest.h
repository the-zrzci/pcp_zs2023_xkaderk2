//
// Created by Matěj on 14.11.2023.
//

#ifndef REALMOFTEMPORALITY_CHEST_H
#define REALMOFTEMPORALITY_CHEST_H

#include <vector>

#include "Entity.h"
#include "Inventory.h"

namespace game {
    class Chest : public game::Entity {
        Q_OBJECT
        
        QString m_name;
        game::Inventory* m_inventory;

      public:
        Chest(core::Game* game, game::EntityParams entityParams, QString m_name);
        Chest(core::Game* game, game::EntityParams entityParams, game::Inventory* inventory, QString m_name);
        Chest(core::Game* game, game::EntityParams entityParams, QVector<game::items::Item*> items, QString m_name);
        Chest(
          core::Game* game,
          game::EntityParams entityParams,
          QVector<game::items::Item*> items,
          int capacity,
          int width
        );
        game::Inventory* getInventory();
    };
}  // namespace game

#endif  // REALMOFTEMPORALITY_CHEST_H
