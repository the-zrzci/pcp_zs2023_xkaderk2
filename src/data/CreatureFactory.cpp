//
// Created by xkapp on 19.11.2023.
//
#include "CreatureFactory.h"

#include <QVector>
#include <random>

#include "constants.h"
#include "game/creature/abilities/basicAttacks/Hit.h"
#include "game/creature/abilities/paladin/HolyLight.h"
#include "game/creature/abilities/warrior/Slam.h"
#include "src/game/creature/AIController.h"
#include "src/game/creature/Creature.h"
#include "src/game/creature/abilities/basicAttacks/Hit.h"
#include "src/game/creature/roles/Mage.h"
#include "src/game/creature/roles/Paladin.h"
#include "src/game/creature/roles/Warrior.h"
#include "src/game/items/Weapon.h"

game::creature::Creature* data::createSkeleton(
  game::core::Game* game,
  QString nickname,
  int level,
  int money,
  game::Inventory* loot
) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<float> dis(0.0f, 1.0f);
    float random = dis(gen);
    int result = 0;

    QVector<game::creature::abilities::Ability*> abilities = {new game::creature::abilities::Hit(game)};
    game::items::Weapon* weapon = nullptr;

    game::items::Item* head = new game::items::Armor(
      game,
      *new game::EntityParams({"Head", QSizeF(1, 1), data::ASSET_PREFIX + "items/headtest.png"}),
      "popis hlavy xd",
      1,
      {1, 1, 1, 1},
      50,
      game::items::ItemType::Armor,
      {1, 1, 1, 1},
      game::items::ArmorType::Head
    );

    static const game::SpriteSheet SKELETON_LOW_LVL_VAR_1_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_low_lvl_1.png",
      {64, 64},
      {21, 3},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_LOW_LVL_VAR_2_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_low_lvl_2.png",
      {64, 64},
      {21, 3},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_LOW_LVL_VAR_3_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_low_lvl_3.png",
      {64, 64},
      {21, 3},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_MED_LVL_VAR_1_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_med_lvl_1.png",
      {64, 64},
      {21, 3},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_MED_LVL_VAR_2_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_med_lvl_2.png",
      {96, 96},
      {36, 20},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_HIGH_LVL_VAR_1_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_high_lvl_1.png",
      {96, 96},
      {36, 16},
      {6, 9, 7, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    static const game::SpriteSheet SKELETON_HIGH_LVL_VAR_2_SPRITE = {
      data::ASSET_PREFIX + "creatures/skeleton_high_lvl_2.png",
      {64, 64},
      {21, 3},
      {6, 9, 8, 6},
      {5, 6, 6, 6},
      .innerHeight = 46,
    };

    if (level <= 3) {
        result = 1 + floor(random * 3);

    } else if (level <= 6) {
        result = 1 + floor(std::pow(random, 0.5) * 5);

    } else if (level <= 10) {
        result = 1 + floor(std::pow(random, 0.5) * 7);
    }

    game::SpriteSheet spriteSheetToBeUsed = {};

    switch (result) {
        case 1:
            spriteSheetToBeUsed = SKELETON_LOW_LVL_VAR_1_SPRITE;
            break;
        case 2:
            spriteSheetToBeUsed = SKELETON_LOW_LVL_VAR_2_SPRITE;
            break;
        case 3:
            spriteSheetToBeUsed = SKELETON_LOW_LVL_VAR_3_SPRITE;
            break;
        case 4:
            spriteSheetToBeUsed = SKELETON_MED_LVL_VAR_1_SPRITE;
            break;
        case 5:
            spriteSheetToBeUsed = SKELETON_MED_LVL_VAR_2_SPRITE;
            break;
        case 6:
            spriteSheetToBeUsed = SKELETON_HIGH_LVL_VAR_1_SPRITE;
            break;
        case 7:
            spriteSheetToBeUsed = SKELETON_HIGH_LVL_VAR_2_SPRITE;
            break;
    }

    auto skeleton = new game::creature::Warrior(
      game,
      game::EntityParams{
        "Skeleton",
        {0.6, 0.3},
        spriteSheetToBeUsed,
        nickname,
      },
      new game::creature::Energy{100, game::creature::EnergyType::Rage, 100, "Rage"},
      new game::Inventory(15, 5),
      false,
      100
    );

    // todo set loot, levelup the creature
    game::creature::AIController* skeletonAI = new game::creature::AIController(
      {5, true, 10, {1, 1}, {{1, {game::creature::AIController::Action::UseAbility, "1"}}}},
      2,
      10
    );

    skeleton->setAIController(skeletonAI);

    skeleton->setLevel(level);
    skeleton->getInventory()->addItem(head);
    skeleton->setXpReward(data::BASIC_MOB_XP_BY_LVL.at(level - 1));
    skeleton->modifyCreature(data::SKELETON_HP_BY_LVL.at(level - 1), abilities, weapon, {});

    return skeleton;
}

game::creature::Creature* data::createSpirit(
  game::core::Game* game,
  QString nickname,
  int level,
  int money,
  game::Inventory* loot
) {
    auto spirit = new game::creature::Warrior(
      game,
      {"Spirit",
       {0.6, 0.3},
       {
         data::ASSET_PREFIX + "creatures/spirit.png",
         .frameSize = {64, 64},
         .hitboxOffset = {20, 2},
         .stateFrameCount = {6, 9, 8, 6},
         .stateFrameFrequency = {5, 7, 6, 6},
         .innerHeight = 46,
       },
       nickname},
      new game::creature::Energy{100, game::creature::EnergyType::Rage, 100, "Rage"},
      new game::Inventory(15, 5),
      false,
      100
    );

    spirit->setLevel(level);
    game::creature::AIController* spiritAI = new game::creature::AIController(
      {5, true, 10, {1, 1}, {{1, {game::creature::AIController::Action::UseAbility, "1"}}}},
      2,
      10
    );

    QVector<game::creature::abilities::Ability*> abilities = {new game::creature::abilities::Hit(game)};
    game::items::Weapon* weapon = nullptr;

    spirit->setAIController(spiritAI);
    spirit->setXpReward(data::BASIC_MOB_XP_BY_LVL.at(level - 1));
    spirit->modifyCreature(data::SPIRIT_HP_BY_LVL.at(level - 1), abilities, weapon, {});

    return spirit;
};

game::creature::Creature* data::createBoss(game::core::Game* game, QString nickname) {
    auto boss = new game::creature::Warrior(
      game,
      {"Boss",
       {1.2, 0.6},
       {
         data::ASSET_PREFIX + "creatures/boss.png",
         .frameSize = {192, 192},
         .hitboxOffset = {85, 67},
         .stateFrameCount = {6, 8, 8, 7},
         .stateFrameFrequency = {5, 7, 6, 6},
         .innerHeight = 46,
       },
       nickname},
      new game::creature::Energy{5000, game::creature::EnergyType::Mana, 5000, "Mana"},
      new game::Inventory(15, 5),
      false,
      100
    );

    boss->setLevel(9);
    QVector<game::creature::abilities::Ability*> abilities = {
      new game::creature::abilities::Hit(game),
      new game::creature::abilities::Slam(game),
      new game::creature::abilities::HolyLight(game)};
    game::creature::AIController* bossAI = new game::creature::AIController(
      {7,
       true,
       10,
       {1, 1, 1, 1, 2, 3},
       {{1, {game::creature::AIController::Action::UseAbility, "1"}},
        {2, {game::creature::AIController::Action::UseAbility, "2"}},
        {3, {game::creature::AIController::Action::UseAbility, "3"}}}},
      3,
      10
    );

    boss->setAbilities(abilities);
    boss->setAIController(bossAI);
    boss->setXpReward(data::CHAMPION_MOB_XP_BY_LVL.at(9));

    return boss;
}
