#ifndef REALMOFTEMPORALITY_QUESTS_H
#define REALMOFTEMPORALITY_QUESTS_H

#include <QString>
#include <array>
#include <map>

#include "MapId.h"
#include "constants.h"
#include "src/game/quests/Quest.h"
#include "src/game/quests/QuestType.h"
#include "src/game/quests/questTypes/AssassinationQuest.h"
#include "src/game/quests/questTypes/InteractionQuest.h"
#include "src/game/quests/questTypes/ItemQuest.h"
#include "src/game/quests/questTypes/LocationQuest.h"
#include "utils.h"

namespace data {
    enum class QuestId {
        TUTORIAL_1,
        TUTORIAL_2,
        TUTORIAL_3,
        SIDE_EDDIE_1,
        FOREST_EXIT_1,
        FOREST_EXIT_2,
        MEET_ISIL,
        ISIL_QUEST,
        ISIL_RESCUE,
        ISIL_RESCUE_2,
        STOP_FIRES,
        ISIL_WOOD,
        GOING_TO_CAVE,
        DEFEAT_BOSS,
        //
        __MAX_QUEST_ID,
    };

    // The Quest Array scales according to the QuestId enum
    using QuestArray = std::array<const game::quests::QuestParams *, static_cast<std::size_t>(QuestId::__MAX_QUEST_ID)>;

    namespace {
        using game::Entity;
        using game::quests::AssassinationQuestParams;
        using game::quests::InteractionQuestParams;
        using game::quests::ItemQuestParams;
        using game::quests::QuestParams;
        using game::quests::QuestType;

        const QuestArray QUEST_ARRAY{{
          new QuestParams({
            QuestId::TUTORIAL_1,
            "Wakey wakey",
            "Look around, find some place.\nUse WASD to move.",
            QuestType::Location,
            .followup = QuestId::TUTORIAL_2,
            .map = data::MapId::Forest,
            .location = "spawn exit",
          }),

          new InteractionQuestParams({
            {
              QuestId::TUTORIAL_2,
              "New game, who dis?",
              "Go checkout that guy over there.\nClick on him to talk.",
              QuestType::Interaction,
              .map = data::MapId::Forest,
              .location = "Eddie",
              .entity = "Eddie",
            },
            Entity::Dialogue,
          }),

          new AssassinationQuestParams({
            {
              QuestId::TUTORIAL_3,
              "Monsters nearby",
              .description = "Help Eddie sleep at night by defeating the monster behind his house.\n"
                             "Use numbers from your action bar to use abilities.",
              QuestType::Assassination,
              .rewards = {"Small Heal Potion"},
              .dialogue =
                "Hello stranger!\nYou must be a traveler, I have never seen you around here!\n\n"
                "You look strong! Would you mind helping me out?\n"
                "There is an evil skeleton behind my house that keeps me up at night, could you defeat it for me?\n"
                "I will give you a small heal potion in return.",
              .map = data::MapId::Forest,
              .location = "Eddie's skeleton",
              .entity = "Skeleton",
            },
            .targetNickname = "Eddie's skeleton",
            .mapTied = true,
            .checkPrevious = true,
          }),

          new ItemQuestParams({
            {
              QuestId::SIDE_EDDIE_1,
              "Eddie's just lazy",
              "Collect some wood for Eddie.\nPick them up by clicking on them.",
              QuestType::Item,
              .prerequisites = {QuestId::TUTORIAL_3},
              .rewards = {"Small Mana Potion"},
              .dialogue =
                "I need some more help.\n"
                "I am literally paralyzed, developers of this game disabled all my logic and I can't do ANYTHING!\n"
                "I need some firewood, could you collect 3 logs and bring them to me, please?",
              .map = data::MapId::Forest,
              .location = "Eddie's wood",
              .entity = "Eddie",
              .count = 3,
            },
            .dropOffLocation = "Eddie",
            .item = "Log",
          }),

          new AssassinationQuestParams({
            {
              QuestId::FOREST_EXIT_1,
              "Proving yourself",
              .description = "Kill 3 more skeletons on your way to the Northern Wall.",
              QuestType::Assassination,
              .prerequisites = {QuestId::TUTORIAL_3},
              .followup = QuestId::FOREST_EXIT_2,
              .dialogue =
                "Wow! You're really good, you should go talk to Isil, the Ranger-General of the Northern Wall.\n"
                "You have to defeat the skeletons on the road there, though.\nGood luck!",
              .map = data::MapId::Forest,
              .location = "Forest trio",
              .entity = "Skeleton",
              .count = 3,
            },
            .targetNickname = "",
            .mapTied = true,
            .checkPrevious = true,
          }),

          new QuestParams({
            QuestId::FOREST_EXIT_2,
            "Leaving the Forest",
            "Go through the mountain pass.",
            QuestType::Location,
            .followup = QuestId::MEET_ISIL,
            .map = data::MapId::Forest,
            .location = "forest_to_NW",
          }),

          new InteractionQuestParams({
            {
              QuestId::MEET_ISIL,
              "Let's goo",
              "Find Isil or whatever the name was.",
              QuestType::Interaction,
              .rewards = {"Small Heal Potion"},
              .map = data::MapId::NorthernWall,
              .location = "Isil",
              .entity = "Isil",
            },
            Entity::Dialogue,
          }),

          new AssassinationQuestParams({
            {
              QuestId::ISIL_QUEST,
              "Protect the remains of Northern Wall",
              .description = "Kill 6 skeletons around Northern Wall.",
              QuestType::Assassination,
              .prerequisites = {QuestId::TUTORIAL_3},
              .rewards = {"Enchanted slipper", "Park", "Ripped trousers", "Old man's shoes"},
              .dialogue =
                "Hello there! I'm glad to see such a strong being that is not trying to kill me.\n"
                "I need your help, the Northern Wall is under attack!\n"
                "Please help me defeating some of the skeletons, I will reward you with everything I have left!",
              .map = data::MapId::NorthernWall,
              .location = "Northern Wall",
              .entity = "Skeleton",
              .count = 6,
            },
            .mapTied = true,
            .checkPrevious = false,
          }),

          new InteractionQuestParams({
            {
              QuestId::ISIL_RESCUE,
              "She has a boyfriend...",
              "Go find Isil's man, maybe he's alive.",
              QuestType::Interaction,
              .prerequisites = {QuestId::ISIL_QUEST},
              .rewards = {"Warrior's epaulets"},
              .dialogue = "Thank you so much for the help!\n"
                          "Have you seen my boyfriend? He has to be there somewhere!\n"
                          "He should've been back by now!",
              .map = data::MapId::NorthernWall,
              .entity = "Thranduil",
            },
            Entity::Dialogue,
          }),

          new ItemQuestParams({
            {
              QuestId::ISIL_RESCUE_2,
              "Give the guy a potion",
              "Find Thranduil's potion and give it to him.",
              QuestType::Item,
              .prerequisites = {QuestId::ISIL_RESCUE},
              .dialogue = "H-Hello there! Are you here to finish me off?\n"
                          "Isil sent you? That's so sweet of her, thank you for finding me.\n"
                          "I was fighting these skeletons, but they got me good...\n"
                          "That skeleton over there stole my healing potion, if you bring it to me "
                          "I'll be on my way back to Isil in no time!",
              .map = data::MapId::NorthernWall,
              .location = "Stolen potion",
              .entity = "Isil",
            },
            .dropOffLocation = "Thranduil",
            .item = "Small Heal Potion",
          }),

          new InteractionQuestParams({
            {
              QuestId::STOP_FIRES,
              "Firemanning",
              "Click on fires around Northern Wall to put them out.",
              QuestType::Interaction,
              .prerequisites = {QuestId::ISIL_QUEST},
              .dialogue = "Why is everything burning?? Please, put out the fire!",
              .map = data::MapId::NorthernWall,
              .location = "Northern Wall",
              .entity = "Fire",
              .count = 6,
            },
            Entity::Remove,
          }),

          new ItemQuestParams({
            {
              QuestId::ISIL_WOOD,
              "Is everyone lazy here?",
              "Collect some wood for Isil.",
              QuestType::Item,
              .prerequisites = {QuestId::ISIL_QUEST},
              .rewards = {"Small Mana Potion"},
              .dialogue = "I need wood for fire and rebuilding defense, can you help?",
              .map = data::MapId::NorthernWall,
              .entity = "Isil",
              .count = 4,
            },
            .dropOffLocation = "Isil",
            .item = "Log",
          }),

          new QuestParams({
            QuestId::GOING_TO_CAVE,
            "The source of evil",
            "Go to the dark cave.",
            QuestType::Location,
            .prerequisites = {QuestId::ISIL_QUEST},
            .followup = QuestId::DEFEAT_BOSS,
            .map = data::MapId::NorthernWall,
            .location = "to_cave1",
          }),

          new AssassinationQuestParams({
            {
              QuestId::DEFEAT_BOSS,
              "Defeat Evil",
              .description = "Find and defeat the boss of this cave.",
              QuestType::Assassination,
              .prerequisites = {QuestId::GOING_TO_CAVE},
              .map = data::MapId::SmallCave,
              .entity = "Boss",
            },
          }),
        }};
    }  // namespace

    static const QuestArray QUESTS =
      sortAndReturn(QUEST_ARRAY, [](QuestParams const *a, QuestParams const *b) { return a->id < b->id; });

    extern const QuestParams *getQuestParams(QuestId id);
}  // namespace data

#endif  // REALMOFTEMPORALITY_QUESTS_H
