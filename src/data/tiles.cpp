#include "tiles.h"

game::core::Tile data::getTile(game::core::Tile tile) {
    if (!tile.baseName.isEmpty()) {
        tile.base = &BASE_TILES.at(tile.baseName);
        if (tile.base->isWall) {
            tile.height = std::max(1, tile.height);
        } else {
            tile.height = 0;
        }
    }
    return tile;
}

game::core::Tile data::getTile(QString baseName) {
    return getTile(game::core::Tile{baseName});
}
