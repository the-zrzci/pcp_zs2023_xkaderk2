#ifndef REALMOFTEMPORALITY_ENTITIES_H
#define REALMOFTEMPORALITY_ENTITIES_H

#include <QString>
#include <array>
#include <map>
#include <set>

#include "constants.h"
#include "entities/enemies.h"
#include "entities/friends.h"
#include "entities/items.h"
#include "entities/objects.h"
#include "src/game/Entity.h"
#include "src/game/items/Item.h"
#include "utils.h"

namespace data {
    namespace {
        const auto ENTITY_ARRAY =
          concatenate(data::OBJECT_ARRAY, data::ITEM_ARRAY, data::ENEMY_ARRAY, data::FRIEND_ARRAY);
    }  // namespace

    static const auto ENTITY_KEYS = generateNameKeys(ENTITY_ARRAY);
    static const auto ENTITIES = generateNameMap(ENTITY_ARRAY);

    // Categories
    static const auto OBJECT_KEYS = generateNameKeys(OBJECT_ARRAY);
    static const auto ITEM_KEYS = generateNameKeys(ITEM_ARRAY);
    static const auto ENEMY_KEYS = generateNameKeys(ENEMY_ARRAY);
    static const auto FRIEND_KEYS = generateNameKeys(FRIEND_ARRAY);

    extern game::Entity* getEntity(game::core::Game* game, QString name);

    template <class T>
    constexpr T getEntityAs(game::core::Game* game, QString name) {
        auto* entity = getEntity(game, name);
        auto* casted = qobject_cast<T>(entity);
        if (casted == nullptr) {
            // Not the requested type
            delete entity;
            return nullptr;
        }

        return casted;
    }

}  // namespace data

#endif  // REALMOFTEMPORALITY_ENTITIES_H
