//
// Created by xkapp on 19.11.2023.
//

#ifndef REALMOFTEMPORALITY_CREATUREFACTORY_H
#define REALMOFTEMPORALITY_CREATUREFACTORY_H
#include <QString>

#include "src/game/Inventory.h"
#include "src/game/creature/Creature.h"
#include "src/game/creature/abilities/Ability.h"

namespace data {
    // extern game::creature::Creature* createHog(int level, int money, game::Inventory* loot);
    // extern game::creature::Creature* createUndeadHog(int level, int money, game::Inventory* loot);
    extern game::creature::Creature* createSkeleton(
      game::core::Game* game,
      QString nickname,
      int level,
      int money,
      game::Inventory* loot
    );
    extern game::creature::Creature* createSpirit(
      game::core::Game* game,
      QString nickname,
      int level,
      int money,
      game::Inventory* loot
    );
    extern game::creature::Creature* createCorruptedSpirit(
      game::core::Game* game,
      QString nickname,
      int level,
      int money,
      game::Inventory* loot
    );
    extern game::creature::Creature* createBoss(game::core::Game* game, QString nickname);

}  // namespace data

#endif  // REALMOFTEMPORALITY_CREATUREFACTORY_H
