#ifndef REALMOFTEMPORALITY_DATA_UTILS_H
#define REALMOFTEMPORALITY_DATA_UTILS_H

#include <QString>
#include <array>
#include <map>

namespace data {
    template <class T, std::size_t n, typename Compare>
    constexpr auto sortAndReturn(std::array<T, n> list, Compare compare) {
        std::sort(list.begin(), list.end(), compare);
        return list;
    }

    template <class T, std::size_t n>
    constexpr auto generateNameMap(std::array<T, n> const &list) {
        std::map<QString, T> map;

        for (T const &item : list) {
            map[item.name] = item;
        }

        return map;
    };

    template <class T, std::size_t n>
    constexpr auto generateNameKeys(std::array<T, n> const &list) {
        return generateNameKeys(list, [](T const &item) { return item.name; });
    };

    template <class T>
    constexpr auto generateNameKeys(std::map<QString, T> const &map) {
        std::array<QString, map.size()> keys;

        for (int i = 0; i < map.size(); i++) {
            keys[i] = getKey(map[i].first);
        }

        return keys;
    };

    template <class T, std::size_t n, typename GetKey>
    constexpr auto generateNameKeys(std::array<T, n> const &list, GetKey getKey) {
        std::array<QString, n> keys;

        for (int i = 0; i < list.size(); i++) {
            keys[i] = getKey(list[i]);
        }

        return keys;
    };

    // Source: https://stackoverflow.com/a/42774523
    template <typename Type, std::size_t... sizes>
    constexpr auto concatenate(const std::array<Type, sizes> &...arrays) {
        std::array<Type, (sizes + ...)> result;
        std::size_t index{};

        ((std::copy_n(arrays.begin(), sizes, result.begin() + index), index += sizes), ...);

        return result;
    }
}  // namespace data

#endif  // REALMOFTEMPORALITY_DATA_UTILS_H
