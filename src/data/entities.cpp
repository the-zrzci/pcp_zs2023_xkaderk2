#include "entities.h"

#include "src/game/core/Game.h"

game::Entity* data::getEntity(game::core::Game* game, QString name) {
    const game::EntityTemplate* entityTemplate = &ENTITIES.at(name);
    auto entity = entityTemplate->create(game, *entityTemplate);
    entity->setParent(game);
    return entity;
}
