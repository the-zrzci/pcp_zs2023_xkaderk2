#ifndef REALMOFTEMPORALITY_CONSTANTS_H
#define REALMOFTEMPORALITY_CONSTANTS_H

#include <QString>

namespace data {
    static const QString ASSET_PREFIX = "rot:/assets/";
}  // namespace data

#endif  // REALMOFTEMPORALITY_CONSTANTS_H
