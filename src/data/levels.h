//
// Created by xkapp on 11.12.2023.
//

#ifndef REALMOFTEMPORALITY_LEVELS_H
#define REALMOFTEMPORALITY_LEVELS_H
#include <array>
#include <map>

#include "../game/creature/Stats.h"

namespace data {
    static const game::creature::Stats WARRIOR_LVL_UP{4, 2, 1, 1, 0};
    static const game::creature::Stats MAGE_LVL_UP{1, 1, 4, 2, 0};
    static const game::creature::Stats PALADIN_LVL_UP{1, 4, 2, 1, 0};

    static const std::array<int, 10> LEVELS_XP{{100, 200, 350, 500, 750, 850, 950, 1000, 1250, 1500 }};

    static const std::map<QString, int> BASE_MAX_HEALTH{{{"Mage", 50}, {"Warrior", 80}, {"Paladin", 80}}};
    static const std::map<QString, int> BASE_MAX_ENERGY{{{"Mage", 160}, {"Warrior", 100}, {"Paladin", 160}}};

    static const std::array<int, 10> SKELETON_HP_BY_LVL{{80,90,110,130,150,180,210,250,300,380}};
    static const std::array<int, 10> SPIRIT_HP_BY_LVL{{90,110,130,150,180,210,250,300,380,410}};

    static const std::array<int, 10> BASIC_MOB_XP_BY_LVL{{18,28,40,60,70,96,100,110,120,130}};
    static const std::array<int, 10> RARE_MOB_XP_BY_LVL{{26,34,44,60,65,75,90,104,132,160}};
    static const std::array<int, 10> CHAMPION_MOB_XP_BY_LVL{{32,48,70,110,140,180,250,320,400,480}};

    static const std::array<int, 10> BASIC_SPELL_COST_MODIFIER{{0,1,1,2,2,2,3,3,4,5}};
    static const std::array<int, 10> ADAVANCED_SPELL_COST_MODIFIER{{0,1,2,4,4,6,7,8,9,10}};
    static const std::array<int, 10> ULTIMATE_SPELL_COST_MODIFIER{{0,0,0,0,0,2,4,6,6,8}};

    static const std::array<int, 10> MAIN_QUEST_XP{{30,45,70,80,90,120,160,190,260,300}};
}  // namespace data

#endif  // REALMOFTEMPORALITY_LEVELS_H
