#include "gameFactory.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>

#include "constants.h"
#include "entities.h"
#include "src/game/core/Location.h"
#include "src/game/core/utils.h"
#include "src/game/creature/Creature.h"
#include "src/game/creature/abilities/basicAttacks/Hit.h"
#include "src/game/creature/abilities/basicAttacks/MagicCast.h"
#include "tiles.h"

game::core::Game* data::createGame(QString role, QString nickname, bool loadGame) {
    auto* game = new game::core::Game();
    game->setMaps({
      game::core::Map::loadFromFile(game, data::MapId::Forest, "rot:/assets/maps/forest_alt.json"),
      game::core::Map::loadFromFile(game, data::MapId::NorthernWall, "rot:/assets/maps/northern_wall.json"),
      game::core::Map::loadFromFile(game, data::MapId::SmallCave, "rot:/assets/maps/cave1.json"),
      game::core::Map::loadFromFile(game, data::MapId::Castle, "rot:/assets/maps/greing_castle.json"),
      game::core::Map::loadFromFile(game, data::MapId::Maze, "rot:/assets/maps/stone_maze.json"),
    });

    if (loadGame) {
        game->loadGame();
    } else {
        auto* player = data::getEntityAs<game::creature::Creature*>(game, role);
        player->setNickname(nickname);

        game->setPlayer(player);
        game->changeMap(data::MapId::Forest, "SPAWN");
        game->loadStateFromFile("rot:/assets/default_save.json");
    }

    return game;
}
