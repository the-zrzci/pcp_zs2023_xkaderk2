#ifndef REALMOFTEMPORALITY_MAP_ID_H
#define REALMOFTEMPORALITY_MAP_ID_H

namespace data {
    enum class MapId { Forest = 1, NorthernWall = 2, SmallCave = 3, Castle = 4, Maze = 5};
}

#endif  // REALMOFTEMPORALITY_MAP_ID_H
