#ifndef REALMOFTEMPORALITY_TILES_H
#define REALMOFTEMPORALITY_TILES_H

#include <QString>
#include <array>
#include <map>

#include "constants.h"
#include "src/game/core/Tile.h"
#include "utils.h"

namespace data {
    namespace {
        const std::array<game::core::TileBase, 14> BASE_TILE_ARRAY{{
          {"Grass", true, false, data::ASSET_PREFIX + "tiles/grass.png", data::ASSET_PREFIX + "tiles/grass_top.png"},
          {"Dirt Path",
           true,
           false,
           data::ASSET_PREFIX + "tiles/dirt_path.png",
           data::ASSET_PREFIX + "tiles/dirt_path_top.png"},
          {"Wall", false, true, data::ASSET_PREFIX + "tiles/wall.png"},
          {"Stone", false, true, data::ASSET_PREFIX + "tiles/stone.png"},
          {"Stone Floor", true, false, data::ASSET_PREFIX + "tiles/stone_floor.png"},
          {"Stone Brick Wall", false, true, data::ASSET_PREFIX + "tiles/brick_wall.png"},
          {"Stone Brick FLoor", true, false, data::ASSET_PREFIX + "tiles/brick_floor.png"},
          {"Invisible Wall", false, false, data::ASSET_PREFIX + "tiles/invisible_wall.png"},
          {"Water", false, false, data::ASSET_PREFIX + "tiles/water.png"},
          {"Water BLOCC", false, true, data::ASSET_PREFIX + "tiles/water.png"},
          {"Wooden Floor", true, false, data::ASSET_PREFIX + "tiles/wooden_floor.png"},
          {"Wooden Wall #1", false, true, data::ASSET_PREFIX + "tiles/wooden_wall_var1.png"},
          {"Wooden Wall #2", false, true, data::ASSET_PREFIX + "tiles/wooden_wall_var2.png"},
          {"V.O.I.D.", false, true, data::ASSET_PREFIX + "tiles/VOID.png"},
        }};
    }  // namespace

    // Name of tile that should be used if map doesn't have a default tile
    static const QString DEFAULT_DEFAULT_TILE = "Grass";

    static const auto BASE_TILE_KEYS = generateNameKeys(BASE_TILE_ARRAY);
    static const auto BASE_TILES = generateNameMap(BASE_TILE_ARRAY);

    extern game::core::Tile getTile(game::core::Tile tile);
    extern game::core::Tile getTile(QString base);

}  // namespace data

#endif  // REALMOFTEMPORALITY_TILES_H
