#include "quests.h"

const game::quests::QuestParams* data::getQuestParams(data::QuestId id) {
    return QUESTS.at(static_cast<int>(id));
}
