#ifndef REALMOFTEMPORALITY_FRIENDS_H
#define REALMOFTEMPORALITY_FRIENDS_H

#include "../CreatureFactory.h"
#include "../constants.h"
#include "../quests.h"
#include "src/game/Entity.h"
#include "src/game/creature/Civilian.h"
#include "src/game/creature/Creature.h"
#include "src/game/creature/Energy.h"
#include "src/game/creature/Stats.h"
#include "src/game/creature/abilities/Ability.h"
#include "src/game/creature/roles/Mage.h"
#include "src/game/creature/roles/Paladin.h"
#include "src/game/creature/roles/Warrior.h"

namespace data {
    const std::array<game::EntityTemplate, 6> FRIEND_ARRAY{
      {
        {
          {
            "Eddie",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/civilian_1.png",
              .frameSize = {64, 64},
              .hitboxOffset = {20, 2},
              .stateFrameCount = {6},
              .stateFrameFrequency = {5},
              .innerHeight = 46,
            },
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Civilian(
                game,
                params,
                {
                  QuestId::TUTORIAL_3,
                  QuestId::SIDE_EDDIE_1,
                  QuestId::FOREST_EXIT_1,
                }
              );
          },
        },
        {
          {
            "Isil",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/elf_wahmen.png",
              .frameSize = {64, 64},
              .hitboxOffset = {20, 2},
              .stateFrameCount = {6},
              .stateFrameFrequency = {5},
              .innerHeight = 46,
            },
            .nickname = "Ranger-General",
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Civilian(
                game,
                params,
                {
                  QuestId::ISIL_QUEST,
                  QuestId::ISIL_RESCUE,
                  QuestId::STOP_FIRES,
                  QuestId::ISIL_WOOD,
                  QuestId::GOING_TO_CAVE,
                }
              );
          },
        },
        {
          {
            "Thranduil",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/elf_male.png",
              .frameSize = {64, 64},
              .hitboxOffset = {20, 2},
              .stateFrameCount = {6},
              .stateFrameFrequency = {5},
              .innerHeight = 46,
            },
            .nickname = "Elven Alchymist",
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Civilian(game, params, {QuestId::ISIL_RESCUE_2});
          },
        },

        //
        // PLAYERS
        //
        {
          {
            "Warrior",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/warrior.png",
              .frameSize = {164, 164},
              .hitboxOffset = {70, 52},
              .stateFrameCount = {6, 9, 8, 7, 6},
              .stateFrameFrequency = {5, 7, 6, 6, 6},
              .innerHeight = 46,
            },
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Warrior(
                game,
                params,
                new game::creature::Energy{0, game::creature::EnergyType::Rage, 100, "Rage"},
                new game::Inventory(15, 5),
                true,
                20
              );
          },
        },
        {
          {
            "Mage",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/wizard.png",
              .frameSize = {64, 64},
              .hitboxOffset = {20, 2},
              .stateFrameCount = {6, 9, 8, 6},
              .stateFrameFrequency = {5, 7, 6, 6},
              .innerHeight = 46,
            },
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Mage(
                game,
                params,
                new game::creature::Energy{260, game::creature::EnergyType::Mana, 260, "Mana"},
                new game::Inventory(15, 5),
                true,
                20
              );
          },
        },
        {
          {
            "Paladin",
            {0.6, 0.3},
            {
              data::ASSET_PREFIX + "creatures/paladin.png",
              .frameSize = {128, 128},
              .hitboxOffset = {54, 32},
              .stateFrameCount = {6, 9, 6, 7},
              .stateFrameFrequency = {5, 7, 6, 6},
              .innerHeight = 46,
            },
          },
          [](game::core::Game* game, const game::EntityParams& params) {
              return new game::creature::Paladin(
                game,
                params,
                new game::creature::Energy{200, game::creature::EnergyType::Mana, 200, "Mana"},
                new game::Inventory(15, 5),
                true,
                20
              );
          },
        },
      },
    };
}

#endif  // REALMOFTEMPORALITY_FRIENDS_H
