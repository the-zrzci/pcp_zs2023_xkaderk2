#ifndef REALMOFTEMPORALITY_ENEMIES_H
#define REALMOFTEMPORALITY_ENEMIES_H

#include "../CreatureFactory.h"
#include "../constants.h"
#include "../quests.h"
#include "src/game/Entity.h"
#include "src/game/creature/Civilian.h"
#include "src/game/creature/Creature.h"
#include "src/game/creature/Energy.h"
#include "src/game/creature/Stats.h"
#include "src/game/creature/abilities/Ability.h"
#include "src/game/creature/roles/Mage.h"

namespace data {
    const std::array<game::EntityTemplate, 12> ENEMY_ARRAY{
      {{
         {
           "Skeleton lvl. 1",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_low_lvl_2.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 1, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 2",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_low_lvl_2.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 2, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 3",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_low_lvl_3.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 3, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 4",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_low_lvl_3.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 4, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 5",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_med_lvl_1.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 5, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 6",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_med_lvl_1.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 6, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 7",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_med_lvl_1.png",
             {64, 64},
             {21, 3},
             {6, 9, 8, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 7, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 8",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_high_lvl_1.png",
             {96, 96},
             {36, 16},
             {6, 9, 7, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 8, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 9",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_high_lvl_1.png",
             {96, 96},
             {36, 16},
             {6, 9, 7, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 9, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Skeleton lvl. 10",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/skeleton_high_lvl_1.png",
             {96, 96},
             {36, 16},
             {6, 9, 7, 6},
             {5, 6, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSkeleton(game, params.nickname, 10, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Spirit lvl. 1",
           {0.6, 0.3},
           {
             data::ASSET_PREFIX + "creatures/spirit.png",
             .frameSize = {64, 64},
             .hitboxOffset = {20, 2},
             .stateFrameCount = {6, 9, 8, 6},
             .stateFrameFrequency = {5, 7, 6, 6},
             .innerHeight = 46,
           },
         },
         [](game::core::Game* game, const game::EntityParams& params) {
             return createSpirit(game, params.nickname, 1, 10, new game::Inventory(15, 5));
         },
       },
       {
         {
           "Boss",
           {1.2, 0.6},
           {
             data::ASSET_PREFIX + "creatures/boss.png",
             .frameSize = {192, 192},
             .hitboxOffset = {85, 67},
             .stateFrameCount = {6, 8, 8, 7},
             .stateFrameFrequency = {5, 7, 6, 6},
             .innerHeight = 46,
           },
           "Kil'jaeden, The Deceiver",
         },
         [](game::core::Game* game, const game::EntityParams& params) { return createBoss(game, params.nickname); },
       }}};
}

#endif  // REALMOFTEMPORALITY_ENEMIES_H
