#ifndef REALMOFTEMPORALITY_ENTITIES_OBJECTS_H
#define REALMOFTEMPORALITY_ENTITIES_OBJECTS_H

#include <QString>
#include <array>

#include "../constants.h"
#include "src/game/Decoration.h"
#include "src/game/Entity.h"
#include "src/game/Fire.h"

namespace data {
    const std::array<game::EntityTemplate, 6> OBJECT_ARRAY{{
      {
        {
          "Cabin",
          {5, 3},
          {data::ASSET_PREFIX + "objects/cabin.png", {962, 851}, {70, 0}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Entity(game, params); },
      },
      {
        {
          "Tree",
          {1, 0.5},
          {data::ASSET_PREFIX + "objects/tree.png", {1024, 1024}, {370, 0}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Entity(game, params); },
      },
      {
        {
          "Big Tree",
          {1, 0.5},
          {data::ASSET_PREFIX + "objects/big_tree.png", {1024, 1024}, {420, 16}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Entity(game, params); },
      },
      {
        {
          "Bush #1",
          {0.8, 0.4},
          {data::ASSET_PREFIX + "objects/bush_1.png", {366, 259}, {95, 0}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Decoration(game, params); },
      },
      {
        {
          "Fire",
          {0.7, 0.4},
          {data::ASSET_PREFIX + "objects/flame.png", {16, 16}, {3, 0}, {32}, {16}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Fire(game, params, 1); },
      },
      {
        {
          "Toothless",
          {1.5, 2},
          {data::ASSET_PREFIX + "objects/toothless.png", {398, 304}, {140, 15}, {82}, {8}},
        },
        [](game::core::Game* game, const game::EntityParams& params) { return new game::Decoration(game, params); },
      },
    }};

}  // namespace data

#endif  // REALMOFTEMPORALITY_ENTITIES_OBJECTS_H
