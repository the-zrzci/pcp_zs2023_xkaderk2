#ifndef REALMOFTEMPORALITY_ENTITIES_ITEMS_H
#define REALMOFTEMPORALITY_ENTITIES_ITEMS_H

#include <QString>
#include <array>

#include "../constants.h"
#include "game/items/Potion.h"
#include "src/game/Entity.h"
#include "src/game/items/Item.h"

namespace data {
    const std::array<game::EntityTemplate, 22> ITEM_ARRAY{{
      {
        {
          "Blop",
          {1, 1},
          {data::ASSET_PREFIX + "items/blop.png", {216, 216}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Item(game, params, "Popis itemu je zde :D", 1, 50, game::items::ItemType::Misc);
        },
      },
      {
        {
          "Log",
          {1, 1},
          {data::ASSET_PREFIX + "items/log.png", {406, 406}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Item(game, params, "A piece of a tree", 1, 50, game::items::ItemType::Misc);
        },
      },
      {
        {
          "Rusty Sword",
          {1, 1},
          {data::ASSET_PREFIX + "items/rusty_sword.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "A a rusty sword usable for combat.",
              1,
              30,
              {3, 0, 0, 1, 0, 0, 0, 0, 0},
              game::items::ItemType::Weapon,
              {4, 0, 0, 0},
              false,
              game::items::WeaponType::Sword
            );
        },
      },
      {
        {
          "Smith's hammer",
          {1, 1},
          {data::ASSET_PREFIX + "items/smiths_hammer.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "With this hammer, the blacksmith beat his woman.",
              1,
              24,
              {1, 3, 0, 0, 0, 0, 0, 0, 0},
              game::items::ItemType::Weapon,
              {3, 0, 3, 0},
              false,
              game::items::WeaponType::Hammer
            );
        },
      },
      {
        {
          "Piece of wood",
          {1, 1},
          {data::ASSET_PREFIX + "items/piece_of_wood.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "A piece of wood broken from a tree \"with magical powers\".",
              1,
              12,
              {0, 1, 4, 0, 0, 0, 0, 0, 0},
              game::items::ItemType::Weapon,
              {1, 3, 2, 2},
              true,
              game::items::WeaponType::Staff
            );
        },
      },
      {
        {
          "Woodcutter's axe",
          {1, 1},
          {data::ASSET_PREFIX + "items/woodcutters_axe.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "An axe with which you chop down any tree",
              3,
              45,
              {5, 5, 0, 1, 0, 0, 0, 0, 0},
              game::items::ItemType::Weapon,
              {5, 1, 3, 3},
              false,
              game::items::WeaponType::Axe
            );
        },
      },
      {
        {
          "Enchanted slipper",
          {1, 1},
          {data::ASSET_PREFIX + "items/enchanted_slipper.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "The slipper of an old witch, maybe has magical powers.",
              4,
              52,
              {2, 2, 6, 0, .1, 0, 1, 1, 1},
              game::items::ItemType::Weapon,
              {3, 5, 5, 5},
              true,
              game::items::WeaponType::Axe
            );
        },
      },
      {
        {
          "Club",
          {1, 1},
          {data::ASSET_PREFIX + "items/club.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "The club of the frost troll.",
              6,
              98,
              {7, 9, 0, 0, -.2, 0, 0, 0, 0},
              game::items::ItemType::Weapon,
              {10, 3, 0, 0},
              false,
              game::items::WeaponType::Hammer
            );
        },
      },
      {
        {
          "Knight's Sword",
          {1, 1},
          {data::ASSET_PREFIX + "items/knight_sword.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "Sword of the Fallen Knight.",
              8,
              140,
              {12, 4, 0, 2, 0, 2, 0, 0, 0},
              game::items::ItemType::Weapon,
              {15, 0, 0, 0},
              false,
              game::items::WeaponType::Sword
            );
        },
      },
      {
        {
          "Magic hammer",
          {1, 1},
          {data::ASSET_PREFIX + "items/magic_hammer.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Weapon(
              game,
              params,
              "A hammer enchanted by a local magician.",
              9,
              164,
              {6, 7, 7, 0, 0, 2, 1, 0, 3},
              game::items::ItemType::Weapon,
              {7, 11, 17, 13},
              false,
              game::items::WeaponType::Hammer
            );
        },
      },
      // END OF WEAPONS
      {
        {
          "Park",
          {1, 1},
          {data::ASSET_PREFIX + "items/park.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Old parka made of sloth leather",
              1,
              {0, 2, 0, 0, 0, 1, 0, 0, 0},
              18,
              game::items::ItemType::Armor,
              {3, 1, 1, 1},
              game::items::ArmorType::Chest
            );
        },
      },
      {
        {
          "Ripped trousers",
          {1, 1},
          {data::ASSET_PREFIX + "items/ripped_trousers.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Ripped pants of a local homeless man",
              1,
              {0, 1, 0, 0, 0, 1, 0, 0, 0},
              12,
              game::items::ItemType::Armor,
              {2, 1, 1, 3},
              game::items::ArmorType::Legs
            );
        },
      },
      {
        {
          "Miner's gloves",
          {1, 1},
          {data::ASSET_PREFIX + "items/miners_gloves.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "The gloves with which the miner mined the gems",
              2,
              {3, 3, 0, 0, 0, 2, 2, 0, 0},
              34,
              game::items::ItemType::Armor,
              {4, 0, 0, 0},
              game::items::ArmorType::Gloves
            );
        },
      },
      {
        {
          "Old man's shoes",
          {1, 1},
          {data::ASSET_PREFIX + "items/o_shoes.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "The old man went halfway around the world with these shoes.",
              3,
              {0, 1, 0, 3, .2, 3, 3, 3, 3},
              42,
              game::items::ItemType::Armor,
              {2, 3, 3, 3},
              game::items::ArmorType::Feet
            );
        },
      },
      {
        {
          "Magician's hat",
          {1, 1},
          {data::ASSET_PREFIX + "items/m_hat.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "\"Wise\" magician's hat.",
              4,
              {0, 1, 3, 0, 0, 1, 5, 5, 5},
              55,
              game::items::ItemType::Armor,
              {2, 5, 5, 5},
              game::items::ArmorType::Head
            );
        },
      },
      {
        {
          "Long coat",
          {1, 1},
          {data::ASSET_PREFIX + "items/long_coat.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Used to look cool.",
              5,
              {3, 4, 0, 0, -.1, 4, 4, 4, 4},
              18,
              game::items::ItemType::Armor,
              {4, 4, 4, 4},
              game::items::ArmorType::Chest
            );
        },
      },
      {
        {
          "Apprentice's cloak",
          {1, 1},
          {data::ASSET_PREFIX + "items/a_cloak.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "The apprentice got it by mistake.",
              6,
              {1, 3, 4, 1, 0, 4, 4, 4, 4},
              18,
              game::items::ItemType::Armor,
              {2, 4, 2, 3},
              game::items::ArmorType::Back
            );
        },
      },
      {
        {
          "Warrior's epaulets",
          {1, 1},
          {data::ASSET_PREFIX + "items/w_epaulets.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Warrior's epaulets covered in blood.",
              7,
              {6, 4, 0, 0, 0, 6, 3, 4, 6},
              18,
              game::items::ItemType::Armor,
              {9, 3, 2, 4},
              game::items::ArmorType::Shoulders
            );
        },
      },
      {
        {
          "Emerald necklace",
          {1, 1},
          {data::ASSET_PREFIX + "items/emerald_necklace.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Traded with a villager.",
              8,
              {2, 3, 6, 2, 0, 1, 1, 1, 1},
              18,
              game::items::ItemType::Armor,
              {1, 4, 7, 6},
              game::items::ArmorType::Necklace
            );
        },
      },
      {
        {
          "Diamond ring",
          {1, 1},
          {data::ASSET_PREFIX + "items/d_ring.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Armor(
              game,
              params,
              "Has an outrageous value",
              9,
              {1, 5, 12, 0, 0, 1, 1, 1, 1},
              18,
              game::items::ItemType::Armor,
              {3, 13, 12, 11},
              game::items::ArmorType::Ring
            );
        },
      },
      {
        {
          "Small Heal Potion",
          {1, 1},
          {data::ASSET_PREFIX + "items/h_potion.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Potion(
              game,
              params,
              "Small Healing potion",
              1,
              67,
              game::items::ItemType::Consumable,
              true,
              30,
              game::creature::EnergyType::None

            );
        },
      },
      {
        {
          "Small Mana Potion",
          {1, 1},
          {data::ASSET_PREFIX + "items/m_potion.png", {1024, 1024}},
        },
        [](game::core::Game *game, const game::EntityParams &params) {
            return new game::items::Potion(
              game,
              params,
              "Small Mana potion",
              1,
              78,
              game::items::ItemType::Consumable,
              false,
              30,
              game::creature::EnergyType::Mana

            );
        },
      },
    }};

}  // namespace data

#endif  // REALMOFTEMPORALITY_ENTITIES_ITEMS_H
