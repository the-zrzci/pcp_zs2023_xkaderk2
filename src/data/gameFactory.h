#ifndef REALMOFTEMPORALITY_GAME_FACTORY_H
#define REALMOFTEMPORALITY_GAME_FACTORY_H

#include <QJsonObject>
#include <QUrl>

#include "src/game/core/Game.h"
#include "src/game/core/Map.h"

namespace data {
    extern game::core::Game* createGame(QString role, QString nickname, bool loadGame);
}  // namespace data

#endif  // REALMOFTEMPORALITY_GAME_FACTORY_H
