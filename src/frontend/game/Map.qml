import QtQuick
import QtQuick.Shapes
import GameEntity
import "../Global"

Item {
    id: mapRoot
    // Map
    x: rootWindow.width*0.5 - frontend.game.player.position.x * Global.tileSize
    y: rootWindow.height*0.5 - frontend.game.player.position.y * Global.tileSize

    property real playerHeight: 0.5

    property point lastPos: Qt.point(x,y)
    property point lastPosOffset: Qt.point(0, 0)
    function updatePosOffset() {
        lastPosOffset = Qt.point(
            mapRoot.x - mapRoot.lastPos.x,
            mapRoot.y - mapRoot.lastPos.y
        )
    }
    Connections {
        target: frontend.game

        function onMapChanged() {
            lastPos = Qt.point(x,y)
        }
        function onPlayerTeleported() {
            updatePosOffset()
        } 
    }


    // Reflections
    Rectangle {
        id: reflection
        // opacity: 0.5
        // z: 9999999999999999
        visible: Global.enableReflections
        layer.enabled: Global.enableReflections
        width: frontend.game.currentMap.width * Global.tileSize
        height: frontend.game.currentMap.height * Global.tileSize

        // Reflected Tiles
        MapListTiles { reflect: true }

        // Reflected Entities
        MapListEntities { reflect: true }
    }

    // Tiles
    MapListTiles {}

    // Entities
    MapListEntities {}

    // Debug range finds
    Repeater {
        model: Global.debug ? frontend.game.currentMap.lastRangeChecks : []

        Shape {
            visible: Global.debug
            id: rangeShape
            property bool full: modelData.minAngle == modelData.maxAngle
            x: (modelData.center.x - modelData.radius) * Global.tileSize
            y: (modelData.center.y - modelData.radius) * Global.tileSize
            z: y
            width: modelData.radius * 2 * Global.tileSize
            height: modelData.radius * 2 * Global.tileSize

            ShapePath {
                startX: modelData.radius * Global.tileSize
                startY: modelData.radius * Global.tileSize

                strokeColor: "blue"
                fillColor: Qt.rgba(0,0.2,1,0.2)

                PathAngleArc {
                    centerX: modelData.radius * Global.tileSize; centerY: modelData.radius * Global.tileSize
                    radiusX: modelData.radius * Global.tileSize; radiusY: modelData.radius * Global.tileSize
                    startAngle: rangeShape.full ? 0 : modelData.minAngle
                    sweepAngle: rangeShape.full ? 360 : modelData.maxAngle - modelData.minAngle
                }
                PathLine{
                    x: modelData.radius * Global.tileSize
                    y: modelData.radius * Global.tileSize
                }
            }
        }
    }
}
