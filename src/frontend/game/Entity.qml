import QtQuick
import GameEntity
import "../Global"
import "entity"

Loader {
    id: entityRoot
    property var entity
    property bool enableShaders: true
    property bool hoverable: false
    property bool hoverableJustHitbox: false
    property bool highlight: mouseArea.containsMouse
    property bool shadowed: entity.shadowed ?? false

    width: entity.hitbox.width * Global.tileSize
    height: entity.hitbox.height * Global.tileSize

    x: entity.position.x * Global.tileSize - width*0.5
    y: entity.position.y * Global.tileSize - height*0.5
    z: entity.position.y + entity.hitbox.height*0.5 - 0.01
    
    // Entity type selection
    Component { id: creature; Creature {} }
    Component { id: object; Object {} }
    Component { id: sensor; Sensor {} }

    sourceComponent: ({
        [GameEntity.Creature]: creature,
        [GameEntity.Object]: object,
        [GameEntity.Sensor]: sensor,
    }[entity.type] || object)

    onLoaded: {
    }

    // QUESTion mark
    Text {
        id: questMark
        y: (item?.sprite.y ?? 0) - fontInfo.pixelSize - (item?.enemyHP?.height ?? 0)
        anchors.horizontalCenter: parent.horizontalCenter

        color: "yellow"
        style: Text.Outline
        styleColor: "black"
        font.bold: true
        font.pointSize: 1024
        fontSizeMode: Text.Fit
        width: parent.width
        height: width * 2
        horizontalAlignment: Text.AlignHCenter

        function updateContent() {
            if (entity.type == undefined) return
            text = ("" 
                + (entity.quests?.length ? "!" : "") 
                + (frontend.game.quests.isQuestSubject(entity) ? "?" : "")
            )
        }
        Component.onCompleted: updateContent()
        Connections {
            target: frontend.game.quests
            function onActiveQuestsChanged() {
                questMark.updateContent()
            }
        }
    }

    // Debug hitbox
    Rectangle {
        visible: Global.debug
        anchors.fill: parent
        color: "transparent"
        z: 10
        opacity: 0.5
        border {
            width: 2
            color: "red"
        }
    }

    MouseArea {
        property bool hoverable: entityRoot.hoverable || !!entity.interaction 

        id: mouseArea

        property real spriteScale: (item?.sprite.height ?? 0) / entity.spriteSheet.frameSize.height 
        property bool useHeight: !hoverableJustHitbox && entity.spriteSheet.innerHeight != 0

        anchors.bottom: parent.bottom

        width: parent.width
        height: useHeight ? spriteScale * entity.spriteSheet.innerHeight : parent.height

        enabled: hoverable
        hoverEnabled: hoverable
        cursorShape: hoverable ? Qt.PointingHandCursor : undefined
        acceptedButtons: entity.interaction ? Qt.LeftButton : Qt.NoButton

        onClicked: {
            entity.interact(entity.interaction)
            switch (entity.interaction) {
                case GameEntity.OpenInventory:
                    break
                case GameEntity.Dialogue:
                    dialogue.open(entity)
                    break
            }
        }
        
    }
}
