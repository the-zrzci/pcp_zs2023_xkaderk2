import QtQuick
import "../Global"

Item {
    property var sheet
    property int spriteFrame: 0
    property int spriteState: 0
    property bool enableShaders: true
    property bool highlight: false
    property bool shadowed: false

    clip: true  // because sourceClipRect reloads image

    Image {
        id: img
        property real frameStep: sheet.frameSize.width / sourceSize.width
        property real stateStep: sheet.frameSize.height / sourceSize.height

        smooth: sheet.frameSize.width > 192 // smoothing for nonpixelart
        cache: true
        source: sheet.source

        width: parent.width / sheet.frameSize.width * sourceSize.width
        height: parent.height / sheet.frameSize.height * sourceSize.height

        x: -width * spriteFrame * frameStep
        y: -height * spriteState * stateStep
    }


    layer.enabled: enableShaders
    layer.effect: ShaderEffect {
        property real factor: (shadowed && Global.enableShadows ? Global.shadowBrightness : 1) + 0.5 * highlight
        property real alpha: 1
        vertexShader: "qrc:/shaders/default.vert.qsb"
        fragmentShader: "qrc:/shaders/Multiply.frag.qsb"
        supportsAtlasTextures: true
    }
}
