import QtQuick
import QtQuick.Shapes
import "../Global"

ShapePath {
    property int horizontal
    property int vertical
    property bool active
    property real thickness: maptile.width * 0.5 * active

    id: ao
    strokeWidth: -1
    startX: maptile.width * 0.5 + maptile.width * 0.5 * horizontal
    startY: maptile.width * 0.5 + maptile.width * 0.5 * vertical
    PathLine {
        relativeX: -ao.thickness * horizontal
        relativeY: 0
    }
    PathLine {
        relativeX: 0
        relativeY: ao.thickness * -vertical
    }
    PathLine {
        relativeX: ao.thickness * horizontal
        relativeY: 0
    }

    fillGradient: RadialGradient {
        centerX: ao.startX; centerY: ao.startY
        focalX: ao.startX; focalY: ao.startY
        centerRadius: ao.thickness * 0.9
        GradientStop { position: 1; color: "transparent" }
        GradientStop { position: 0; color: "black" }
    }
}
