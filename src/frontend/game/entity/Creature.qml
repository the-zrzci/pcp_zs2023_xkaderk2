import QtQuick
import GameEntity
import "../../Global"
import "../"
import "util.js" as Util

Item {
    property alias sprite: sprite
    property alias enemyHP: enemyHP

    anchors.fill: parent

    // make ghosts transparent
    opacity: !entity.collidable ? 0.7 : 1

    // Minecraft entity shadow
    Rectangle {
        visible: enableShaders && !entity.shadowed
        anchors.fill: parent
        anchors.bottomMargin: -0.25 * height
        radius: Math.max(width, height)
        color: Qt.rgba(0, 0, 0, 1 - Global.shadowBrightness)
    }

    SpriteSheet {
        id: sprite

        property var entityRect: Util.calculateEntityScreenRectangle(entity, Global.tileSize)
        width: entityRect.width
        height: entityRect.height
        x: entityRect.x
        y: entityRect.y

        sheet: entity.spriteSheet
        spriteFrame: entity.spriteFrame ?? 0
        spriteState: entity.spriteState ?? 0
        enableShaders: entityRoot.enableShaders
        highlight: entityRoot.highlight
        shadowed: entityRoot.shadowed
    }

    // Hit and heal particles
    Item {
        id: hitParticleRadius
        width: 70
        height: 70
        anchors.centerIn: sprite
    }

    Component {
        id: hitAndHealParticle

        Text {
            id: particle
            font.bold: true
            //style: Text.Outline
           // styleColor: "black"

            x: Math.random() * hitParticleRadius.width
            y: Math.random() * hitParticleRadius.height

            transform: Translate { id: particleTranslate }

            SequentialAnimation {
                running: true

                ParallelAnimation {
                    NumberAnimation {
                        target: particleTranslate
                        properties: "y"
                        from: 0
                        to: -Global.tileSize
                        duration: 2000
                    }
                    NumberAnimation {
                        target: particle
                        properties: "opacity"
                        from: 1
                        to: 0
                        duration: 2000
                    }
                }
                ScriptAction { script: particle.destroy(); }
            }
        }
    }
    Connections {
        target: entity

        function onCreatureSmallHealed(health) {
            hitAndHealParticle.createObject(hitParticleRadius, {
                color: "#28fc03",
                "font.pixelSize": 18,
                text: health > 0 ? "+"+health : health+""
            });
        }
        function onCreatureHealed(health) {
            hitAndHealParticle.createObject(hitParticleRadius, {
                color: "#28fc03",
                "font.pixelSize": 24,
                text: health > 0 ? "+"+health : health+""
            });
        }
        function onCreatureDamaged(health) {
            hitAndHealParticle.createObject(hitParticleRadius, {
                color: "#fc0303",
                "font.pixelSize": 22,
                text: health > 0 ? "-"+health : health+""
            });
        }

    }

    // Nametag
    Text{
        visible: entity != frontend.game.player
        text: (entity.nickname ?? "") + "\n" + entity.name + (entity.health > 0 ? " lvl. " + entity.level : "")
        color: "white"
        font.bold: true
        font.pointSize: 12
        horizontalAlignment: Text.AlignHCenter
        style: Text.Outline
        styleColor: "black"
        anchors {
            horizontalCenter: enemyHP.horizontalCenter
            bottom: enemyHP.visible ? enemyHP.top : enemyHP.bottom
        }
    }

    Rectangle {
        //HEALTH PLATE
        id: enemyHP
        visible: entity != frontend.game.player && entity.health > 0
        radius: 4
        color: Qt.rgba(0,0,0,0.75)
        width: parent.width*2
        height: parent.height/2
        
        property real spriteScale: sprite.height / entity.spriteSheet.frameSize.height 
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: spriteScale * entity.spriteSheet.innerHeight + Global.tileSize * 0.25
        }
        Rectangle{
            //green HEALTH rectangle
            width:  (entity.health / entity.maxHealth) * parent.width
            height: parent.height
            color: "green"
            radius: 4
        }
        Rectangle {
            //BORDER in solo rectangle to allow advanced configuration
            width: parent.width
            height: parent.height
            color: "transparent"
            radius: 4
            border.color: "black"
            border.width: 2
            z: 6
        } 
    }
}
