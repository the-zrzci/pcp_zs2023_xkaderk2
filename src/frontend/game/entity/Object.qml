import QtQuick
import GameEntity
import "../../Global"
import "../"
import "util.js" as Util

Item {
    property alias sprite: sprite

    anchors.fill: parent

    Seethrough {
        id: sprite

        property var sheet: entity.spriteSheet

        property var entityRect: Util.calculateEntityScreenRectangle(entity, Global.tileSize)
        width: entityRect.width
        height: entityRect.height
        x: entityRect.x
        y: entityRect.y

        enableSeethrough: enableShaders && entity.type == GameEntity.Object
        position: Qt.point(
            entity.position?.x - entity.hitbox.width*0.5 + x / Global.tileSize, 
            entity.position?.y - entity.hitbox.height*0.5 + y / Global.tileSize
        )
        target: sprite
        targetSize: Qt.size(width / Global.tileSize, height / Global.tileSize)
        hitboxHeight: entity.hitbox.height

        SpriteSheet {
            sheet: entity.spriteSheet
            spriteFrame: entity.spriteFrame ?? 0
            spriteState: entity.spriteState ?? 0

            anchors.fill: parent
            enableShaders: entityRoot.enableShaders
            highlight: entityRoot.highlight
            shadowed: entityRoot.shadowed
        }
    }
}
