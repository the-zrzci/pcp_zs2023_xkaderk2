function calculateEntityScreenRectangle(entity, scale) {
    const sheet = entity.spriteSheet;

    const aspectRatio = sheet.frameSize.height / sheet.frameSize.width;

    const entityWidth = sheet.frameSize.width - sheet.hitboxOffset.width * 2;
    const widthRatio = sheet.frameSize.width / entityWidth;

    const width = entity.hitbox.width * widthRatio * scale;
    const height = entity.hitbox.width * widthRatio * scale * aspectRatio;

    return Qt.rect(
        // x
        entity.hitbox.width * scale * 0.5 - width * 0.5,
        // y
        entity.hitbox.height * scale -
            height +
            sheet.hitboxOffset.height * (height / sheet.frameSize.height),
        //
        width,
        height
    );
}
