import QtQuick
import "../../Global"
import "../"
import "util.js" as Util

Item {
    property alias sprite: sprite

    transform: [ 
        Rotation {
            angle: entity.direction ? Math.atan2(entity.direction.y, entity.direction.x) / Math.PI * 180 : 0
            origin {
                x: width * 0.5
                y: height * 0.5
            }
        },
        Translate {
            y: entity.direction ? -0.5 * Global.tileSize : 0
        }
    ]

    SpriteSheet {
        id: sprite
        sheet: entity.spriteSheet
        spriteFrame: entity.spriteFrame ?? 0
        spriteState: entity.spriteState ?? 0

        property var entityRect: Util.calculateEntityScreenRectangle(entity, Global.tileSize)

        width: entityRect.width
        height: entityRect.height
        x: entityRect.x
        y: entityRect.y

        enableShaders: entityRoot.enableShaders
        highlight: entityRoot.highlight
        shadowed: entityRoot.shadowed
    }
}
