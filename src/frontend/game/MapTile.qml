import QtQuick
import QtQuick.Shapes
import "../Global"

Item {
    property int tileX
    property int tileY
    property var map: frontend.game.currentMap
    property var tile: map.getTileOrDefault(tileX, tileY)
    property var base: tile.getBase() 

    property var topNeighbor: Global.enableAO && map.getTileOrDefault(tileX, tileY - 1)
    property var rightNeighbor: Global.enableAO && map.getTileOrDefault(tileX + 1, tileY)
    property var bottomNeighbor: Global.enableAO && map.getTileOrDefault(tileX, tileY + 1)
    property var leftNeighbor: Global.enableAO && map.getTileOrDefault(tileX - 1, tileY)

    property var topLeftNeighbor: Global.enableAO && map.getTileOrDefault(tileX - 1, tileY - 1)
    property var topRightNeighbor: Global.enableAO && map.getTileOrDefault(tileX + 1, tileY - 1)
    property var bottomLeftNeighbor: Global.enableAO && map.getTileOrDefault(tileX - 1, tileY + 1)
    property var bottomRightNeighbor: Global.enableAO && map.getTileOrDefault(tileX + 1, tileY + 1)

    property bool renderWall: base.isWall 
    property bool renderFast: false
    property bool enableShadows: true
    property bool renderTop: true

    property var tileHeight: tile.height * renderWall

    property real angle: 0.7
    property real maxShadowHeight: 5
    id: maptile

    visible: !!base.texture

    width: Global.tileSize
    height: Global.tileSize

    Image {
        id: overTop
        visible: !!base.top && renderTop
        anchors.bottom: parent.top
        anchors.horizontalCenter: parent.center
        source: base.top
        width: parent.width
        fillMode: Image.PreserveAspectFit
        cache: true

        property bool aoLeft: Global.enableAO && tileAO.isBellowLeft || (!tileAO.isBellowTop && tileAO.isBellowTopLeft)
        property bool aoRight: Global.enableAO && tileAO.isBellowRight || (!tileAO.isBellowTop && tileAO.isBellowTopRight)

        layer.enabled: !renderFast && ((Global.enableShadows && tile.rightShadowed > 0) || aoLeft || aoRight)
        layer.effect: ShaderEffect {
            property real base: Global.enableShadows ? 1 - shadowWrapper.getShadow(tile.rightShadowed).a : 1
            property real factor: 1 - Global.shadowBrightness * (tile.rightShadowed && Global.enableShadows ? 0.9 : 0.6)
            property real topAO: 0
            property real bottomAO: 0
            property real leftAO: 0.5 * overTop.aoLeft * Global.enableAO
            property real rightAO: 0.5 * overTop.aoRight * Global.enableAO
            vertexShader: "qrc:/shaders/default.vert.qsb"
            fragmentShader: "qrc:/shaders/AO.frag.qsb"
            supportsAtlasTextures: true
        }
    }

    Seethrough {
        id: wrapper
        anchors.bottom: parent.bottom
        width: maptile.width
        height: maptile.width * (angle * tileHeight + 1)

        enableSeethrough: !renderFast && base.isWall && renderWall
        position: Qt.point(tileX, tileY - angle * tileHeight)
        target: wrapper
        targetSize: Qt.size(1, angle * tileHeight + 1)
        hitboxHeight: 1

        Image {
            visible: renderTop
            id: top
            anchors.top: parent.top
            source: base.texture
            width: parent.width
            fillMode: Image.PreserveAspectFit
        }

        Image {
            visible: !!tileHeight
            id: front

            transform: Scale { yScale: angle; origin { y: front.height } }

            anchors {
                bottom: wrapper.bottom
                left: wrapper.left
                right: wrapper.right
            }

            height: maptile.width * tileHeight
            width: wrapper.width

            source: base.texture
            fillMode: Image.Tile
            sourceSize.width: wrapper.width
            sourceSize.height: wrapper.width

            verticalAlignment: Image.AlignBottom
        }

        Shape {
            id: shadowWrapper
            visible: !renderFast && enableShadows && renderTop && base.name != "Water" && (renderWall || tile.rightShadowed || tile.leftShadowed)
            // asynchronous: true
            anchors.fill: parent

            function getShadow(distance: real) {
                return distance == 0 ? Qt.rgba(0, 0, 0, 0) : Qt.rgba(0, 0, 0, (1 - Global.shadowBrightness) * Math.min(1, 1 - distance / (maxShadowHeight+1)) ** 0.7)
                return distance == 0 ? Qt.rgba(0, 0, 0, 0) : Qt.rgba(0, 0, 0, (1 - Global.shadowBrightness) * Math.min(1, 1 - distance / (maxShadowHeight+1)) ** 0.7)
            }
 
            // Top right shadow triangle
            ShapePath {
                strokeWidth: -1
                fillColor: shadowWrapper.getShadow(tile.rightShadowed)
                startX: 0; startY: 0
                PathLine { x: maptile.width; y: maptile.width }
                PathLine { x: maptile.width; y: 0 }
            }
            // Bottom left shadow triangle
            ShapePath {
                strokeWidth: -1
                fillColor: shadowWrapper.getShadow(tile.leftShadowed)
                startX: 0; startY: 0
                PathLine { x: maptile.width; y: maptile.width }
                PathLine { x: 0; y: maptile.width }
            }

            // Frontfacing selfshadow
            ShapePath {
                strokeWidth: -1
                startX: 0; startY: maptile.width
                PathLine { x: maptile.width; y: maptile.width }
                PathLine { x: maptile.width; y: wrapper.height }
                PathLine { x: 0; y: wrapper.height }

                fillGradient: LinearGradient {
                    spread: ShapeGradient.PadSpread
                    x1: 0; y1: wrapper.height
                    x2: 0; y2: wrapper.height - maptile.width * maxShadowHeight
                    GradientStop { position: 0; color: Qt.rgba(0,0,0, 1 - Global.shadowBrightness * 0.5) }
                    GradientStop { position: 1; color: "transparent" }
                }
            }
        }

        MapTileAO {
            id: tileAO
            visible: !renderFast && Global.enableAO
            opacity: 1 - Global.shadowBrightness
        }
    }

    layer.enabled: base.name == "Water" && !renderFast
    layer.effect: ShaderEffect {
        property point tilePos: Qt.point(tileX, tileY)
        property size mapSize: Qt.size(map.width, map.height) 
        property real amplitude: maptile.width * 0.0002
        property real tileSize: maptile.width
        property real scale: 2
        property real time: 0
        property var reflectionSource: reflection
        property real reflectionAlpha: 0.3
        blending: false
        mesh: Qt.size(16,16)
        vertexShader: "qrc:/shaders/Water.vert.qsb"
        fragmentShader: Global.enableReflections ? "qrc:/shaders/Water.frag.qsb" : ""
        supportsAtlasTextures: false

        Connections {
            target: window
            function onFrameSwapped() {
                time = Date.now() * 0.002 % 1000000
            }
        }
    }
}
