import QtQuick
import QtQuick.Shapes
import "../Global"

ShapePath {
    property int horizontal
    property int vertical
    property bool clip: false
    property bool active
    property real thickness: maptile.width * 0.5 * active

    property int horizontal_: Math.abs(horizontal)
    property int vertical_: Math.abs(vertical)

    id: ao
    strokeWidth: -1
    startX: ao.thickness * horizontal_ + (horizontal < 0 ? maptile.width : 0) 
    startY: ao.thickness * vertical_ + (vertical < 0 ? maptile.width: 0) 
    PathLine {
        relativeX: -ao.thickness * 2 * horizontal_
        relativeY: -ao.thickness * 2 * vertical_
    }
    PathLine {
        relativeX: maptile.width * vertical_
        relativeY: maptile.width * horizontal_
    }
    PathLine {
        relativeX: ao.thickness * 2 * horizontal_
        relativeY: ao.thickness * 2 * vertical_
    }

    fillGradient: LinearGradient {
        x1: ao.startX - ao.thickness * horizontal_ * 2; y1: ao.startY - ao.thickness * vertical_ * 2
        x2: ao.startX; y2: ao.startY
        GradientStop { position: 0; color: "transparent" }
        GradientStop { position: 0.5; color: "black" }
        GradientStop { position: clip ? 0.5 : 0; color: "transparent" }
        GradientStop { position: 1; color: "transparent" }
    }
}
