import QtQuick
import "../Global"

Repeater {
    id: tileRow
    property bool reflect: false


    property real overscanX: 2
    property real overscanY: 4

    property int tileColumnCount: rootWindow.width / Global.tileSize + overscanX * 2
    property int tileRowCount: rootWindow.height / Global.tileSize + overscanY * 2

    property point playerPos: frontend.game.player.position
    property real screenX: playerPos.x - tileColumnCount * 0.5
    property real screenY: playerPos.y - tileRowCount * 0.5

    model: tileColumnCount

    Repeater {
        id: tileColumn
        model: tileRowCount

        property int columnI: index 
        property int offsetX: Math.floor((screenX - columnI) / tileColumnCount + 1) * tileColumnCount

        MapTile {
            id: mapTile

            property int rowI: index + overscanY

            property int offsetY: Math.floor((screenY - rowI) / tileRowCount + 1) * tileRowCount

            property int realTileX: columnI + offsetX
            property int realTileY: rowI + offsetY

            property int relativeX: realTileX - screenX
            property int relativeY: realTileY - screenY

            property var currentMap: frontend.game.currentMap

            property bool changingMap: false

            renderTop: !reflect
            renderFast: Global.renderFast || reflect
            enableShadows: Global.enableShadows

            map: frontend.game.currentMap
            tileX: realTileX
            tileY: realTileY 

            x: Global.tileSize * realTileX
            y: Global.tileSize * realTileY
            z: realTileY + tile.height * 0.01

            Component.onCompleted: updateTile()
            onRealTileXChanged: Qt.callLater(updateTile)
            onRealTileYChanged: Qt.callLater(updateTile)

            transform: [
                Scale { xScale: 1; yScale: reflect ? -1 : 1; origin {x: 0; y: mapTile.height}  },
                Scale {
                    id: mapTileScale
                    origin { x: mapTile.width/2; y: mapTile.height/2 }
                }
            ]

            function updateRelative() {
                var newX = realTileX - Math.floor(screenX)
                var newY = realTileY - Math.floor(screenY)
                var relativeOffsetX = relativeX - newX
                var relativeOffsetY = relativeY - newY
                relativeX = newX
                relativeY = newY
                
                if (changingMap) {
                    tileX -= relativeOffsetX 
                    tileY -= relativeOffsetY 
                    tile = map.getTileOrDefault(tileX, tileY)
                }
            }

            function updateTile() { 
                if (changingMap) return
                map = currentMap
                tileX = realTileX
                tileY = realTileY
                tile = map.getTileOrDefault(tileX, tileY)
            }

            SequentialAnimation {
                id: changeAnimation

                NumberAnimation { 
                    target: mapTileScale; 
                    properties: "xScale, yScale"
                    to: 1.1
                    duration: 100
                }
                ScriptAction {
                    script: {
                        changingMap = false
                        updateTile()
                    }
                }
                NumberAnimation { 
                    target: mapTileScale; 
                    properties: "xScale, yScale"
                    to: 1.0
                    duration: 100
                }
            }
            Timer {
                id: mapTileTimer
                interval: 0
                onTriggered: {
                    changeAnimation.start()
                }
            }

            Connections {
                target: frontend.game

                function onPlayerTeleported() {
                    Qt.callLater(updateRelative)

                    // Interval based on distance from center
                    mapTileTimer.interval = Math.sqrt(
                        (realTileX - playerPos.x)**2 + (realTileY - playerPos.y)**2
                    ) * 100
                    mapTileTimer.start()
                }

                function onMapChanged() {
                    updateRelative()

                    changingMap = true                    
                }
            }
        }
    }
}
