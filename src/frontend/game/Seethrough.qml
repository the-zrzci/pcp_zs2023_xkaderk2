import QtQuick
import "../Global"

Item {
    property var target: parent
    property size targetSize: Qt.size(1,1)
    property point position
    property bool enableSeethrough: false
    property real hitboxHeight: 0

    property real transparencyRadius: 4 / targetSize.width
    property real playerHeight: 0.75

    property var player: frontend.game.player
    property bool isCovering: (
        (Math.abs((position.y + targetSize.height * 0.5) - player.position.y) < targetSize.height * 0.5) &&
        (Math.abs((position.x + targetSize.width * 0.5) - player.position.x) < targetSize.width * 0.5 + transparencyRadius)
    )

    layer.enabled: enableSeethrough && isCovering
    layer.effect: ShaderEffect {
        property var player: frontend.game.player
        property point center: Qt.point(
            (player.position.x - position.x) / targetSize.width,
            (player.position.y - position.y - playerHeight) / (targetSize.height)
        )
        property var source: target
        property real minAlpha: 0.3
        property real exponent: 4

        property real ratio: targetSize.width / targetSize.height
        property real radius: (transparencyRadius / targetSize.width) * Math.max(0, Math.min(1, position.y - player.position.y + targetSize.height - hitboxHeight))
        property real resolution: targetSize.width * Global.tileSize
        vertexShader: "qrc:/shaders/default.vert.qsb"
        fragmentShader: "qrc:/shaders/Seethrough.frag.qsb"
        supportsAtlasTextures: true
    }
}
