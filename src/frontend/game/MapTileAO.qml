import QtQuick
import QtQuick.Shapes
import "../Global"

Shape {
    asynchronous: true

    property var isBellowTop: topNeighbor.height > tileHeight
    property var isBellowRight: rightNeighbor.height > tileHeight
    property var isBellowBottom: bottomNeighbor.height > tileHeight
    property var isBellowLeft: leftNeighbor.height > tileHeight

    property var isBellowTopLeft: topLeftNeighbor.height > tileHeight
    property var isBellowTopRight: topRightNeighbor.height > tileHeight
    property var isBellowBottomLeft: bottomLeftNeighbor.height > tileHeight
    property var isBellowBottomRight: bottomRightNeighbor.height > tileHeight

    z: 10

    // Top ambient occulsion
    MapTileAOEdge { 
        horizontal: 0
        vertical: 1
        active: isBellowTop
    }
    // Right ambient occulsion
    MapTileAOEdge { 
        horizontal: -1
        vertical: 0
        active: isBellowRight
    }
    // Bottom ambient occulsion
    MapTileAOEdge { 
        horizontal: 0
        vertical: -1
        active: isBellowBottom
    }
    // Left ambient occulsion
    MapTileAOEdge { 
        horizontal: 1
        vertical: 0
        active: isBellowLeft
        clip: true
    }

    // Top left ambient occulsion
    MapTileAOCorner { 
        horizontal: -1
        vertical: -1
        active: isBellowTopLeft && !isBellowTop && !isBellowLeft
    }
    // Top right ambient occulsion
    MapTileAOCorner { 
        horizontal: 1
        vertical: -1
        active: isBellowTopRight && !isBellowTop && !isBellowRight
    }
    // Bottom left ambient occulsion
    MapTileAOCorner { 
        horizontal: -1
        vertical: 1
        active: isBellowBottomLeft && !isBellowBottom && !isBellowLeft
    }
    // Bottom right ambient occulsion
    MapTileAOCorner { 
        horizontal: 1
        vertical: 1
        active: isBellowBottomRight && !isBellowBottom && !isBellowRight
    }
}
