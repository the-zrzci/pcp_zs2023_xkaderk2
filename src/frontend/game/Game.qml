import QtQuick
import QtQuick.Controls
import "../Global"
import "../"
import "ui"
import "ui/quest"

Rectangle {
    id: rootWindow
    anchors.fill: parent

    property bool lastLeft: false
    property bool inventoryVisible: false
    property bool characterVisible: false
    property bool devPromptVisible: false

    Component.onCompleted: keyboardController.forceActiveFocus()
    Connections {
        target: frontend.game
        function onPausedChanged() {
            keyboardController.forceActiveFocus()
        }
        function onPlayerWon() {
            deathScreen.visible = true
            deathScreen.won = true
            deathScreen.forceActiveFocus()
        }
    }

    Connections {
        target: frontend.game.player
        function onLevelChanged(level) {
            questBanner.show("Level up!","#f7cf1f","Congratulations, you are now level " + level + "!")
        }
        function onCreatureDied() {
            deathScreen.visible = true
            deathScreen.won = false
            deathScreen.forceActiveFocus()
        }
        function onCreatureHealed() {
            deathScreen.visible = false
            keyboardController.forceActiveFocus()
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        function getMapPos() {
            return Qt.point(
                (mouseArea.mouseX - mapRoot.x) / Global.tileSize,
                (mouseArea.mouseY - mapRoot.y) / Global.tileSize
            )
        }
    }

    Item {
        id: keyboardController
        // Keyboard handler
        focus: true

        onActiveFocusChanged: {
            if (!activeFocus) {
                // Prevent from walking when alt tabbing
                frontend.game.controls.up = false
                frontend.game.controls.down = false
                frontend.game.controls.left = false
                frontend.game.controls.right = false
                frontend.game.controls.sprint = false
            }
        }

        function onKeyUpdate(event, pressed){
            if (devPromptVisible) return // Dont process keypress when dev prompt is open

            // Figure out what walk key was pressed
            var control = {
                [Qt.Key_W]: "up",
                [Qt.Key_S]: "down",
                [Qt.Key_A]: "left",
                [Qt.Key_D]: "right",
                [Qt.Key_Shift]: "sprint",
            }[event.key];

            if (control) { 
                // If it was walk key then tell backend which
                frontend.game.controls[control] = pressed
            } else if (pressed === true) {
                // Otherwise if pressed check for other actions
                switch (event.key) {
                    case Qt.Key_B:
                        //open inventory
                        inventoryVisible = !inventoryVisible
                        break
                    case Qt.Key_C:
                        characterVisible = !characterVisible
                        break
                    case Qt.Key_F8:
                        Global.debug = !Global.debug
                        break
                    case Qt.Key_F7:
                        Global.enableAO = !Global.enableAO
                        break
                    case Qt.Key_F6:
                        Global.enableReflections = !Global.enableReflections
                        break
                    case Qt.Key_F5:
                        window.reloadQML()
                        break
                    case Qt.Key_Escape:
                        frontend.game.paused = !frontend.game.paused
                        break
                    case Qt.Key_Semicolon:
                        devPrompt.open()
                        break
                    case Qt.Key_F1:
                        questBanner.show("You have been quested!", "yellow", "Share with your friend to totally quest them.")
                        break
                    case Qt.Key_1:
                    case 43:
                        frontend.game.player.useAbility(0, mouseArea.getMapPos())
                        break
                    case Qt.Key_2:
                    case 282:
                        frontend.game.player.useAbility(1, mouseArea.getMapPos())
                        break
                    case Qt.Key_3:
                    case 352:
                        frontend.game.player.useAbility(2, mouseArea.getMapPos())
                        break
                    case Qt.Key_4:
                    case 268:
                        frontend.game.player.useAbility(3, mouseArea.getMapPos())
                        break
                    case Qt.Key_5:
                    case 344:
                        frontend.game.player.useAbility(4, mouseArea.getMapPos())
                        break
                    case Qt.Key_6:
                    case 381:
                        frontend.game.player.useAbility(5, mouseArea.getMapPos())
                        break
                    case Qt.Key_7:
                    case 221:
                        frontend.game.player.useAbility(6, mouseArea.getMapPos())
                        break
                    case Qt.Key_8:
                    case 193:
                        frontend.game.player.useAbility(7, mouseArea.getMapPos())
                        break
                    case Qt.Key_9:
                    case 205:
                        frontend.game.player.useAbility(8, mouseArea.getMapPos())
                        break
                }
            }
        }

        Keys.onPressed: (event) => onKeyUpdate(event, true)
        Keys.onReleased: (event) => onKeyUpdate(event, false)
    }

    // Game

    Map {
        id: mapRoot
        visible: true
    }


    // HUD

    ActiveQuests {}

    PickupLog {}

    Minimap {}

    HealthBar {}

    XPBar {}
    SpellBar { id: spellBar }

    // Windows

    Dialogue {
        id: dialogue
    }

    Inventory {
        visible: inventoryVisible
        itemsForRow: frontend.game.player.inventory.width
        capacity: frontend.game.player.inventory.items.length
        name: "Player"
        entity: frontend.game.player
    }
    
    Character {
        visible: characterVisible
    }

    // Overlay

    QuestBanner { id: questBanner }

    DebugText {
        id: tpsCounter
        color: "lime"
        
        Connections {
            target: frontend.game
            function onFpsUpdated() {
                tpsCounter.text = Math.round(frontend.game.fps) + " TPS"
            }
        }
    }
    DebugText {
        id: fpsCounter
        color: "lime"
        property real lastFrame: Date.now()
        y: 16

        Connections {
            target: window
            function onFrameSwapped() { 
                window.requestUpdate()
                
                var now = Date.now()
                var fps = 1000 / (now - fpsCounter.lastFrame)
                fpsCounter.lastFrame = now

                fpsCounter.text = Math.round(fps) + " FPS"
            }
        }
    }
    DebugText {
        anchors.right: parent.right
        text: "x=" + frontend.game.player.position.x.toFixed(2) + ", y=" + frontend.game.player.position.y.toFixed(2)
    }

    
    PauseMenu {
        visible: frontend.game.paused
    }

    DeathScreen { id: deathScreen; visible: false }
    
    Rectangle {
        visible: devPromptVisible
        color: Qt.rgba(0,0,0,0.3)
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
        }
        height: childrenRect.height + 8

        TextInput {
            property string lastCommand: ""
            id: devPrompt
            focus: true
            property bool running: false
            
            x: 4
            y: 4
            width: parent.width - 8

            function close() {
                devPrompt.text = ""
                devPromptVisible = false
                keyboardController.forceActiveFocus()
            }

            function open() {
                devPromptVisible = true
                devPrompt.forceActiveFocus()
            }

            Keys.onPressed: (event) => {
                switch (event.key) {
                    case Qt.Key_Escape:
                        devPrompt.close()
                        break
                    case Qt.Key_Up:
                        devPrompt.text = lastCommand
                        break
                }
            }

            onEditingFinished: {
                if (running) return
                running = true
                var text = devPrompt.text
                if (text) {
                    var result = frontend.game.executeCommand(text)
                    if (result) questBanner.show(result, "cyan")
                    lastCommand = text
                }
                devPrompt.close()
                running = false
            }
        }
    }
}
