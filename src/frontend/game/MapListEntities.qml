import QtQuick
import "../Global"

Item {
    property bool reflect: false
    property var listTarget: parent

    property bool changingMap: false
    property var objects: []

    // base functions
    function addEntity(entity) {
        var object = entityComponent.createObject(listTarget, { entity })
        objects.push(object)
    }
    function removeEntity(entity) {
        if (entity == frontend.game.player) return // dont remove player

        var index = objects.findIndex(object => object.entity == entity)
        if (index == -1) return console.warn("Couldn't remove entity", entity)

        var [object] = objects.splice(index, 1)
        object.destroy()
    }

    // mass functions
    function addAll() {
        for (var entity of frontend.game.currentMap.entities) {
            addEntity(entity)
        }
    }
    function removeAll() {
        for (var object of objects) object.destroy()
        objects.length = 0
    }

    // utility
    function refreshAll() {
        removeAll()
        addAll()
    }

    Component.onCompleted: addAll()
    Component.onDestruction: removeAll()

    Timer {
        id: entityRepeaterTimer
        interval: 500
        onTriggered: {
            refreshAll()
            changingMap = false
        }
    }

    Connections {
        target: frontend.game

        function onMapChanged() {
            changingMap = true
            entityRepeaterTimer.start()
        }
    }

    Connections {
        target: frontend.game.currentMap

        function onEntityAdded(entity) {
            addEntity(entity)
        }
        function onEntityRemoved(entity) {
            removeEntity(entity)
        }
    }

    Component { 
        id: entityComponent; 
        Entity {
            id: entityRoot

            property bool changing: changingMap && entity != frontend.game.player

            x: entity.position.x * Global.tileSize - width*0.5 - (changing ? mapRoot.lastPosOffset.x : 0)
            y: entity.position.y * Global.tileSize - height*0.5 - (changing ? mapRoot.lastPosOffset.y : 0)
            z: entity.position.y + entity.hitbox.height*0.5 - 0.01 - (changing ? mapRoot.lastPosOffset.y : 0)

            opacity: changing ? 0 : 1

            enableShaders: !Global.renderFast && !reflect
            transform: Scale { xScale: 1; yScale: reflect ? -1 : 1; origin {x: 0; y: entityRoot.height} }

            Behavior on opacity {
                NumberAnimation { target: entityRoot; property: "opacity"; duration: 300 }
            }
        } 
    }

}
