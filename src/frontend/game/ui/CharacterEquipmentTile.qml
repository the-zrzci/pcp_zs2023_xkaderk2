import QtQuick
import "../../Global"


Rectangle {
    property var armor
    property string src
    height: parent.height * 0.18
    width: parent.width * 0.18
    color: "transparent"
    border.color: Qt.rgba(0,0,0,0.75)
    border.width: 1
    MouseArea {
        anchors.fill: parent
        onDoubleClicked: {
            console.log("Double-clicked item:", item);
            frontend.game.player.useItem(armor, false)
        }
    }
    Image{
        source: armor?.spriteSheet.source ?? "../../../../assets/items/" + src
        width: parent.width - 1
        height: parent.height - 1
        
    }
}
