import QtQuick
import "../../Global"
import "../"
import "../entity/util.js" as Util

Rectangle {
    function getItemByArmorType(armorType) {
        var items = frontend.game.player.equipment

        const filteredItems = items.filter(item => item.armorType === armorType);
        if (filteredItems.length > 0) {
            // Return the first matching item
            return filteredItems[0];
        } else {
            return null;
        }
    }
    id: character
    height: rootWindow.height / 2
    width: rootWindow.width / 4
    color: Qt.rgba(0,0,0,0.35)
    x: rootWindow.width/6
    y: rootWindow.height/6
    
    TitleBar {
        id: tb
        windowName: "Character"
        windowTarget: character
    }
Item {
    width: parent.width * 0.8
    height: parent.height * 0.5
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: tb.bottom
    id: charSheet
    SpriteSheet {
        sheet: frontend.game.player.spriteSheet
        property var entityRect: Util.calculateEntityScreenRectangle(frontend.game.player, parent.width * 0.8)
        width: entityRect.width
        height: entityRect.height
        anchors.centerIn: charSheet
        z: 50
    }}
    CharacterEquipmentTile {
        id: head
        anchors.top: tb.bottom
        armor: getItemByArmorType(0)
        src: "Head"
    }
    CharacterEquipmentTile {
        id: shoulders
        anchors.top: head.bottom
        armor: getItemByArmorType(1)
        src: "Shoulders"
        
    }
    CharacterEquipmentTile {
        id: back
        anchors.top: shoulders.bottom
        armor: getItemByArmorType(2)
        src: "Back"
        
    }
    CharacterEquipmentTile {
        id: chest
        anchors.top: back.bottom
        armor: getItemByArmorType(3)
        src: "Chest"
        
    }
    
    //Second column
    CharacterEquipmentTile {
        id: ring
        anchors.top: tb.bottom
        anchors.right: parent.right
        armor: getItemByArmorType(4)
        src: "Ring"
        
    }
    CharacterEquipmentTile {
        id: legs
        anchors.top: ring.bottom
        anchors.right: parent.right
        armor: getItemByArmorType(5)
        src: "Legs"
        
    }
    CharacterEquipmentTile {
        id: feet
        anchors.top: legs.bottom
        anchors.right: parent.right
        armor: getItemByArmorType(6)
        src: "Feet"
        
    }
    CharacterEquipmentTile {
        id: necklace
        anchors.top: feet.bottom
        anchors.right: parent.right
        armor: getItemByArmorType(7)
        src: "Necklace"
        
    }
    CharacterEquipmentTile {
        id: gloves
        anchors.top: necklace.bottom
        anchors.right: parent.right
        armor: getItemByArmorType(8)
        src: "Gloves"
        
    }
    CharacterEquipmentTile {
        id: weapon
        anchors.top: chest.bottom
        anchors.left: parent.left
        armor: frontend.game.player.weapon
        src: "Weapon"
    }
    Text{
        text: "Strength: " + frontend.game.player.stats.strength +
            " Stamina: " + frontend.game.player.stats.stamina +
            " \nIntellect: " + frontend.game.player.stats.intellect +
            " Agility: " + frontend.game.player.stats.agility +
            " \nMovement : " + frontend.game.player.stats.movementSpeed + 
            " Armors:\n physical: " + frontend.game.player.stats.tempArmor +
            " Magical: " + frontend.game.player.stats.tempMagicArmor +
            " \nLight: " + frontend.game.player.stats.tempLightArmor +
            " Shadow: " + frontend.game.player.stats.tempShadowArmor
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        width: parent.width * 0.8
        wrapMode: Text.WordWrap
        color: "white"
        font.pixelSize: parent.height / 24
        
        
    }
    
}
