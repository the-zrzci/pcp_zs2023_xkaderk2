import QtQuick
import "../../Global"

Rectangle {
    id: inventory
    
    // Make sure when you're creating new Inventory Frame you Pass THESE 3 variables <3
    property int capacity //15
    property int itemsForRow //5
    property string name
    property var entity
    
    width: rootWindow.height / 16 * itemsForRow
    // In height calcuation we have to ensure it will add another lane for probably 1 item slot
    // And + 1/10 for InventoryTopBar where we can put some text
    height: rootWindow.height / 16 * Math.ceil(capacity/itemsForRow) + ((rootWindow.height / 16 * Math.ceil(capacity/itemsForRow)) / 10)

    x: rootWindow.width- width * 1.2
    y: rootWindow.height - height * 1.2

    color: Qt.rgba(0,0,0,0.15)
    visible: false

    function open() {
        inventory.visible = true;
    }

    function close() {
        inventory.visible = false;
    }
    
    function setXY(){
        inventory.x = (rootWindow.width/2) - width 
        inventory.y = (rootWindow.height/2) - height
    }
    
    //Connections {
    //    target: inventory
    //    function contentChanged() {
    //        inventory.needsRedraw = true;
    //    }
    //}
    TitleBar {
        windowName: name
        windowTarget: inventory
    }
    

    
    Rectangle{
        id: inventorySlots
        width: inventory.width
        // 1/10 was for top bar, so inventory Slots take the rest
        height: (inventory.height / 10) * 9
        color: "transparent"
        anchors.bottom: parent.bottom
        Repeater {
            model: inventory.capacity
            delegate: InventoryTile {
                id: inventoryTile
                item: entity?.inventory.items[index]

            }
        }
    }
}
