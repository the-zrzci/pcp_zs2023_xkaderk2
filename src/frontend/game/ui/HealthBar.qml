import QtQuick
import "../../Global"

Rectangle {
    id: healthBar
    width: rootWindow.width / 12
    height: rootWindow.height / 12
    property int textSize: healthBar.height/6.5
    y: rootWindow.height / 16
    x: rootWindow.width / 16
    color: "transparent"
    Rectangle {
        //NAME
        color: "black"
        id: name
        width: parent.width - parent.width / 3
        height: parent.height/3
        Text{
            text: frontend.game.player.nickname
            color: "white"
            anchors.leftMargin: 5
            anchors.left: parent.left
            font.bold: true
            font.pointSize: textSize
        }
        Text{
            text: frontend.game.player.level
            color: "white"
            anchors.right: parent.right
            anchors.rightMargin: 5
            font.bold: true
            font.pointSize: textSize
        }
    }
    Rectangle {
        //HEALTH
        color: "transparent"
        border.color: Qt.rgba(0,0,0,0.75)
        border.width: 2
        radius: 10
        id: health
        width: parent.width
        height: parent.height/3
        y: height
        Rectangle {
            width:  (frontend.game.player.health / frontend.game.player.maxHealth) * parent.width
            height: parent.height
            color: "green"
            radius: 10
            z: 5
        }
        Rectangle {
            width:  ((frontend.game.player.health / frontend.game.player.maxHealth) * parent.width) - parent.width/5
            height: parent.height
            color: "green"
            z: 4
        }
        Text{
            text: frontend.game.player.health + "/" + frontend.game.player.maxHealth
            color: "white"
            z: 10
            anchors.leftMargin: 5
            anchors.left: parent.left
            font.bold: true
            font.pointSize: textSize
        }

    }
    Rectangle {
        //ENERGY
        color: "transparent"
        border.color: Qt.rgba(0,0,0,0.75)
        border.width: 2
        radius: 10
        id: energy
        width: parent.width
        height: parent.height/3
        y: height*2
        Rectangle {
            width: (frontend.game.player.energy.energy / frontend.game.player.energy.maxEnergy) * parent.width
            height: parent.height
            color: "blue"
            radius: 10
            z: 5
        }
        Rectangle {
            width: ((frontend.game.player.energy.energy / frontend.game.player.energy.maxEnergy) * parent.width) - parent.width/5
            height: parent.height
            color: "blue"
            z: 4
        }
        Text{
            color: "white"
            text: frontend.game.player.energy.energy + "/" + frontend.game.player.energy.maxEnergy
            z: 10
            anchors.leftMargin: 5
            anchors.left: parent.left
            font.bold: true
            font.pointSize: textSize
        }
    }

}
