import QtQuick
import "../../"

MouseArea {
    property bool won: false

    anchors.fill: parent

    Rectangle {
        anchors.fill: parent
        color: Qt.rgba(0, 0, 0, 0.7)

        Column {
            anchors.centerIn: parent
            spacing: 32

            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                visible: won
                source: "rot:/assets/partypopper.png"
                width: 128
                height: 128
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                text: won ? "You won the game!" : "You died."
                font.bold: true
                font.pixelSize: 32
                color: "white"
            }

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                MenuButton {
                    buttonText: "Exit to menu"

                    MouseArea {
                        anchors.fill: parent
                        cursorShape: Qt.PointingHandCursor
                        onClicked: frontend.exitGame()
                    }
                }
            }
        }
    }
}
