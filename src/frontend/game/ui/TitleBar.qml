import QtQuick
Rectangle {
    property string windowName
    property var windowTarget
    id: titleBar
    width: parent.width
    height: rootWindow.height / 48
    anchors.top: parent.top
    color: "black"
    Text{
        text: titleBar.windowName
        anchors.left: parent.left
        anchors.top: parent.top
        color: "white"
        font.pointSize: rootWindow.height / 128
        font.bold: true
    }
    MouseArea {
        anchors.fill: parent
        drag.target: windowTarget
    }
}
