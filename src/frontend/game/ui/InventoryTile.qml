import QtQuick
import "../../Global"
Rectangle {
    id: inventoryTile
    property var item: {}
    property int minimumPSize: 4
    property int pSize: inventoryTile.height / 3.2
    width: inventorySlots.width / inventory.itemsForRow
    // Math.ceil is used to fill the whole inventorySlots area
    height: inventorySlots.height / Math.ceil(inventory.capacity/inventory.itemsForRow)
    color: Qt.rgba(0,0,0,0.45)
    border.color: Qt.rgba(0,0,0,0.75)
    border.width: 1
    Image{
        source: item?.spriteSheet.source ?? ""
        height: inventoryTile.height
        width: inventoryTile.width
    }
    MouseArea {
        id: tileMouseArea
        anchors.fill: parent
        hoverEnabled: true

        onReleased: {
            var fromIndex = index;
            var localMouse = tileMouseArea.mapToItem(inventorySlots, mouse.x, mouse.y);
            var toIndex = Math.floor(localMouse.x / (inventorySlots.width / inventory.itemsForRow)) +
                Math.floor(localMouse.y / (inventorySlots.height / Math.ceil(inventory.capacity/inventory.itemsForRow))) * inventory.itemsForRow;

            // Check if the drop position is within the valid range
            if (localMouse.x >= 0 && localMouse.x <= inventorySlots.width &&
                localMouse.y >= 0 && localMouse.y <= inventorySlots.height) {
                frontend.game.player.inventory.moveItem(fromIndex, toIndex);
            } else {
                frontend.game.player.inventory.moveItem(fromIndex, -1);
            }
        }
        onDoubleClicked: {
            console.log("Double-clicked item:", inventoryTile.item);
            frontend.game.player.useItem(inventoryTile.item, true)
        }
        
    }

    states: [
        State {
            name: "hover"
            when: tileMouseArea.containsMouse

            PropertyChanges {
                target: description
                visible: item !== null
            }
        }
    ]


    Rectangle {
        id: description
        width: inventoryTile.width * 5
        height : inventoryTile.height * 6
        y: - description.height * 1.2
        color: Qt.rgba(0.118,0.118,0.118,0.85)
        visible: false

        Rectangle {
            id: descriptionRectangle
            width: parent.width * 0.9
            height: parent.height * 0.9
            anchors.centerIn: parent
            anchors.margins: 10
            color: Qt.rgba(0,0,0,0)

            Text {
                id: textName
                text: item?.name ?? ""
                font.bold: true
                color: "white"
                font.pixelSize: 15
            }

            Text {
                id: descriptionText
                text: item?.description ?? ""
                anchors.top: textName.bottom
                width: descriptionRectangle.width
                wrapMode: Text.WordWrap
                color: "white"
                font.pixelSize: 12}
            Text {
                id: weaponTypeTxt
                text: ["Misc", "Consumable", "Armor", "Weapon"][item?.itemType] ?? "idk?"
                anchors.top: descriptionText.bottom
                font.bold: true
                color: "white"
                font.pixelSize: 15
                Column {
                    anchors.top: weaponTypeTxt.bottom 
                    
                    // Weapon damage stats
                    Text {
                        visible: item?.itemType === 3  && item?.damage.physical !== 0
                        text: "Attack Damage: " + (item?.damage?.physical || "N/A") 
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 3 && item?.damage.magical !== 0
                        text: "Magical Damage: " + (item?.damage?.magical || "N/A")
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 3 && item?.damage.light !== 0
                        text: "Light Damage: " + (item?.damage?.light || "N/A")
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 3  && item?.damage.shadow !== 0
                        text: "Shadow Damage: " + (item?.damage?.shadow || "N/A")
                        font.pixelSize: pSize  
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    // Armor protection stats
                    Text {
                        visible: item?.itemType === 2  
                        text: "Physical protection: " + (item?.armor?.physical || "N/A")  
                        font.pixelSize: pSize  
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 2  
                        text: "Magical protection: " + (item?.armor?.magical || "N/A")
                        font.pixelSize: pSize  
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 2 
                        text: "Light protection: " + (item?.armor?.light || "N/A")
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.itemType === 2 
                        text: "Shadow protection: " + (item?.armor?.shadow || "N/A")
                        font.pixelSize: pSize
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    // Display non-zero bonus stats
                    Text {
                        visible: item?.bonus?.strength !== 0
                        text: "Strength Bonus: " + item?.bonus?.strength
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.bonus?.stamina !== 0
                        text: "Stamina Bonus: " + item?.bonus?.stamina
                        font.pixelSize: pSize  
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }
                    Text {
                        visible: item?.bonus?.intellect !== 0
                        text: "Intellect Bonus: " + item?.bonus?.intellect
                        font.pixelSize: pSize
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.bonus?.agility !== 0
                        text: "Agility Bonus: " + item?.bonus?.agility
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }
                    Text {
                        visible: item?.bonus?.movementSpeed !== 0
                        text: "Movement Speed Bonus: " + item?.bonus?.movementSpeed
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.bonus?.tempArmor !== 0
                        text: "Armor Bonus: " + item?.bonus?.tempArmor
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }
                    Text {
                        visible: item?.bonus?.tempMagicArmor !== 0
                        text: "Magic Armor Bonus: " + item?.bonus?.tempMagicArmor
                        font.pixelSize: pSize
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }

                    Text {
                        visible: item?.bonus?.tempLightArmor !== 0
                        text: "Light Armor Bonus: " + item?.bonus?.tempLightArmor
                        font.pixelSize: pSize
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }
                    Text {
                        visible: item?.bonus?.tempShadowArmor !== 0
                        text: "Shadow Armor Bonus: " + item?.bonus?.tempShadowArmor
                        font.pixelSize: pSize 
                        color: "white"
                        fontSizeMode: Text.Fit;
                        minimumPixelSize: minimumPSize;
                        
                    }
                    
                }
            }
        }
    }
    
    x: (index % inventory.itemsForRow) * width
    y: Math.floor(index / inventory.itemsForRow) * height
}
