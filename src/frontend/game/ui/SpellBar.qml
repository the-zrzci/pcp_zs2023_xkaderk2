import QtQuick
import "../../Global"
Rectangle {

    id: spellBar
    width: rootWindow.width * 0.7
    height: (rootWindow.width * 0.7) / 21 // width / 8 //for 8 spells? :D
    color: Qt.rgba(0,0,0,0.15)
    anchors.bottom: parent.bottom
    anchors.horizontalCenter: parent.horizontalCenter
    Repeater {
        model: frontend.game.player.abilities.length
        delegate: SpellTile {
            spell: frontend.game.player.abilities[index]
        }
    }

    Rectangle {
        id: castingBar

        property var spell: frontend.game.player.abilities[frontend.game.player.castedAbilityIndex]
        property real progress: spell ? spell.castingTimer / spell.castingTime : 0

        opacity: 0

        states: [
            State {
                name: "visible"
                when: !!castingBar.spell
                PropertyChanges {
                    target: castingBar
                    opacity: 1
                }
            }
        ]

        transitions: [
            Transition {
                from: "visible"
                to: ""
                NumberAnimation {
                    property: "opacity"
                    duration: 800
                }
            }
        ]

        anchors.bottom: spellBar.top
        anchors.bottomMargin: 16
        anchors.horizontalCenter: spellBar.horizontalCenter

        width: spellBar.width * 0.5
        height: spellName.height + 4
        radius: 8
        color: Qt.rgba(0,0,0,0.5)

        Rectangle {
            color: Qt.rgba(1, 0.9, 0, 0.75)
            radius: 8
            x: 2
            y: 2
            height: parent.height - 4
            width: parent.width * (1 - castingBar.progress) - 4
        }

        Text {
            id: spellName
            anchors.centerIn: parent
            text: castingBar.spell?.name ?? ""
            font.bold: true
            color: "white"
        }
    }
}
