import QtQuick
import GameEntity

Column {
    id: pickupLog
    anchors.right: rootWindow.right
    anchors.top: rootWindow.verticalCenter

    move: Transition {
        NumberAnimation { property: "y"; duration: 300 }
    }

    Component {
        id: toastComponent

        Row {
            id: toast

            padding: 8
            spacing: 8

            property alias text: toastText.text
            property alias source: toastImage.source

            transform: Translate { id: toastTranslate }

            Text {
                id: toastText
                color: "white"
                style: Text.Outline
                styleColor: "black"
                font.pixelSize: 16
                anchors.verticalCenter: parent.verticalCenter
            }

            Image {
                id: toastImage
                width: 32
                height: 32
                sourceSize { width: 32; height: 32 }
                anchors.verticalCenter: parent.verticalCenter
            }

            SequentialAnimation {
                running: true

                PropertyAction { target: toastTranslate; property: "y"; value: 30 }
                PropertyAction { target: toast; property: "opacity"; value: 0 }

                ParallelAnimation {
                    NumberAnimation { target: toastTranslate; property: "y"; to: 0; duration: 500; easing.type: Easing.OutQuad }
                    NumberAnimation { target: toast; property: "opacity"; to: 1; duration: 500 }
                }

                PauseAnimation { duration: 1500 }

                NumberAnimation { target: toast; property: "opacity"; to: 0; duration: 300 }
                ScriptAction { script: toast.destroy(); }
            }
        }
    }

    Connections {
        target: frontend.game

        function onPlayerInteractedWith(entity, interaction) {
            if (interaction != GameEntity.Pickup) return
            
            toastComponent.createObject(pickupLog, {
                text: "Picked up " + entity.name,
                source: entity.spriteSheet.source,
            })
        }
    }
}
