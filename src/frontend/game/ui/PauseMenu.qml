import QtQuick
import "../../"
import "../../Global"

Rectangle {
    anchors.fill: parent
    color: Qt.rgba(0,0,0,0.5)

    Column {
        anchors.centerIn: parent
        spacing: 10

        Text {
            color: "white"
            text: "Game Paused"
        }

        MenuButton {
            buttonText: "Resume"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: frontend.game.paused = false
            }
        }

        MenuButton {
            buttonText: "Save Game"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    frontend.game.saveGame()
                    questBanner.show("Game saved.", "lime")
                    frontend.game.paused = false
                }
            }
        }

        MenuButton {
            buttonText: "Load Game"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: {
                    frontend.game.loadGame()
                    questBanner.show("Game loaded.", "lime")
                    frontend.game.paused = false
                }
            }
        }

        MenuButton {
            buttonText: "Settings"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: Global.showSettings = true
            }
        }

        MenuButton {
            buttonText: "Exit to menu"

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                onClicked: frontend.exitGame()
            }
        }
    }
}
