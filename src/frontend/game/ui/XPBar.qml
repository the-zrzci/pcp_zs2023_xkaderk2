import QtQuick
import "../../Global"

Rectangle {
    id: slot
    property int xp: frontend.game.player.expPoints
    property int maxXp: frontend.game.player.xpNeededForNextLevel
    width: rootWindow.width * 0.7
    height: rootWindow.height / 64
    anchors.bottom: spellBar.top
    anchors.horizontalCenter: parent.horizontalCenter
    color: Qt.rgba(0,0,0,0.15)

    Rectangle {
        width: slot.width * (xp/maxXp)
        height: slot.height
        color: "purple"
        z: 15
    }
    Text {
        color: "white"
        anchors.horizontalCenter: parent.horizontalCenter
        text: xp.toString() + " / " + maxXp.toString()
        font.bold: true
        z: 20
    }
}
