import QtQuick
import "../../Global"
Rectangle{
    property var spell: {}
    width: parent.height
    height: parent.height
    z: 2
    x: parent.height * index
    border.color: Qt.rgba(0,0,0,0.85)
    border.width: 1
    color: "black"
    visible: spell.requiredLevel < frontend.game.player.level && (frontend.game.player.weapon || !spell.requiresWeapon)
   //visible: frontend.game.player.getWeapon() == null

    Image{
        source: spell.icon
        height: parent.height
        width: parent.width
        z: 1
    }
    Text{
     text: index + 1
     color: "white"
     font.bold: true
     z: 2
        style: Text.Outline
        styleColor: "black"
    }

    Connections {
        target: spell

        function onFinishedCasting() {
            spell.updateTargetPosition(mouseArea.getMapPos())
        }
    }


    Rectangle {
        //visible: true
        visible: spell.cooldownTimer > 0
        anchors.fill: parent;
        color: Qt.rgba(0,0,0,0.75)
        anchors.centerIn: parent
        z: 2

        Text {
            anchors.centerIn: parent
            text: Math.ceil(spell.cooldownTimer) + ""
            font.pixelSize: 24
            color: "white"
            //font.bold: true
        }

    }

    Rectangle {
        id: description
        width: 250
        height : 250
        y: parent.y - 270
        color: Qt.rgba(0.118,0.118,0.118,0.85)
        visible: false
        z: 10

        Rectangle {
            width: parent.width - 20
            height: parent.height - 20
            anchors.centerIn: parent
            anchors.margins: 10
            color: Qt.rgba(0,0,0,0)

            Text {
                text: spell.name
                font.bold: true
                color: "white"
                font.pixelSize: 15
            }

            Text {
                id: cost
                color: "white"
                y: 17
                text: spell.energyCost + " % " + spell.energyType
                visible: spell.energyType !== "None"
                font.pixelSize: 15
            }

            Text {
                id: range
                color: "white"
                y: 17
                anchors.right: parent.right
                horizontalAlignment: Text.AlignRight
                text: "Range " + spell.range.toFixed(2) + " m"
                visible: spell.ranged || spell.range > 0
                font.pixelSize: 15
            }

            Text {
                id: casting
                color: "white"
                y: 35
                text: {
                    if (spell.castingTime === 0) {
                        return "Instant cast"
                    } else {
                        return spell.castingTime.toFixed(1) + " sec cast"
                    }
                }
                font.pixelSize: 15
            }


            Text {
                id: cooldown
                color: "white"
                z: 5
                y: 35
                anchors.right: parent.right
                horizontalAlignment: Text.AlignRight
                text: {
                    if (spell.cooldown === 0) {
                        return "No cooldown"
                    } else {
                        return spell.cooldown + " sec cooldown"
                    }
                }
                font.pixelSize: 15
            }

            Text {
                text: spell.description
                y: 60
                color: "white"
                font.pixelSize: 12
                width: 220
                wrapMode: Text.WordWrap
            }
        }


    }

    MouseArea {
        id: mouseArea1
        anchors {
            fill: parent
        }
        hoverEnabled: true

    }

    states: [
        State {
            name: "hover"
            when: mouseArea1.containsMouse

            PropertyChanges {
                target: description
                visible: true
            }
        }
    ]


}
