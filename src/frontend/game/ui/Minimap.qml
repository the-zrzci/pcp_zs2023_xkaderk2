import QtQuick
import GameEntity

Item {
    property int tileCount: 24

    anchors {
        top: parent.top
        topMargin: 10
        right: parent.right
        rightMargin: 10
    }

    id: minimap
    width: parent.width * 0.1
    height: parent.width * 0.1

    property int tileSize: width / tileCount
    property point playerPos: frontend.game.player.position

    property int playerX: Math.floor(playerPos.x)
    property int playerY: Math.floor(playerPos.y)

    property real offsetX: playerPos.x - playerX
    property real offsetY: playerPos.y - playerY

    function shouldShow(area: rect) {
        var relativePosition = Qt.point(playerPos.x - area.x, playerPos.y - area.y)
        var areaSize = Math.max(area.width, area.height)

        return (relativePosition.x**2 + relativePosition.y**2) <= ((tileCount + areaSize)*0.5)**2
    }

    function getMinimapPosition(position: point) {
        var relativePosition = Qt.point(playerPos.x - position.x, playerPos.y - position.y)
        return Qt.point(
            (tileCount*0.5 - relativePosition.x) * tileSize,
            (tileCount*0.5 - relativePosition.y) * tileSize
        )
    }

    // Minimap itself
    Item {
        anchors.fill: parent
        clip: true

        // Tiles
        Repeater {
            model: tileCount + 3

            Repeater {
                id: tileColumn
                property int columnI: index
                model: tileCount + 3

                Image {
                    property int columnI: tileColumn.columnI - 1
                    property int rowI: index - 1

                    property int tileX: columnI + playerX - Math.floor(tileCount*0.5) - 1
                    property int tileY: rowI + playerY - Math.floor(tileCount*0.5) - 1
                    property var tile: frontend.game.currentMap.getTileOrDefault(tileX, tileY)
                    property var base: tile.getBase()

                    visible: !!base.texture //&& ((tileCount*0.5 - columnI - 0.6)**2 + (tileCount*0.5 - rowI - 0.6)**2) <= (tileCount*0.5 + 1)**2
                    smooth: false
                    cache: true

                    x: tileSize * (columnI - offsetX)
                    y: tileSize * (rowI - offsetY)
                    width: tileSize
                    height: tileSize

                    source: base.texture
                    
                    sourceSize.width: 2
                    sourceSize.height: 2
                }
            }
        }

        // Quest areas
        Repeater {
            model: frontend.game.quests.active.filter(q => q.isOnThisMap && q.targetArea.width && shouldShow(q.targetArea))

            Rectangle {
                property var area: modelData.targetArea
                property point minimapPosition: getMinimapPosition(area)

                width: area.width * tileSize
                height: area.height * tileSize
                x: minimapPosition.x + tileSize
                y: minimapPosition.y + tileSize

                opacity: 0.6
                color: Qt.rgba(1, 1, 0, 0.75)
                border.color: "gold"
                border.width: 2
            }
        }

        // Entities
        Repeater {
            model: frontend.game.currentMap.entities
        
            Rectangle {
                property var entity: modelData
                
                property point minimapPosition: getMinimapPosition(entity?.position ?? Qt.point(0 ,0)) 

                visible: entity && entity != frontend.game.player && shouldShow(entity.boundingBox)
                function updateColor() {
                    if (!entity) return
                    if (entity.type == GameEntity.Object) {
                        color = "gray"
                    } else if (frontend.game.quests.isQuestSubject(entity)) {
                        color = "yellow"
                    } else if (entity.type == GameEntity.Creature) {
                        color = entity.isFriendly ? "lime" : "red"
                    } else {
                        color = "cyan"
                    }
                }
                Component.onCompleted: updateColor()
                Connections {
                    target: frontend.game.quests
                    function onActiveQuestsChanged() {
                        updateColor()
                    }
                }
                radius: entity?.type == GameEntity.Creature ? Math.max(width,height) : 0

                width: (entity?.hitbox.width ?? 0) * tileSize 
                height: entity?.type == GameEntity.Creature ? width : (entity?.hitbox.height ?? 0) * tileSize

                x: minimapPosition.x - width * 0.5 + tileSize
                y: minimapPosition.y - height * 0.5 + tileSize
            }
        }

        layer.enabled: true
        layer.effect: ShaderEffect {
            property real radius: 0.5
            vertexShader: "qrc:/shaders/default.vert.qsb"
            fragmentShader: "qrc:/shaders/Round.frag.qsb"
        }
    }

    // Border
    Rectangle {
        id: border
        anchors.fill: parent
        radius: width
        color: "transparent"
        border.color: "gold"
        border.width: 2
    }

    // Player
    Rectangle {
        id: player
        color: "gold"
        radius: width
        border.color: "white"
        anchors.centerIn: parent
        width: tileSize
        height: tileSize
    }

    // Quest arrows
    Repeater {
        model: frontend.game.quests.active.filter(q => q.isOnThisMap)

        Item {
            id: questArrow
            width: parent.width * 0.2
            height: width
            clip: true

            x: parent.width * 0.5 - width * 0.5
            y: -height * 0.25

            property point target: Qt.point(
                modelData.targetArea.x + modelData.targetArea.width * 0.5,
                modelData.targetArea.y + modelData.targetArea.height * 0.5
            )

            property real angle: Math.atan2(
                playerPos.y - target.y,
                playerPos.x - target.x
            ) / Math.PI * 180

            transform: [
                Rotation { angle: 45; origin { x: width * 0.5; y: width * 0.5 } },
                Rotation { 
                    angle: questArrow.angle - 90; 
                    origin { 
                        x: minimap.width * 0.5 - questArrow.x
                        y: minimap.height * 0.5 - questArrow.y 
                    } 
                }
            ]

            Rectangle {
                color: "gold"

                width: parent.width
                height: width

                x: -width * 0.5
                y: -height * 0.5

                transform: Rotation { angle: 45; origin { x: width * 0.5; y: width * 0.5 } }
            }
        }
    }
}
