import QtQuick
import QtQuick.Layouts

ColumnLayout {
    visible:  frontend.game.quests.active.length
    anchors {
        left: parent.left
        verticalCenter: parent.verticalCenter
    }
    spacing: 4

    Text {
        Layout.leftMargin: 8
        color: "yellow"
        text: "Quests"
        style: Text.Outline
        styleColor: "black"
        font.pointSize: 15
    }

    Repeater {
        model: frontend.game.quests.active

        Rectangle {
            property var quest: modelData
            property var questInfo: quest.info
            color: Qt.rgba(0, 0, 0, 0.3)

            width: Math.min(Math.max(rootWindow.width * 0.25, 100), 400)
            height: childrenRect.height

            Column {
                width: parent.width
                padding: 6
                spacing: 4
                
                RowLayout {
                    width: parent.width - 6 * 2

                    Text {
                        color: "yellow"
                        text: questInfo.name
                        style: Text.Outline
                        styleColor: "black"
                        font.pointSize: 11
                        wrapMode: Text.WordWrap
                        Layout.fillWidth: true
                    }
                    Text {
                        id: progress
                        visible: questInfo.count > 1
                        text: quest.progress + "/" + questInfo.count
                        style: Text.Outline
                        styleColor: "black"
                        font.pointSize: 8
                        color: quest.progress < questInfo.count ? "white" : "lime"
                    }
                }
                Text {
                    color: "white"
                    text: questInfo.description
                    style: Text.Outline
                    styleColor: "black"
                    font.pointSize: 8
                    wrapMode: Text.WordWrap
                    width: parent.width - 6
                }
            }
        }
    }
}
