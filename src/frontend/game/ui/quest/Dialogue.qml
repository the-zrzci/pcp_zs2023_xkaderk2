import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../../../"

Rectangle {
    property var entity
    property int selectedQuest: -1

    property var activeQuests: entity?.activeQuests ?? []
    property var availableQuests: entity?.quests ?? []
    property var quests: activeQuests.concat(availableQuests)
    property var rewards: selectedQuest != -1? frontend.game.quests.getRewards(quests[selectedQuest].id) : []

    function open(targetEntity) {
        entity = targetEntity

        // If only one quest is available show that one
        if (quests.length == 1) selectedQuest = 0

        visible = true
        rewardGrid.columns = dialogueColumn.width / rewardGrid.childrenRect.width
        flickable.contentHeight = Qt.binding(() => dialogueColumn.height)
    }
    function close() {
        selectedQuest = -1
        visible = false
        flickable.contentHeight = 0
        rewardGrid.columns = 99
    }


    visible: false
    anchors.centerIn: parent
    color: Qt.rgba(0, 0, 0, 0.7)
    radius: 8

    width: rootWindow.width * 0.75 + 16 * 2
    height: rootWindow.height * 0.75 + 16 * 2

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 16
        spacing: 16

        // Title
        Text {
            text: entity?.name + "'s quest" 
                + (selectedQuest == -1 ? "s:" : ": "+quests[selectedQuest].name) 
                + (selectedQuest != -1 && selectedQuest < activeQuests.length ? " (active)" : "")
            color: "yellow"
            font.pixelSize: 24
            font.bold: true
            style: Text.Outline
            styleColor: "black"
            wrapMode: Text.WordWrap
            Layout.fillWidth: true
        }

        // Scrollable part
        Flickable {
            id: flickable
            boundsBehavior: Flickable.StopAtBounds 
            Layout.fillWidth: true
            Layout.fillHeight: true 
            contentWidth: width
            contentHeight: dialogueColumn.height
            clip: true
            ScrollBar.vertical: ScrollBar { palette.mid: "white" }

            ColumnLayout {
                id: dialogueColumn
                spacing: 16
                width: parent.width

                // Quest list
                Repeater {
                    model: selectedQuest == -1 ? quests : []

                    Rectangle {
                        property var quest: modelData

                        width: parent.width
                        height: 18 + 16 * 2

                        color: mouseArea.containsMouse ? Qt.rgba(1, 1, 1, 0.2) : "transparent"
                        radius: 4

                        Text {
                            id: text
                            x: 16
                            y: 16
                            text: quest.name + (index < activeQuests.length ? " (active)" : "")
                            color: index < activeQuests.length ? "yellow" : "white"
                            font.pixelSize: 16
                            font.bold: true
                            elide: Text.ElideRight
                            width: parent.width
                            maximumLineCount: 1
                        }

                        Rectangle {
                            visible: index > 0
                            width: parent.width
                            height: 1
                            anchors.top: parent.top
                            anchors.topMargin: -8
                            color: "gray"
                        }

                        MouseArea {
                            id: mouseArea
                            anchors.fill: parent
                            cursorShape: Qt.PointingHandCursor
                            hoverEnabled: true
                            onClicked: selectedQuest = index
                        }
                    }
                }

                // Dialogue
                Text {
                    visible: quests.length == 0 || selectedQuest != -1
                    text: selectedQuest != -1 ? (quests[selectedQuest].dialogue ?? "") : entity?.name + " has nothing to say."
                    color: "white"
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                    Layout.fillHeight: true 
                }

                // Description
                Text {
                    visible: selectedQuest != -1
                    text: "Quest description"
                    color: "yellow"
                    font.pixelSize: 16
                    font.bold: true
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                }
                Text {
                    visible: selectedQuest != -1
                    text: quests[selectedQuest]?.description ?? ""
                    color: "white"
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                    Layout.fillHeight: true 
                }

                // Rewards
                Text {
                    visible: selectedQuest != -1
                    text: "Quest rewards"
                    color: "yellow"
                    font.pixelSize: 16
                    font.bold: true
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                }
                Grid {
                    id: rewardGrid
                    visible: rewards.length > 0
                    rowSpacing: 4
                    Layout.topMargin: -8
                    columnSpacing: 16
                    columns: 99
                    
                    Layout.fillWidth: true

                    Repeater {
                        model: rewards

                        Row {
                            spacing: 8

                            Image {
                                anchors.verticalCenter: parent.verticalCenter
                                source: modelData.spriteSheet.source
                                width: 48
                                height: 48
                            }

                            Text {
                                anchors.verticalCenter: parent.verticalCenter
                                font.bold: true
                                text: modelData.name
                                color: "white"
                            }
                        }
                    }
                }
                Text {
                    visible: selectedQuest != -1 && rewards.length == 0
                    text: "No reward"
                    color: "white"
                    wrapMode: Text.WordWrap
                    Layout.fillWidth: true
                    Layout.fillHeight: true 
                }
            }
        }

        // Buttons
        RowLayout {
            spacing: 16

            MenuButton {
                visible: selectedQuest != -1 && quests.length > 1
                Layout.fillWidth: true
                buttonText: "Back to list"
                isPrimary: false
                
                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: selectedQuest = -1
                }
            }

            MenuButton {
                Layout.fillWidth: true
                buttonText: "Close"
                isPrimary: false

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: close()
                }
            }
            
            MenuButton {
                visible: selectedQuest >= activeQuests.length
                Layout.fillWidth: true
                buttonText: "Accept"
                
                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        frontend.game.quests.startQuest(quests[selectedQuest].id)
                        close()
                    }
                }
            }
        }
    }
}
