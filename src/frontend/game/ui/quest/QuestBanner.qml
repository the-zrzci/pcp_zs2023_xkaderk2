import QtQuick

Rectangle {
    id: banner
    color: Qt.rgba(0, 0, 0, 0.3)
    width: 0 // animated
    height: 72
    y: parent.height * 0.05

    property var queue: []

    Connections {
        target: frontend.game.quests

        function onQuestStarted(quest) {
            banner.show("Quest Started", "yellow", quest.name)
        }

        function onQuestCompleted(quest) {
            banner.show("Quest Complete", "lime", quest.name)
        }
    }


    function show(title: string, textColor: color, subTitle: string) {
        if (animation.running) {
            queue.push([title, textColor, subTitle])
            return
        }

        text.text = title
        subText.text = subTitle || ""
        text.color = Qt.color(textColor)
        animation.start()
    }

    Text {
        id: text
        opacity: 0 // animated
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 4
        text: "Missing message :)"
        color: "yellow"
        style: Text.Outline
        styleColor: "black"
        font {
            weight: Font.DemiBold
            pixelSize: 38
        }
    }

    Text {
        id: subText
        opacity: 0 // animated
        y: 0 // animated
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 4
        text: ""
        color: "white"
        style: Text.Outline
        styleColor: "black"
        font {
            weight: Font.DemiBold
            pixelSize: 18
        }
    }

    SequentialAnimation {
        id: animation
        alwaysRunToEnd: true

        onFinished: {
            var next = queue.shift()
            if (next) banner.show(...next)
        }

        // Reset
        PropertyAction { target: banner; property: "x"; value: 0 }
        PropertyAction { target: banner; property: "width"; value: 0 }
        PropertyAction { target: text; property: "anchors.topMargin"; value: -16 }
        PropertyAction { target: text; property: "opacity"; value: 0}
        PropertyAction { target: subText; property: "opacity"; value: 0 }
        PropertyAction { target: banner; property: "visible"; value: true }

        // Show
        NumberAnimation { target: banner; property: "width"; to: parent.width; duration: 300 }
        ParallelAnimation {
            NumberAnimation { target: text; property: "opacity"; to: 1; duration: 200 }
            NumberAnimation { target: text; property: "anchors.topMargin"; to: 4; duration: 300 }
        }
        NumberAnimation { target: subText; property: "opacity"; to: 1; duration: 300 }

        // Wait
        PauseAnimation { duration: 1500 }

        // Hide
        ParallelAnimation {
            NumberAnimation { target: subText; property: "opacity"; to: 0; duration: 200 }
            NumberAnimation { target: text; property: "opacity"; to: 0; duration: 400 }
        }
        NumberAnimation { target: banner; property: "x"; to: parent.width; duration: 200 }
        PropertyAction { target: banner; property: "visible"; value: false }
    }
}
