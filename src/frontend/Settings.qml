import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "Global"

MouseArea {
    visible: Global.showSettings
    enabled: visible
    anchors.fill: parent
    hoverEnabled: true

    Rectangle {
        anchors.centerIn: parent

        width: window.width * 0.75 + 16 * 2
        height: window.height * 0.75 + 16 * 2
        color: Qt.rgba(0, 0, 0, 0.7)
        radius: 8

        ColumnLayout {
            y: 32
            spacing: 16
            anchors.horizontalCenter: parent.horizontalCenter

            Text {
                Layout.alignment: Qt.AlignHCenter
                text: "Settings"
                color: "white"
                font.bold: true
                font.pixelSize: 24
                horizontalAlignment: Text.AlignHCenter
            }

            Switch {
                checked: Global.renderFast
                text: "Simple rendering"
                onToggled: { 
                    Global.renderFast = checked
                    if (checked) {
                        Global.enableShadows = false
                        Global.enableReflections = false
                        Global.enableAO = false
                    }
                }
            }

            Switch {
                enabled: !Global.renderFast
                checked: Global.enableShadows
                text: "Enable shadows"
                onToggled: Global.enableShadows = checked
            }

            Switch {
                enabled: !Global.renderFast
                checked: Global.enableReflections
                text: "Enable reflections"
                onToggled: Global.enableReflections = checked
            }

            Switch {
                enabled: !Global.renderFast
                checked: Global.enableAO
                text: "Enable ambient occulsion"
                onToggled: Global.enableAO = checked
            }

            MenuButton {
                buttonText: "Close"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: Global.showSettings = false
                }
            }
        }


    }
}
