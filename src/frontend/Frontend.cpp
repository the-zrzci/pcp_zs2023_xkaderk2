#include "Frontend.h"

#include "src/data/entities.h"
#include "src/data/gameFactory.h"
#include "src/game/Entity.h"
#include "src/game/items/ArmorType.h"

frontend::Frontend::Frontend() {
    m_state = FrontendState::MainMenu;

    qmlRegisterType<Frontend>("Frontend", 1, 0, "FrontendState");
    qmlRegisterType<game::Entity>("GameEntity", 1, 0, "GameEntity");
    qmlRegisterType<game::items::ItemType>("ItemType", 1, 0, "ItemType");

    // Make self available to QML
    m_engine.rootContext()->setContextProperty("frontend", this);

#ifdef QT_DEBUG
    m_engine.rootContext()->setContextProperty("DEV_MODE", true);
#else
    m_engine.rootContext()->setContextProperty("DEV_MODE", false);
#endif
}

frontend::Frontend::~Frontend() {
    switch (m_state) {
        case FrontendState::Game:
            exitGame();
            break;

        case FrontendState::Editor:
            exitEditor();
            break;
    }
}

void frontend::Frontend::setState(FrontendState state) {
    m_state = state;
    emit stateChanged(state);
}

void frontend::Frontend::load() {
    assert(m_window == nullptr);

    // Load QML
    m_engine.load("rot:/src/frontend/main.qml");

    if (m_engine.rootObjects().size() != 1) {
        throw std::runtime_error("QML wasn't loaded properly.");
    }

    // Get window reference
    m_window = static_cast<QQuickWindow*>(m_engine.rootObjects().at(0));
}

void frontend::Frontend::reload() {
    m_engine.clearComponentCache();
    emit reloaded();
}

void frontend::Frontend::startGame(QString role, QString name, bool loadGame) {
    assert(m_state == FrontendState::MainMenu);

    try {
        m_game = data::createGame(role, name, loadGame);

        m_game->start();
        setState(FrontendState::Game);
    } catch (std::runtime_error e) {
        // :(
        qDebug() << e.what();
    }
}

void frontend::Frontend::exitGame() {
    assert(m_state == FrontendState::Game);

    setState(FrontendState::MainMenu);

    m_game->stop();
    delete m_game;
    m_game = nullptr;
}

void frontend::Frontend::startEditor() {
    assert(m_state == FrontendState::MainMenu);

    m_game = new game::core::Game();
    m_game->setPlayer(static_cast<game::creature::Creature*>(data::getEntity(m_game, "Mage")));
    m_game->setMaps({
      new game::core::Map(data::MapId::Forest, {}, {{{}}}, {}, {{}}),
    });
    m_game->changeMap(data::MapId::Forest, "");

    m_editor = new editor::Editor(m_game);

    setState(FrontendState::Editor);
}

void frontend::Frontend::exitEditor() {
    assert(m_state == FrontendState::Editor);

    setState(FrontendState::MainMenu);

    delete m_editor;
    m_editor = nullptr;

    delete m_game;
    m_game = nullptr;
}
