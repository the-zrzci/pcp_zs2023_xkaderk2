#ifndef REALMOFTEMPORALITY_FRONTEND_H
#define REALMOFTEMPORALITY_FRONTEND_H

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickWindow>

#include "src/editor/Editor.h"
#include "src/game/core/Game.h"

namespace frontend {
    class Frontend : public QObject {
        Q_OBJECT

        Q_PROPERTY(FrontendState state MEMBER m_state NOTIFY stateChanged);
        Q_PROPERTY(game::core::Game* game MEMBER m_game NOTIFY stateChanged);
        Q_PROPERTY(editor::Editor* editor MEMBER m_editor NOTIFY stateChanged);

      public:
        enum FrontendState { MainMenu, Game, Editor };
        Q_ENUM(FrontendState);

      private:
        FrontendState m_state;
        QQmlApplicationEngine m_engine;
        QQuickWindow* m_window = nullptr;
        game::core::Game* m_game;
        editor::Editor* m_editor;

        void setState(FrontendState state);

      public:
        Frontend();
        ~Frontend();

        void load();
        void reload();

        Q_INVOKABLE void startGame(QString role, QString name, bool loadGame);
        Q_INVOKABLE void exitGame();

        Q_INVOKABLE void startEditor();
        Q_INVOKABLE void exitEditor();

      signals:
        void stateChanged(FrontendState state) const;
        void reloaded() const;
    };
}  // namespace frontend

#endif  // REALMOFTEMPORALITY_FRONTEND_H
