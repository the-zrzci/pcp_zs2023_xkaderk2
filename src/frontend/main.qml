import QtQuick
import Frontend
import "Global"

Window {
    id: window
    width: 800
    height: 500
    title: "Realm of Temporality"
    visible: true

    function onResize() {
        var tileScale = 10
        Global.tileSize = (window.width + window.height) / 2 / tileScale
    }

    onHeightChanged: onResize()
    onWidthChanged: onResize()
    Component.onCompleted: onResize()

    Image {
        source: "rot:/assets/background.png"
        anchors.fill: parent
    }

    Loader {
        id: loader
        anchors.fill: parent
        asynchronous: true

        source: "MainMenu.qml"

        states: [
            State {
                name: ""
                PropertyChanges { target: loader; source: "MainMenu.qml" }
            },
            State {
                name: "game"
                when: frontend.state == FrontendState.Game
                PropertyChanges { target: loader; source: "game/Game.qml" }
            },
            State {
                name: "editor"
                when: DEV_MODE && frontend.state == FrontendState.Editor
                PropertyChanges { target: loader; source: "editor/Editor.qml" }
            }
        ]

        onStatusChanged: {
            loaderError.visible = false

            if (frontend.state == FrontendState.Game && loader.status == Loader.Ready) {
                // Unpause game once rendered
                frontend.game.paused = false
            } else if (loader.status == Loader.Error) {
                loaderError.visible = true
            }
        }
    }
    
    Settings { }

    Rectangle {
        id: loaderLoading
        color: Qt.rgba(0,0,0,0.5)
        anchors.fill: parent
        enabled: loader.status == Loader.Loading

        property real startTime: 0

        opacity: 0

        onStateChanged: {
            if (state == "ACTIVE") startTime = Date.now()
            else if (Date.now() - startTime < 300) {
                // Only do the animation if loading took more that 0.3 seconds
                opacity = 0
            }
        }

        states: State {
            name: "ACTIVE"
            when: loader.status == Loader.Loading
            PropertyChanges { target: loaderLoading; opacity: 1 }
        }

        transitions: [
            Transition {
                from: "ACTIVE"
                to: ""
                NumberAnimation { target: loaderLoading; properties: "opacity"; duration: 300 }
            },
            Transition {
                from: ""
                to: "ACTIVE"
                NumberAnimation { target: loaderLoading; properties: "opacity"; duration: 10 }
            }
        ]

        MouseArea { anchors.fill: parent; enabled: loader.status == Loader.Loading }

        Image {
            visible: !DEV_MODE
            source: "rot:/assets/background.png"
            anchors.fill: parent
        }

        Text {
            anchors.centerIn: parent
            color: "white"
            style: Text.Outline
            styleColor: "black"
            text: "Loading..."
            font.pixelSize: 32
        }
    }

    Rectangle {
        id: loaderError
        visible: false
        color: "red"
        anchors.fill: parent

        MouseArea { anchors.fill: parent; enabled: loaderError.visible }

        Column {
            anchors.centerIn: parent
            spacing: 10

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: "white"
                text: "An error has occured while loading QML :("
                font.pixelSize: 32
            }
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: "white"
                text: "See console for more info."
                font.pixelSize: 24
            }
        }
    }

    Connections { 
        target: frontend

        function onReloaded() {
            window.reloadQML()
        }
    }

    function reloadQML() {
        if (!loader.source) {
            console.log("Skipping hot-reload, nothing is loaded.")
            return
        }
        // hot reload
        console.log("Hot-reloading...")

        // pause game while reloading
        if (frontend.state == FrontendState.Game) frontend.game.paused = true

        var source = loader.source
        loader.source = ""
        loader.source = source
    }
}
