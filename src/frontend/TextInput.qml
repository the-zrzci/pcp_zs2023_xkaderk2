import QtQuick
import QtQuick.Controls

TextField {
    id: control
    placeholderText: "text"
    color: "white"
    property alias backgroundColor: backgroundRect.color

    leftPadding: 8
    verticalAlignment: TextField.AlignVCenter

    background: Rectangle {
        id: backgroundRect
        implicitWidth: 220
        implicitHeight: 45
        radius: 8
        color: control.enabled ? "transparent" : "#353637"
        border.color: control.enabled ? "#C34E4E" : "transparent"
        border.width: 2
    }
}
