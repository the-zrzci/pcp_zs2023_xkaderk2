import QtQuick
import "Global"

Item {
    anchors.centerIn: parent
    anchors.fill: parent
    id: mainMenuContainer

    property bool creatingGame: false

    NewGame {
        visible: creatingGame
    }

    Rectangle {
        id: mainMenuFrame
        visible: !creatingGame

        states: [
            State {
                name: "MenuOnTop"
                when: mainMenuContainer.height <= 500
                AnchorChanges {
                    target: mainMenuFrame
                    anchors.verticalCenter: undefined
                    anchors.top: parent.top
                }
            },
            State {
                name: "MenuInMiddle"
                when: mainMenuContainer.height > 500
                AnchorChanges {
                    target: mainMenuFrame
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.top: undefined
                }
            }
        ]


        width: 350
        implicitHeight: childrenRect.height + 50
        x: 60

        color: Qt.rgba(0.118,0.118,0.118,0.5)

        Rectangle {

            width: parent.width
            height: 100
            color: "transparent"

            FontLoader {
                id: rye
                source: "../../assets/fonts/Rye-Regular.ttf" // Path to your font file
            }

            Text {
                text: "Realm\nof Temporality"
                color: "white"
                anchors.centerIn: parent
                font {
                    pixelSize: 30
                    family: rye.name
                }
            }
        }


        Column {
            spacing: 18
            anchors.horizontalCenter: parent.horizontalCenter
            y: 120


            MenuButton {
                buttonText: "New game"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        creatingGame = true
                    }
                }
            }

            MenuButton {
                buttonText: "Load game"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: frontend.startGame("", "", true)
                }
            }

            MenuButton {
                buttonText: "Editor"
                visible: DEV_MODE

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        frontend.startEditor();
                    }
                }
            }

            MenuButton {
                buttonText: "Settings"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: Global.showSettings = true
                }
            }


            MenuButton {
                buttonText: "Quit game"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        Qt.quit();
                    }
                }
            }

        }


    }
}
