import QtQuick

import QtQuick

Rectangle {
    id: iconToggle

    property string source
    property bool active: false
    property alias iconHeight: icon.height

    color: "#aaa"

    states: [
        State {
            name: "active"
            when: active
            PropertyChanges {
                target: iconToggle
                color: "#C34E4E" 
            }
        },
        State {
            name: "hover"
            when: !active && mouseArea.containsMouse
            PropertyChanges {
                target: iconToggle
                color: "#bbb"
            }
        }
    ]

    width: 64
    height: 45
    radius: 8

    Image {
        id: icon
        source: iconToggle.source
        height: 32
        sourceSize.height: height // makes SVG look good
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent   
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        cursorShape: Qt.PointingHandCursor
    }
}
