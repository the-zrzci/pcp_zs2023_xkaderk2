import QtQuick
import QtQuick.Layouts
import "."

Rectangle {
    anchors.centerIn: parent
    color: Qt.rgba(0.118,0.118,0.118,0.5)

    width: childrenRect.width + 32
    height: childrenRect.height + 32

    ColumnLayout {
        x: 16
        y: 16
        spacing: 32

        Text {
            Layout.alignment: Qt.AlignCenter

            text: "New Game"
            color: "white"
            font {
                pixelSize: 30
                bold: true
            }
        }

        ColumnLayout {
            Layout.alignment: Qt.AlignCenter

            Text {
                Layout.alignment: Qt.AlignCenter
                text: "Player name"
                color: "white"
                font.pixelSize: 20
            }

            TextInput {
                id: nameInput
                focus: true
                placeholderText: "Player"
                backgroundColor: Qt.rgba(0,0,0,0.4)
                width: 256
                height: 32  
            }
        }

        ColumnLayout {
            Layout.alignment: Qt.AlignCenter

            Text {
                Layout.alignment: Qt.AlignCenter
                text: "Choose your class"
                color: "white"
                font.pixelSize: 20
            }

            RowLayout {
                spacing: 16

                Repeater {
                    model: ["Warrior", "Mage", "Paladin"]

                    Rectangle {
                        id: classTile
                        width: 128 + 8*2 + 3*2
                        height: 128 + 64
                        radius: 8
                        color: mouseArea.containsMouse ? Qt.rgba(1,1,1,0.4) : Qt.rgba(0,0,0,0.4)

                        Behavior on color {
                            ColorAnimation { target: classTile; duration: 100 }
                        }
                    
                        ColumnLayout {
                            x: 8
                            y: 8
                            spacing: 16

                            Rectangle {
                                width: childrenRect.width + 3
                                height: childrenRect.height + 3
                                color: Qt.rgba(0,0,0,0.5)
                                radius: 8

                                Image {
                                    x: 3
                                    y: 3
                                    Layout.alignment: Qt.AlignCenter
                                    source: "rot:/assets/splashart_"+modelData+".png"
                                    width: 128
                                    height: 128
                                    sourceSize {
                                        width: 128
                                        height: 128
                                    }
                                    layer.enabled: true
                                    layer.effect: ShaderEffect {
                                        property real radius: 0.69
                                        vertexShader: "qrc:/shaders/default.vert.qsb"
                                        fragmentShader: "qrc:/shaders/Round.frag.qsb"
                                    }
                                }
                            }

                            Text {
                                Layout.alignment: Qt.AlignCenter
                                color: "white"
                                text: modelData
                                font.pixelSize: 16
                            }
                        }

                        MouseArea {
                            id: mouseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            cursorShape: Qt.PointingHandCursor
                            onClicked: {
                                if (!nameInput.text) return nameInput.forceActiveFocus()
                                frontend.startGame(modelData, nameInput.text, false)
                            }
                        }
                    }
                } 
            }
        }
    }
}
