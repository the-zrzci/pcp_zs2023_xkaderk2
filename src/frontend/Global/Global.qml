pragma Singleton
import QtQuick 
import Frontend

QtObject {
    property real tileSize: TILE_SIZE
    property bool debug: frontend.state == FrontendState.Editor
    property real shadowBrightness: 0.7
    property bool enableAO: true
    property bool enableReflections: true
    property bool enableShadows: true
    property bool renderFast: false
    property bool showSettings: false
}
