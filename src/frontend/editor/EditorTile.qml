import QtQuick
import "../Global"
import "../game"

Rectangle {
    id: tileRoot
    property int index
    property bool renderWall
    property bool renderFast
    property point coords: frontend.editor.getTileCoords(index)

    color: "white"

    x: Global.tileSize * coords.x
    y: Global.tileSize * coords.y - Global.tileSize * tile.height * mapTile.angle
    z: coords.y + frontend.editor.coordOffset.y

    width: Global.tileSize
    height: Global.tileSize * (tile.height * mapTile.angle + 1)

    property var tile: frontend.editor.getTile(coords)
    Connections { 
        target: frontend.editor
    
        function onTileChanged(x, y) {
            if (x != coords.x || y != coords.y) return
            tileRoot.tile = frontend.editor.getTile(coords)
        }
    }

    MapTile {
        id: mapTile
        anchors.fill: parent
        tileX: tileRoot.coords.x
        tileY: tileRoot.coords.y
        tile: tileRoot.tile
        renderWall: tileRoot.renderWall
        renderFast: tileRoot.renderFast

        opacity: tileMouseArea.containsMouse ? 0.4 : 1
    }

    MouseArea {
        id: tileMouseArea
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.width
        acceptedButtons: Qt.NoButton
        hoverEnabled: true
    }
}
