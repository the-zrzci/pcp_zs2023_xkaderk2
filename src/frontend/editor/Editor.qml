import QtQuick
import QtQuick.Shapes
import "../Global"
import "../game"
import "sidebar"
import "rightbar"

Rectangle {
    id: editorRoot
    anchors.fill: parent
    color: "white"

    property int selectedLocation: -1
    property var selectedEntity: undefined

    MouseArea {
        id: dragArea
        anchors.fill: parent
        hoverEnabled: true
        acceptedButtons: Qt.LeftButton | Qt.MiddleButton | Qt.RightButton
        property real lastX: 0
        property real lastY: 0
        property point mapCursorPos
        property point lastTileCoords: Qt.point(0,0)
        property point pressedTile
        property rect selectRect: Qt.rect(0,0,0,0)

        onWheel: (ev) => {
            var lastZoom = mapRoot.zoom
            var lastPos = Qt.point(mapRoot.x, mapRoot.y)
            mapRoot.zoom = Math.min(2, Math.max(0.1, mapRoot.zoom + ev.angleDelta.y / 1024))

            var zoomChange = mapRoot.zoom / lastZoom;
            mapRoot.x = ev.x - zoomChange * (ev.x - mapRoot.x)
            mapRoot.y = ev.y - zoomChange * (ev.y - mapRoot.y)
            overlay.requestPaint()
        }

        onPositionChanged: (ev) => onMouse(ev)
        onPressed: (ev) => {
            lastTileCoords.x -= 1 // invalidate coords to allow placing
            onMouse(ev)
            pressedTile = Qt.point(mapCursorPos.x, mapCursorPos.y)
        }
        onReleased: (ev) => {
            if (sidebar.tileMode && sidebar.rectangleMode) {
                if (ev.button == Qt.LeftButton) {
                    frontend.editor.fill(selectRect, sidebar.selectedBaseTile)
                } else {
                    frontend.editor.fill(selectRect)
                }
            }

            selectRect = Qt.rect(0,0,0,0)
        }

        onClicked: (ev) => {
            if (ev.button == Qt.LeftButton) {
                if (sidebar.entityMode) {
                // Spawning entitities
                    frontend.editor.addEntityTo(sidebar.selectedEntity, mapCursorPos.x, mapCursorPos.y)
                } else if (sidebar.locationMode) {
                    // Spawning points
                    switch (sidebar.selectedLocationType) {
                        case "point":
                            frontend.editor.addPoint(mapCursorPos.x, mapCursorPos.y)
                            break
                        case "area":
                            frontend.editor.addArea(mapCursorPos.x, mapCursorPos.y)
                            break
                    }
                }
            }
        }

        function recalcSelectRect() {
            var topLeft = Qt.point(
                Math.floor(Math.min(pressedTile.x, mapCursorPos.x)),
                Math.floor(Math.min(pressedTile.y, mapCursorPos.y)),
            )
            var bottomRight = Qt.point(
                Math.ceil(Math.max(pressedTile.x, mapCursorPos.x)),
                Math.ceil(Math.max(pressedTile.y, mapCursorPos.y)),
            )

            selectRect = Qt.rect(
                topLeft.x,
                topLeft.y,
                bottomRight.x - topLeft.x,
                bottomRight.y - topLeft.y,
            )
        }

        function toMapPos(x, y) {
            return Qt.point(
                (x - mapRoot.x) / mapRoot.tileSize,
                (y - mapRoot.y) / mapRoot.tileSize
            )
        }

        function onMouse(ev) {
            mapCursorPos = toMapPos(ev.x, ev.y)
            
            // Custom drag
            if (dragArea.pressedButtons & Qt.MiddleButton) {
                mapRoot.x += ev.x - lastX
                mapRoot.y += ev.y - lastY
                overlay.requestPaint()
            }
            lastX = ev.x
            lastY = ev.y

            // Simulate player on cursor
            // frontend.game.player.position = mapCursorPos
            // Tile coords 
            var tileCoords = Qt.point(Math.floor(mapCursorPos.x), Math.floor(mapCursorPos.y))

            // Drawing
            if (sidebar.tileMode && !sidebar.rectangleMode && lastTileCoords != tileCoords) {
                if (dragArea.pressedButtons & Qt.LeftButton) {
                    // Left mouse button
                    frontend.editor.addTileTo(sidebar.selectedBaseTile, tileCoords.x, tileCoords.y)
                } else if (dragArea.pressedButtons & Qt.RightButton) {
                    // Right mouse button
                    frontend.editor.removeTile(tileCoords.x, tileCoords.y)
                }
            }
            lastTileCoords = tileCoords

            positionText.text = mapCursorPos.x.toFixed(2) + " x " + mapCursorPos.y.toFixed(2)
            positionText.text += " (" + tileCoords.x + " x " + tileCoords.y + ")"

            // Recalculate select rectangle
            if (sidebar.tileMode && sidebar.rectangleMode && dragArea.pressedButtons & (Qt.LeftButton | Qt.RightButton)) {
                recalcSelectRect()
            }
        }

        Item {
            id: reflection
            // Placeholder
        }
        
        Item {
            id: mapRoot

            smooth: false

            property real zoom: 1
            property real tileSize: Global.tileSize * mapRoot.zoom

            transform: Scale { xScale: mapRoot.zoom; yScale: mapRoot.zoom }

            Component.onCompleted: {
                mapRoot.x = editorRoot.width / 2
                mapRoot.y = editorRoot.height / 2
            }

            // Custom repeater
            property var tiles: []

            Component { 
                id: tileComponent
                EditorTile { 
                    renderWall: rightbar.perspectiveEnabled 
                    renderFast: rightbar.fastRender
                } 
            }

            Connections {
                target: frontend.editor

                function onTileRemoved(x, y) {
                    // Find and destroy the item
                    for (var i = mapRoot.tiles.length; i > 0 ; i--) {
                        var tile = mapRoot.tiles[i-1].object
                        if (tile && tile.coords.x == x && tile.coords.y == y) {
                            tile.destroy()
                            mapRoot.tiles.splice(i-1, 1)
                            break
                        }
                    }
                }

                function onTileCountChanged(reset) {
                    var wantedCount = frontend.editor.getTileCoordCount()

                    if (reset) {
                        // Delete all items on reset
                        for(var i = mapRoot.tiles.length; i > 0 ; i--) {
                            mapRoot.tiles.pop().object.destroy()
                        }
                    }

                    // If we cannot load component give up
                    if (tileComponent.status == Component.Error) {
                        console.error(tileComponent.errorString())
                        return
                    }

                    var difference = mapRoot.tiles.length - wantedCount

                    if (difference > 0) {
                        // If we have too many children
                        // for (var i = 0; i < difference; i++) {
                        //     mapRoot.tiles.pop().object.destroy()
                        // }
                    } else if (difference < 0){
                        // If we have too few children
                        for (var i = 0; i < -difference; i++) {
                            var incubator = tileComponent.incubateObject(mapRoot, { 
                                index: mapRoot.tiles.length,
                            })
                            mapRoot.tiles.push(incubator)
                        }
                    }
                }

                Component.onCompleted: onTileCountChanged(true)
            }

            Repeater {
                model: frontend.editor.entityPositions

                Entity {
                    id: entityItem
                    entity: frontend.editor.getEntity(modelData)
                    hoverable: sidebar.entityMode
                    hoverableJustHitbox: true
                    x: modelData.x * Global.tileSize - width*0.5
                    y: modelData.y * Global.tileSize - height*0.5
                    z: entityMouse.drag.active 
                        ? (modelData.y + frontend.editor.coordOffset.y) * 1000
                        : modelData.y + frontend.editor.coordOffset.y + entity.hitbox.height*0.5


                    MouseArea {
                        enabled: sidebar.entityMode
                        id: entityMouse
                        anchors.fill: parent
                        cursorShape: entityMouse.drag.active ? Qt.ClosedHandCursor : Qt.OpenHandCursor
                        drag { 
                            target: entityItem
                            onActiveChanged: {
                                // Move entity
                                if (!drag.active) {
                                    var newPos = Qt.point(
                                        (entityItem.x + entityItem.width*0.5) / Global.tileSize,
                                        (entityItem.y + entityItem.height*0.5) / Global.tileSize,
                                    )

                                    if (!selectedEntity || selectedEntity == modelData) {
                                        // Keep entity selected after moving
                                        selectedEntity = newPos
                                    }

                                    frontend.editor.moveEntity(modelData, newPos.x, newPos.y)
                                } else {
                                    selectedEntity = modelData
                                }
                            }
                        }
                        onClicked: {
                            selectedEntity = modelData
                        }
                    }

                    Rectangle {
                        visible: sidebar.entityMode && selectedEntity == modelData
                        anchors.fill: parent
                        color: Qt.rgba(0,1,1,0.5)
                        border.width: 4
                        border.color: "red"
                        radius: Math.min(parent.width, parent.height) * 0.25
                    }
                }
            }

            Repeater {
                model: frontend.editor.locations

                Location {
                    id: locationItem
                    location: modelData
                    selected: sidebar.locationMode && selectedLocation == index
                    z: (frontend.editor.coordOffset.y + 1000)* 1000

                    Connections {
                        target: frontend.editor
                        function onLocationChanged(i) {
                            if (i != index) return
                            locationItem.location = frontend.editor.locations[index]
                        }
                        Component.onCompleted: locationItem.location = frontend.editor.locations[index]
                    }

                    MouseArea {
                        enabled: sidebar.locationMode
                        id: locationMouse
                        anchors.fill: parent
                        cursorShape: locationMouse.drag.active ? Qt.ClosedHandCursor : Qt.OpenHandCursor
                        onClicked: {
                            selectedLocation = index
                        }
                        drag { 
                            target: locationItem
                            onActiveChanged: {                                
                                // Move location
                                if (!drag.active) {
                                    var type = locationItem.location.type
                                    var x = (locationItem.x + (type ? 0 : locationItem.width*0.5)) / Global.tileSize
                                    var y = (locationItem.y + (type ? 0 : locationItem.height*0.5)) / Global.tileSize

                                    frontend.editor.moveLocation(
                                        index,
                                        Math.round(x / 0.25) * 0.25,
                                        Math.round(y / 0.25) * 0.25,
                                    )
                                } else {
                                    selectedLocation = index
                                }
                            }
                        }
                    }
                }
            }
        }

        Rectangle {
            id: rectanglePreview
            visible: dragArea.width > 0

            color: "gray"
            opacity: 0.4

            x: mapRoot.x + dragArea.selectRect.x * mapRoot.tileSize
            y: mapRoot.y + dragArea.selectRect.y * mapRoot.tileSize
            width: dragArea.selectRect.width * mapRoot.tileSize
            height: dragArea.selectRect.height * mapRoot.tileSize

        }

        Canvas {
            id: overlay
            anchors.fill: parent
            opacity: 0.5
            onPaint: {
                var ctx = getContext("2d")
                ctx.clearRect(0, 0, canvasSize.width, canvasSize.height)

                ctx.lineWidth = 1
                ctx.strokeStyle = "#aaa"

                ctx.beginPath()
                var offsetX = mapRoot.x % mapRoot.tileSize
                var ncols = width / mapRoot.tileSize
                for(var i=0; i < ncols+1; i++){
                    ctx.moveTo(mapRoot.tileSize * i + offsetX, 0);
                    ctx.lineTo(mapRoot.tileSize * i + offsetX, height);
                }

                var offsetY = mapRoot.y % mapRoot.tileSize
                var nrows = height / mapRoot.tileSize
                for(var i=0; i < nrows+1; i++){
                    ctx.moveTo(0, mapRoot.tileSize * i + offsetY);
                    ctx.lineTo(width, mapRoot.tileSize * i + offsetY);
                }

                ctx.closePath()
                ctx.stroke()
            }
        }
    }

    Sidebar {
        id: sidebar
    }  

    Rightbar {
        id: rightbar
    }

    Text { 
        id: positionText
        font.family: "monospace" 
        font.pointSize: 8
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 2
        }
    }
} 
