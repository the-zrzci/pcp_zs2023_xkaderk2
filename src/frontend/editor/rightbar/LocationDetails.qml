import QtQuick
import QtQuick.Layouts
import "../../"

ColumnLayout {
    visible: selectedLocation > -1
    spacing: 16

    property var location: frontend.editor.locations[selectedLocation]

    Column {
        Text {
            color: "white"
            text: "Location name:"
        }

        TextInput {
            id: locationName
            
            text: location?.name ?? ""
            onTextEdited: {
                frontend.editor.renameLocation(selectedLocation, locationName.text)
            }
        }
    }

    RowLayout {
        visible: location?.type == 1
        spacing: 8

        ColumnLayout {
            Layout.fillWidth: true
            Text {
                color: "white"
                text: "Width:"
            }

            TextInput {
                id: areaWidth
                Layout.fillWidth: true
                implicitWidth: 0

                validator: DoubleValidator {bottom: 0.5; top: 256}

                text: location?.width ?? 0
                onTextEdited: {
                    frontend.editor.resizeLocation(selectedLocation, +areaWidth.text, 0)
                }
            }
        }

        Text {
            color: "white"
            text: "x"
            Layout.alignment: Qt.AlignVCenter
        }

        ColumnLayout {
            Layout.fillWidth: true
            Text {
                color: "white"
                text: "Height:"
            }

            TextInput {
                id: areaHeight
                Layout.fillWidth: true
                implicitWidth: 0

                validator: DoubleValidator {bottom: 0.5; top: 256}

                text: location?.height ?? 0
                onTextEdited: {
                    frontend.editor.resizeLocation(selectedLocation, 0, +areaHeight.text)
                }
            }
        }
    }

    Column {
        visible: location?.type == 1
        Text {
            color: "white"
            text: "Target location:"
        }

        TextInput {
            id: targetName
            placeholderText: "None"
            text: location?.target ?? ""
            onTextEdited: {
                frontend.editor.setLocationTarget(selectedLocation, targetName.text)
            }
        }
    }

    Item { Layout.fillHeight: true }

    Rectangle {
        height: 1
        width: parent.width
        color: "#444"
    }

    MenuButton {
        buttonText: "Remove location"
        isPrimary: false

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: {
                frontend.editor.removeLocation(selectedLocation)
                selectedLocation = -1
            }
        }
    }
}
