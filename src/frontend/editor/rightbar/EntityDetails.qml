import QtQuick
import QtQuick.Layouts
import "../../"

ColumnLayout {
    visible: !!selectedEntity
    spacing: 10
    property var entity: selectedEntity ? frontend.editor.getEntity(selectedEntity) : {}

    Connections {
        target: frontend.editor
        function onEntityCountChanged(i) {
            if (!selectedEntity) return
            entity = Qt.binding(() => selectedEntity ? frontend.editor.getEntity(selectedEntity) : {})
        }
    }

    Column {
        Text {
            color: "white"
            text: "Entity nickname:"
        }

        TextInput {
            id: entityNickname
            placeholderText: "None"
            text: entity.nickname || ""
            onTextEdited: {
                frontend.editor.nicknameEntity(selectedEntity, entityNickname.text)
            }
        }
    }

    Item { Layout.fillHeight: true }

    Rectangle {
        height: 1
        width: parent.width
        color: "#444"
    }

    MenuButton {
        buttonText: "Remove entity"
        isPrimary: false

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor
            onClicked: {
                frontend.editor.removeEntity(selectedEntity)
                selectedEntity = undefined
            }
        }
    }
}
