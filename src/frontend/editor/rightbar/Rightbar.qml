import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import "../../"

Rectangle {
    property bool fastRender: fastRenderSwitch.checked
    property bool perspectiveEnabled: perspectiveSwitch.checked

    anchors {
        top: parent.top
        right: parent.right
        bottom: parent.bottom
    }

    width: 250
    height: parent.height

    color: Qt.rgba(0, 0, 0, 0.5)

    MouseArea {
        anchors.fill: parent

        ColumnLayout {
            x: 10
            y: 10
            height: parent.height - 20
            spacing: 10

            Row {
                Text {
                    color: "white"
                    text: "Map's default tile:"
                }

                ComboBox {
                    id: defaultTile
                    model: frontend.editor.getTileNames()
                    onActivated: frontend.editor.defaultTile = model[defaultTile.currentIndex]
                }
            }

            Item { Layout.fillHeight: true }
            
            Component { id: locationDetails; LocationDetails { } }
            Component { id: entityDetails; EntityDetails {} }

            Loader {
                sourceComponent: [undefined, entityDetails, locationDetails][sidebar.mode]
            }


            Item { Layout.fillHeight: true }

            Switch {
                id: perspectiveSwitch
                checked: true
                text: "Show perspective"
            }

            Switch {
                id: fastRenderSwitch
                checked: false
                text: "Fast rendering"
            }

            Text {  
                text: "Reload"
                font.underline: true
                MouseArea {
                    anchors.fill: parent
                    onClicked: frontend.editor.refresh()
                    cursorShape: Qt.PointingHandCursor
                }
            }
        }
    }
}
