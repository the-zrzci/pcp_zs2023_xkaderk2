import QtQuick
import "../Global"

Item {
    property var location
    property bool selected: false

    x: (location.x - (location.type ? 0 : 0.5)) * Global.tileSize
    y: (location.y - (location.type ? 0 : 0.5)) * Global.tileSize

    width: (location.width > 0 ? location.width : 1) * Global.tileSize
    height: (location.height > 0 ? location.height : 1) * Global.tileSize

    Component {
        id: pointLocation

        Rectangle {
            anchors.fill: parent
            radius: parent.width
            color : "#00ffff"

            border.color: selected ? "red" : "transparent"
            border.width: 4

            Text {
                text: "P"
                font.bold: true
                font.pixelSize: parent.height * 0.75
                anchors.centerIn: parent
            }
        }
    }

    Component {
        id: areaLocation

        Rectangle {
            color: selected ? "yellow" : Qt.rgba(1,0,0,0.2)
            
            anchors.fill: parent

            border.color: "red"
            border.width: selected ? 4 : 2
        }
    }
    
    
    Loader {
        anchors.fill: parent
        sourceComponent: [pointLocation, areaLocation][location.type]
    }

    Rectangle {
        visible: mouseArea.containsMouse
        color: Qt.rgba(0,0,0,0.3)

        anchors.bottom: location.type ? undefined : parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: location.type ? parent.verticalCenter : undefined

        width: childrenRect.width
        height: childrenRect.height

        Text {
            color: "white"
            text: location.name
            font.pointSize: 16
        }
    }

    MouseArea {
        enabled: sidebar.locationMode
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
    }
}
