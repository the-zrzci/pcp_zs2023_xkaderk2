import QtQuick
import QtQuick.Layouts

Column {
    id: tileList
    anchors.fill: parent

    SelectorItem {
        active: selectedLocationType == "point"
        label: "Point"

        icon: Text {
            text: "P"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor

            onClicked: selectedLocationType = "point"
        }
    }

    SelectorItem {
        active: selectedLocationType == "area"
        label: "Area"

        icon: Text {
            text: "A"
            anchors.centerIn: parent
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor

            onClicked: selectedLocationType = "area"
        }
    }
}
