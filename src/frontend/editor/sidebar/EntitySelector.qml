import QtQuick
import QtQuick.Layouts
import "../../game"

ListView {
    id: tileList
    anchors.fill: parent

    model: [
        frontend.editor.getObjectNames,
        frontend.editor.getEnemyNames,
        frontend.editor.getFriendNames,
        frontend.editor.getItemNames,
    ][entityCategory]()

    clip: true

    delegate: SelectorItem {
        active: selectedEntity == modelData
        label: modelData

        icon: SpriteSheet {
            sheet: frontend.editor.getEntityBase(modelData).spriteSheet

            x: 0
            y: 0
            z: 0
            width: 32
            height: 32
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor

            onClicked: selectedEntity = modelData
        }
    }
}
