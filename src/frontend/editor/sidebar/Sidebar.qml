import QtQuick
import QtQuick.Layouts
import QtQuick.Dialogs
import "../../"

Rectangle {
    anchors {
        top: parent.top
        left: parent.left
        bottom: parent.bottom
    }

    width: 250
    height: parent.height

    color: Qt.rgba(0, 0, 0, 0.5)

    property string selectedBaseTile: frontend.editor.getTileNames()[0]
    property string selectedEntity: frontend.editor.getEntityNames()[0]
    property string selectedLocationType: "point"

    property int mode: 0 // 0 tiles  1 entities  2 locations
    property bool tileMode: mode == 0
    property bool entityMode: mode == 1
    property bool locationMode: mode == 2

    property int entityCategory: 0

    property bool rectangleMode: false

    MouseArea {
        anchors.fill: parent
        ColumnLayout {
            x: 10
            y: 10
            height: parent.height - 20
            spacing: 10


            MenuButton {
                buttonText: "Load map"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: loadDialog.open()
                }

                FileDialog {
                    id: loadDialog
                    currentFolder: "../assets/maps"
                    defaultSuffix: "json"
                    nameFilters: ["JSON map files (*.json)"]
                    fileMode: FileDialog.OpenFile

                    onAccepted: {
                        if (frontend.editor.loadFile(loadDialog.selectedFile)) {
                            console.log("Loaded:)")
                        } else {
                            console.log("Not loaded? :(")
                        }
                    }
                }
            }

            MenuButton {
                buttonText: "Save map"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: saveDialog.open()
                }

                FileDialog {
                    id: saveDialog
                    currentFolder: "../assets/maps"
                    defaultSuffix: "json"
                    fileMode: FileDialog.SaveFile

                    onAccepted: {
                        if (frontend.editor.saveToFile(saveDialog.selectedFile)) {
                            console.log("Saved:)")
                        } else {
                            console.log("Not saved? :(")
                        }
                    }
                }
            }


            Text {
                text: "Mode: " + ["TILES", "ENTITIES", "LOCATIONS"][mode]
                color: "white"
            }
            RowLayout {
                spacing: 10
                Layout.fillWidth: true
                width: parent.childrenRect.width

                IconButton {
                    Layout.fillWidth: true
                    source: "rot:/assets/icons/cubes-solid.svg"
                    active: mode == 0

                    MouseArea {
                        anchors.fill: parent
                        onClicked: mode = 0
                        cursorShape: Qt.PointingHandCursor
                    }
                }

                IconButton {
                    Layout.fillWidth: true
                    source: "rot:/assets/icons/dragon-solid.svg"
                    active: mode == 1

                    MouseArea {
                        anchors.fill: parent
                        onClicked: mode = 1
                        cursorShape: Qt.PointingHandCursor
                    }
                }


                IconButton {
                    Layout.fillWidth: true
                    source: "rot:/assets/icons/location-dot-solid.svg"
                    active: mode == 2

                    MouseArea {
                        anchors.fill: parent
                        onClicked: mode = 2
                        cursorShape: Qt.PointingHandCursor
                    }
                }
            }

            Rectangle {
                visible: entityMode
                color: Qt.rgba(0,0,0,0.2)
                Layout.fillWidth: true
                height: childrenRect.height + 14

                Layout.bottomMargin: -10

                RowLayout {
                    spacing: 10
                    width: parent.width - 8
                    x: 4
                    y: 4

                    IconButton {
                        Layout.fillWidth: true
                        source: "rot:/assets/icons/house-solid.svg"
                        active: entityCategory == 0
                        height: 32
                        iconHeight: 24

                        MouseArea {
                            anchors.fill: parent
                            onClicked: entityCategory = 0
                            cursorShape: Qt.PointingHandCursor
                        }
                    }

                    IconButton {
                        Layout.fillWidth: true
                        source: "rot:/assets/icons/skull-solid.svg"
                        active: entityCategory == 1
                        height: 32
                        iconHeight: 24

                        MouseArea {
                            anchors.fill: parent
                            onClicked: entityCategory = 1
                            cursorShape: Qt.PointingHandCursor
                        }
                    }

                    IconButton {
                        Layout.fillWidth: true
                        source: "rot:/assets/icons/heart-solid.svg"
                        active: entityCategory == 2
                        height: 32
                        iconHeight: 24

                        MouseArea {
                            anchors.fill: parent
                            onClicked: entityCategory = 2
                            cursorShape: Qt.PointingHandCursor
                        }
                    }

                    IconButton {
                        Layout.fillWidth: true
                        source: "rot:/assets/icons/wand-magic-solid.svg"
                        active: entityCategory == 3
                        height: 32
                        iconHeight: 24

                        MouseArea {
                            anchors.fill: parent
                            onClicked: entityCategory = 3
                            cursorShape: Qt.PointingHandCursor
                        }
                    }
                }
            }
            
            Component { id: tileSelect; BaseTileSelector {} }
            Component { id: entitySelect; EntitySelector {} }
            Component { id: locationSelect; LocationTypeSelector {} }
            Rectangle {
                color: Qt.rgba(0, 0, 0, 0.2)
                Layout.fillWidth: true
                Layout.fillHeight: true

                Loader {
                    anchors.fill: parent
                    sourceComponent: [tileSelect, entitySelect, locationSelect][mode]
                }
            }

            MenuButton {
                buttonText: "Back to menu"

                MouseArea {
                    anchors.fill: parent
                    cursorShape: Qt.PointingHandCursor
                    onClicked: {
                        frontend.exitEditor();
                    }
                }
            }
        }

        Row {
            visible: tileMode
            enabled: tileMode

            anchors {
                top: parent.top
                topMargin: 10
                left: parent.right
                leftMargin: 10
            }

            spacing: 10

            IconButton {
                Layout.fillWidth: true
                source: "rot:/assets/icons/paintbrush-solid.svg"
                active: !rectangleMode

                MouseArea {
                    anchors.fill: parent
                    onClicked: rectangleMode = false
                    cursorShape: Qt.PointingHandCursor
                }
            }

            IconButton {
                Layout.fillWidth: true
                source: "rot:/assets/icons/expand-solid.svg"
                active: rectangleMode

                MouseArea {
                    anchors.fill: parent
                    onClicked: rectangleMode = true
                    cursorShape: Qt.PointingHandCursor
                }
            }
        }
    }
}
