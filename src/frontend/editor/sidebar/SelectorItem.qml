import QtQuick
import QtQuick.Layouts

Rectangle {
    property bool active: false
    property alias icon: itemIcon.children
    property alias label: itemLabel.text

    id: listItem
    height: 64
    width: parent?.width ?? 0

    color: "transparent"

    states: [
        State {
            name: "hovered"
            PropertyChanges {
                target: listItem
                color: Qt.rgba(1, 1, 1, 0.3)
            }
        },
        State {
            name: "selected"
            when: active
            PropertyChanges {
                target: listItem
                color: Qt.rgba(0.9, 0.4, 0.1, 0.5)
            }
        }
    ]

    MouseArea {
        hoverEnabled: true
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor

        onEntered: {
            listItem.state = "hovered"
        }

        onExited: {
            listItem.state = active ? "selected" : ""
        }
    }

    RowLayout {
        anchors {
            fill: parent
            leftMargin: 10
            rightMargin: 10
        }
        spacing: 10

        Item { 
            id: itemIcon
            width: 32
            height: 32
        }

        Text {
            id: itemLabel
            Layout.fillWidth: true
            verticalAlignment: Text.AlignVCenter
            y: 16

            color: "white"
            text: ""
        }
    }
}
