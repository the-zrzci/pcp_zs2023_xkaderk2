import QtQuick
import QtQuick.Layouts
import "../../game"

ListView {
    id: tileList
    anchors.fill: parent

    model: frontend.editor.getTileNames()

    clip: true

    delegate: SelectorItem {
        active: selectedBaseTile == modelData
        label: modelData

        icon: MapTile {
            tileX: 0
            tileY: 0
            tile: {
                var tile = frontend.editor.getTileBase(modelData)
                if (tile.height > 1) tile.height = 1
                return tile
            }

            width: 32
            height: 32
        }

        MouseArea {
            anchors.fill: parent
            cursorShape: Qt.PointingHandCursor

            onClicked: selectedBaseTile = modelData
        }
    }
}
