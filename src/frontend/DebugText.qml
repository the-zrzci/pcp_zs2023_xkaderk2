import QtQuick
import "Global"

Text {
    visible: Global.debug
    color: "white"
    text: ""

    Rectangle {
        anchors.fill: parent
        z: -1
        color: "black"
        opacity: 0.3
    }
}
