import QtQuick

Rectangle {
    id: mainMenuButton

    property bool isPrimary: true
    property string buttonText: "-"

    color: isPrimary ? "#C34E4E" : "#4E4EC3"

    width: 220
    height: 45
    radius: 8

    FontLoader {
        id: kottaOne
        source: "../../assets/fonts/KottaOne-Regular.ttf" // Path to your font file
    }

    Text {
        text: mainMenuButton.buttonText
        color: "white"
        anchors.centerIn: parent

        font {
            pixelSize: 24
            family: kottaOne.name
        }
    }

    states: [
        State {
            name: "hover"
            when: mouseArea.containsMouse

            PropertyChanges {
                target: mainMenuButton
                color: "#91c3cf"
            }
        }
    ]
    MouseArea {
        id: mouseArea
        anchors {
            fill: parent
        }
        hoverEnabled: true
        acceptedButtons: Qt.NoButton
        cursorShape: Qt.PointingHandCursor
    }

    transitions: [
        Transition {
            ColorAnimation {
                duration: 130 //ms
            }
        }
    ]

    /*
    onHoveredChanged: {

        if (hovered) {
            mainMenuButton.color = "#91c3cf"; // Change color on hover
        } else {
            mainMenuButton.color = "#C34E4E"; // Change color back when not hovered
        }
    }
    */
}
