#ifndef REALMOFTEMPORALITY_EDITOR_H
#define REALMOFTEMPORALITY_EDITOR_H

#include <QByteArray>
#include <QColor>
#include <QJsonObject>
#include <QList>
#include <QObject>
#include <QPointF>
#include <QString>
#include <QUrl>
#include <map>
#include <vector>

#include "Matrix.h"
#include "src/data/tiles.h"
#include "src/game/Entity.h"
#include "src/game/core/Game.h"
#include "src/game/core/Location.h"
#include "src/game/core/Tile.h"

namespace editor {
    class Editor : public QObject {
        Q_OBJECT

        Q_PROPERTY(int tileCoordCount READ getTileCoordCount NOTIFY tileCountChanged);
        Q_PROPERTY(QList<QPointF> entityPositions READ getEntityPositions NOTIFY entityCountChanged);
        Q_PROPERTY(QPointF coordOffset READ getCoordOffset NOTIFY tileCountChanged);
        Q_PROPERTY(QList<game::core::Location> locations READ getLocations NOTIFY locationCountChanged);
        Q_PROPERTY(QString defaultTile MEMBER m_defaultTile NOTIFY defaultTileChanged);

        QString m_loadedFilename;
        game::core::Game* m_game;
        Matrix<QPoint, game::core::Tile> m_tiles;
        Matrix<QPointF, game::EntityParams> m_entities;
        QList<game::core::Location> m_locations;
        QString m_defaultTile = data::DEFAULT_DEFAULT_TILE;

        QList<QPoint> m_tileCoords;

      public:
        Editor(game::core::Game* game);

        void save(QJsonObject& json);
        void load(const QJsonObject& json);
        Q_INVOKABLE bool saveToFile(QUrl path);
        Q_INVOKABLE bool loadFile(QUrl path);

        // Base tiles
        Q_INVOKABLE QList<QString> getTileNames() const;
        Q_INVOKABLE game::core::Tile getTileBase(QString name) const;

        // Base entities
        Q_INVOKABLE QList<QString> getItemNames() const;
        Q_INVOKABLE QList<QString> getObjectNames() const;
        Q_INVOKABLE QList<QString> getEnemyNames() const;
        Q_INVOKABLE QList<QString> getFriendNames() const;
        Q_INVOKABLE QList<QString> getEntityNames() const;
        Q_INVOKABLE game::EntityParams getEntityBase(QString name) const;

        //
        // TILES

        // Tile getters
        Q_INVOKABLE int getTileCoordCount() const;
        Q_INVOKABLE QPoint getTileCoords(int index) const;
        Q_INVOKABLE game::core::Tile getTile(QPoint position) const;

        // Tile creators
        Q_INVOKABLE void addTileTo(QString baseName, int x, int y);

        // Tile setters
        Q_INVOKABLE void fill(QRect rect);
        Q_INVOKABLE void fill(QRect rect, QString baseName);

        // Tile removers
        Q_INVOKABLE void removeTile(int x, int y);
        Q_INVOKABLE void removeTile(int x, int y, bool update);

        //
        // ENTITIES

        // Entity getters
        Q_INVOKABLE QList<QPointF> getEntityPositions() const;
        Q_INVOKABLE game::EntityParams getEntity(QPointF position) const;

        // Entity creators
        Q_INVOKABLE void addEntityTo(QString name, double x, double y);

        // Entity updaters
        Q_INVOKABLE void moveEntity(QPointF from, double x, double y);
        Q_INVOKABLE void nicknameEntity(QPointF position, QString name);

        // Entity removers
        Q_INVOKABLE void removeEntity(QPointF position);

        //
        // LOCATIONS

        // Location getters
        Q_INVOKABLE QList<game::core::Location> getLocations() const;

        // Location creators
        Q_INVOKABLE void addPoint(double x, double y);
        Q_INVOKABLE void addArea(double x, double y);

        // Location updaters
        Q_INVOKABLE void moveLocation(int index, double x, double y);
        Q_INVOKABLE void renameLocation(int index, QString name);
        Q_INVOKABLE void resizeLocation(int index, qreal width, qreal height);
        Q_INVOKABLE void setLocationTarget(int index, QString target);

        // Location removers
        Q_INVOKABLE void removeLocation(int index);

        //
        // Util
        Q_INVOKABLE QPointF getCoordOffset() const;
        Q_INVOKABLE void refresh() const;

      signals:
        void tileChanged(int x, int y) const;
        void tileRemoved(int x, int y) const;
        void tileCountChanged(bool reset) const;
        void tileListChanged() const;

        void entityCountChanged() const;

        void locationCountChanged() const;
        void locationChanged(int index) const;

        void defaultTileChanged() const;
    };
}  // namespace editor

#endif  // REALMOFTEMPORALITY_EDITOR_H
