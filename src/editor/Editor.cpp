#include "Editor.h"

#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>

#include "src/data/entities.h"
#include "src/game/core/utils.h"

editor::Editor::Editor(game::core::Game* game) : m_game(game) {}

void editor::Editor::save(QJsonObject& json) {
    // Clean up the map
    m_tiles.removeValue({});

    // Convert map to 2D vector
    auto tiles = m_tiles.toVector();

    // Save map default tile
    json["defaultTile"] = m_defaultTile;

    // Convert map to json
    QJsonArray jsonTiles;

    for (auto row : tiles) {
        QJsonArray jsonRow;

        for (auto tile : row) {
            QJsonObject jsonTile;

            tile.saveParameters(jsonTile);

            jsonRow.append(jsonTile);
        }

        jsonTiles.append(jsonRow);
    }

    json["tiles"] = std::move(jsonTiles);

    // Convert entities to json
    QJsonArray jsonEntities;
    QPointF offset = getCoordOffset();

    for (auto pair : m_entities.getData()) {
        QJsonObject jsonEntity;
        jsonEntity["position"] = game::core::qPairToJson(pair.first + offset);
        jsonEntity["name"] = pair.second.name;
        if (!pair.second.nickname.isEmpty()) jsonEntity["nickname"] = pair.second.nickname;
        jsonEntities.append(jsonEntity);
    }

    json["entities"] = std::move(jsonEntities);

    // Convert locations to json
    QJsonArray jsonLocations;

    for (auto location : m_locations) {
        QJsonObject jsonLocation;
        location.translate(offset);
        location.saveParameters(jsonLocation);
        jsonLocations.append(jsonLocation);
    }

    json["locations"] = std::move(jsonLocations);
}

void editor::Editor::load(const QJsonObject& json) {
    // Load default tile
    m_defaultTile = json.value("defaultTile").toString();

    // Clear current tiles
    m_tiles.clear();
    m_tileCoords.clear();

    // Load tiles
    QJsonArray jsonTiles = json.value("tiles").toArray();

    for (int y = 0; y < jsonTiles.size(); y++) {
        auto jsonRow = jsonTiles[y].toArray();

        for (int x = 0; x < jsonRow.size(); x++) {
            auto tile = game::core::Tile::load(jsonRow[x].toObject());
            if (tile.isEmpty()) {
                continue;
            }

            m_tiles.set(x, y, data::getTile(tile));
            m_tileCoords.append({x, y});
        }
    }

    // Clear current entities
    m_entities.clear();

    // Load entities
    QJsonArray jsonEntities = json.value("entities").toArray();

    for (auto jsonEntityRaw : jsonEntities) {
        QJsonObject jsonEntity = jsonEntityRaw.toObject();
        auto entity = getEntityBase(jsonEntity.value("name").toString());
        if (jsonEntity.contains("nickname")) entity.nickname = jsonEntity.value("nickname").toString();
        m_entities.set(game::core::jsonToPointF(jsonEntity.value("position")), entity);
    }

    // Clear current locations
    m_locations.clear();

    // Load locations
    QJsonArray jsonLocations = json.value("locations").toArray();

    for (auto jsonLocationRaw : jsonLocations) {
        game::core::Location location = game::core::Location::load(data::MapId(0), jsonLocationRaw.toObject());
        m_locations.push_back(std::move(location));
    }

    refresh();
}

bool editor::Editor::saveToFile(QUrl path) {
    QJsonObject json;
    save(json);

    QJsonDocument document(json);

    QFile file(path.toLocalFile());

    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }

    file.write(document.toJson(QJsonDocument::Compact));
    file.close();

    return true;
}

bool editor::Editor::loadFile(QUrl path) {
    QFile file(path.toLocalFile());
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }

    auto json = QJsonDocument::fromJson(file.readAll());
    if (json.isNull()) {
        return false;
    }

    load(json.object());
    return true;
}

QList<QString> editor::Editor::getTileNames() const {
    return QList(data::BASE_TILE_KEYS.begin(), data::BASE_TILE_KEYS.end());
}

game::core::Tile editor::Editor::getTileBase(QString name) const {
    return data::getTile(name);
}

QList<QString> editor::Editor::getItemNames() const {
    return QList(data::ITEM_KEYS.begin(), data::ITEM_KEYS.end());
}

QList<QString> editor::Editor::getObjectNames() const {
    return QList(data::OBJECT_KEYS.begin(), data::OBJECT_KEYS.end());
}

QList<QString> editor::Editor::getEnemyNames() const {
    return QList(data::ENEMY_KEYS.begin(), data::ENEMY_KEYS.end());
}

QList<QString> editor::Editor::getFriendNames() const {
    return QList(data::FRIEND_KEYS.begin(), data::FRIEND_KEYS.end());
}

QList<QString> editor::Editor::getEntityNames() const {
    return QList(data::ENTITY_KEYS.begin(), data::ENTITY_KEYS.end());
}

game::EntityParams editor::Editor::getEntityBase(QString name) const {
    return data::ENTITIES.at(name);
}

int editor::Editor::getTileCoordCount() const {
    return m_tileCoords.size();
}

QPoint editor::Editor::getTileCoords(int index) const {
    return m_tileCoords.at(index);
}

game::core::Tile editor::Editor::getTile(QPoint coords) const {
    try {
        return m_tiles.get(coords);
    } catch (std::out_of_range) {
        return {};
    }
}

void editor::Editor::addTileTo(QString baseName, int x, int y) {
    game::core::Tile tile = data::getTile(baseName);

    // Do the adding
    bool replacing = false;
    try {
        auto oldTile = m_tiles.get(x, y);

        if (tile.base->isWall && oldTile.baseName == tile.baseName) {
            // Make tile higher when replacing same type
            tile.height += oldTile.height;
        }
        replacing = true;
    } catch (std::out_of_range) {
        // ignore
    }
    m_tiles.set(x, y, tile);

    if (replacing) {
        emit tileChanged(x, y);
    } else {
        m_tileCoords.append({x, y});
        emit tileCountChanged(false);
    }
}

void editor::Editor::fill(QRect rect) {
    fill(rect, "");
}
void editor::Editor::fill(QRect rect, QString baseName) {
    for (int x = 0; x < rect.width(); x++) {
        for (int y = 0; y < rect.height(); y++) {
            if (baseName.isEmpty()) {
                removeTile(rect.x() + x, rect.y() + y, false);
            } else {
                addTileTo(baseName, rect.x() + x, rect.y() + y);
            }
        }
    }
    m_tiles.updateMinMax();
}

void editor::Editor::removeTile(int x, int y, bool update) {
    if (m_tiles.remove({x, y}, update)) {
        emit tileRemoved(x, y);
    }
}

void editor::Editor::removeTile(int x, int y) {
    removeTile(x, y, true);
}

QList<QPointF> editor::Editor::getEntityPositions() const {
    return m_entities.getKeys();
}

game::EntityParams editor::Editor::getEntity(QPointF position) const {
    try {
        return m_entities.get(position);
    } catch (const std::out_of_range&) {
        return {};
    }
}

void editor::Editor::addEntityTo(QString name, double x, double y) {
    m_entities.set({x, y}, getEntityBase(name));

    emit entityCountChanged();
}

void editor::Editor::moveEntity(QPointF from, double x, double y) {
    m_entities.move(from, {x, y});
    emit entityCountChanged();
}

void editor::Editor::nicknameEntity(QPointF position, QString name) {
    auto entity = game::EntityParams(m_entities.get(position));
    entity.nickname = name;
    m_entities.set(position, entity);
    emit entityCountChanged();
}

void editor::Editor::removeEntity(QPointF position) {
    m_entities.remove(position);
    emit entityCountChanged();
}

QList<game::core::Location> editor::Editor::getLocations() const {
    return m_locations;
}

void editor::Editor::addPoint(double x, double y) {
    QString name = "point_" + QString::number(m_locations.size());

    m_locations.push_back({
      {x, y, 0, 0},
      name,
      game::core::Location::Point,
    });
    emit locationCountChanged();
}

void editor::Editor::addArea(double x, double y) {
    QString name = "area_" + QString::number(m_locations.size());

    m_locations.push_back({{x, y, 1, 1}, name, game::core::Location::Area});
    emit locationCountChanged();
}

void editor::Editor::moveLocation(int index, double x, double y) {
    auto loc = m_locations.at(index);
    loc.moveTo(x, y);
    m_locations.replace(index, loc);
    emit locationChanged(index);
}

void editor::Editor::renameLocation(int index, QString name) {
    auto loc = m_locations.at(index);
    loc.name = name;
    m_locations.replace(index, loc);
    emit locationChanged(index);
}

void editor::Editor::resizeLocation(int index, qreal width, qreal height) {
    auto loc = m_locations.at(index);
    if (width > 0) loc.setWidth(width);
    if (height > 0) loc.setHeight(height);
    m_locations.replace(index, loc);
    emit locationChanged(index);
}

void editor::Editor::setLocationTarget(int index, QString target) {
    auto loc = m_locations.at(index);
    loc.target = target;
    m_locations.replace(index, loc);
    emit locationChanged(index);
}

void editor::Editor::removeLocation(int index) {
    m_locations.removeAt(index);
    emit locationCountChanged();
}

QPointF editor::Editor::getCoordOffset() const {
    return m_tiles.getCoordOffset();
}

void editor::Editor::refresh() const {
    emit tileCountChanged(true);
    emit entityCountChanged();
    emit locationCountChanged();
}
