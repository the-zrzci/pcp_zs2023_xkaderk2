#ifndef REALMOFTEMPORALITY_MATRIX_H
#define REALMOFTEMPORALITY_MATRIX_H

#include <QList>
#include <QPoint>
#include <QRect>
#include <QSize>
#include <unordered_map>
#include <vector>

namespace editor {

    template <class K>
    struct hashFunc {
        std::size_t operator()(const K& k) const {
            std::size_t h1 = std::hash<int>()(k.x());
            std::size_t h2 = std::hash<int>()(k.y());
            return h1 ^ h2;
        }
    };

    template <class K>
    struct equalsFunc {
        bool operator()(const K& lhs, const K& rhs) const {
            return lhs.x() == rhs.x() && lhs.y() == rhs.y();
        }
    };

    template <class K, class T>
    struct Matrix {
        typedef T value_type;
        typedef std::unordered_map<K, value_type, hashFunc<K>, equalsFunc<K>> Container;

        Matrix() {}
        Matrix(Matrix const& other)
            : m_data(other.m_data),
              m_minPoint(other.m_minPoint),
              m_maxPoint(other.m_maxPoint),
              m_minMaxFilled(other.m_minMaxFilled) {}

        value_type const& get(int x, int y) const {
            return get({x, y});
        }
        value_type const& get(K coords) const {
            return m_data.at(coords);
        }

        void set(int x, int y, value_type const& item) {
            return set({x, y}, item);
        }
        void set(K coords, value_type const& item) {
            auto result = m_data.insert_or_assign(coords, item);

            // If the coord is new, recalculate minmax
            if (result.second) {
                updateMinMax(coords);
            }
        }

        void move(K from, K to) {
            auto node = m_data.extract(from);
            if (node.empty()) {
                return;
            }
            node.key() = to;

            m_data.insert(std::move(node));
        }

        std::size_t getSize() const {
            return m_data.size();
        }

        QList<K> getKeys() const {
            QList<K> list;
            for (auto pair : m_data) {
                list.append(pair.first);
            }
            return list;
        }

        K getCoordOffset() const {
            return {std::max(-m_minPoint.x(), 0), std::max(-m_minPoint.y(), 0)};
        }
        QRectF getBoundingBox() const {
            return {m_minPoint, m_maxPoint};
        }

        void removeValue(value_type pattern) {
            // https://en.cppreference.com/w/cpp/container/unordered_map/erase_if
            for (auto first = m_data.begin(), last = m_data.end(); first != last;) {
                if (first->second == pattern) {
                    first = m_data.erase(first);
                } else {
                    ++first;
                }
            }

            // Force recalculate minmax
            updateMinMax();
        }

        bool remove(K coords) {
            return remove(coords, true);
        }
        bool remove(int x, int y) {
            return remove({x, y}, true);
        }
        bool remove(K coords, bool update) {
            if (m_data.erase(coords)) {
                // If removed, recalculate min/max :(
                if (update) updateMinMax();
                return true;
            }
            return false;
        }

        std::vector<std::vector<value_type>> toVector() const {
            QRectF bbox = getBoundingBox();
            K offset = getCoordOffset();

            // Prepare 2D vector
            std::vector<std::vector<value_type>> tiles;
            tiles.resize(bbox.height() + 1);

            for (auto& row : tiles) {
                row.resize(bbox.width() + 1);
            }

            // Fill it
            for (auto pair : m_data) {
                K pos = offset + pair.first;
                tiles[pos.y()][pos.x()] = pair.second;
            }

            return tiles;
        }

        Container getData() {
            return m_data;
        }

        void clear() {
            m_data.clear();
            m_minMaxFilled = false;
        }

        void updateMinMax() {
            m_minMaxFilled = false;
            for (auto pair : m_data) {
                updateMinMax(pair.first);
            }
        }

      private:
        Container m_data;
        K m_minPoint;
        K m_maxPoint;
        bool m_minMaxFilled = false;

        void updateMinMax(K coords) {
            if (!m_minMaxFilled) {
                m_minPoint = m_maxPoint = coords;
                m_minMaxFilled = true;
                return;
            }

            if (coords.x() < m_minPoint.x()) {
                m_minPoint.rx() = coords.x();
            } else if (coords.x() > m_maxPoint.x()) {
                m_maxPoint.rx() = coords.x();
            }

            if (coords.y() < m_minPoint.y()) {
                m_minPoint.ry() = coords.y();
            } else if (coords.y() > m_maxPoint.y()) {
                m_maxPoint.ry() = coords.y();
            }
        }
    };
}  // namespace editor

#endif  // REALMOFTEMPORALITY_MATRIX_H
