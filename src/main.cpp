#include <QDir>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QStringList>

#ifdef QT_DEBUG
#include <QDirIterator>
#include <QElapsedTimer>
#include <QFileSystemWatcher>
#endif

#include "frontend/Frontend.h"

int main(int argc, char* argv[]) {
    // Init
    QGuiApplication app(argc, argv);
    frontend::Frontend gui;

// Setup resource finding
#ifdef QT_DEBUG
    QString resourcePath = QDir::cleanPath(QString(argv[0]) + "/../..");
    QDir::setSearchPaths("rot", QStringList({resourcePath}));
    qInfo() << "Developer mode active: Will load resources from" << resourcePath;
#else
    QDir::setSearchPaths("rot", QStringList({":/res"}));
#endif

    // Start frontend
    gui.load();

#ifdef QT_DEBUG
    // Start hot-reload
    QFileSystemWatcher watcher;
    QElapsedTimer watchDelay;

    watcher.connect(&watcher, &QFileSystemWatcher::fileChanged, &gui, [&watchDelay, &gui](const QString& path) {
        if (watchDelay.hasExpired(1000)) {
            watchDelay.restart();
            gui.reload();
        }
    });

    // Find all qml files
    QDirIterator qmlDir(  //
      resourcePath + "/src/frontend",
      QStringList({"*.qml"}),
      QDir::Files,
      QDirIterator::Subdirectories
    );
    while (qmlDir.hasNext()) {
        watcher.addPath(qmlDir.next());
    }

#endif

    // Run Qt
    return app.exec();
}
